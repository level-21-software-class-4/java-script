## 数组



prompt：默认输入的是字符串 string 

var str='javascrpit'  //拆分字符串 split

const arr =str.split('')  //把str里的字符串分开

console.log(arr)



var arr=[1、'5'、null、undefined]  //可以不用同一个类型

console.log(arr)   //引用型基本数据类型（值类型）

引用型用const 节约空间 列: const arr=[....]

### 增

push、pop、shift、unshift

arr.pop()  //删除最后一个元素

arr.push([4、5、6])  //在数组末尾添加元素

arr [10]='java'  //直接删除元素

arr.shift() // 移除第一个元素

arr.unshift()  //往数组头部前增加元素

### 查

arr[index]

console.log(arr[2])  //查询下标第n个元素

### 改

arr[1]='五'

### 删

delete arr[0]



### 排序sort     

//根据字符串顺序排序，不是按照数字大小排序

const arr=[1、4、5、2、3];

arr.sort();

console.log(arr);

结果：1、2、3、4、5  //int 型可以排序

 reverse（） 反转数组

 arr.reverse()

console.log(arr1)



splice()：嫁接（裁接不需要，需要的是接上去）

splice(index、delecount、insertvalue):  小标index 开始、删除 delecount个元素、再添加insertvalue

//只删除、替换、添加

列：arr.splice(3.0,'嫁'，'接'，666；  //替换作用



### 冒泡排序

列:  let arr=[90,65,78,66,48,39,92,73]  //通过冒泡排序从小到大，从大到小

​     for (var i = 0; i < arr.length-1; i++) {

​       for (var j = 0; j < arr.length-1; j++) {

​       if (arr[j]<arr[j+1]) {

​         [arr[j],arr[j+1]]=[arr[j+1],arr[j]]

​     }  

  }

​    }

​     console.log(arr); 



### 数组解构

const arr=[1、2、3、4];

var a=arr[0];

var b=arr[1];

var d=arr[3];

var [a,.....args]=arr   //赋值给定义好的变量    用"...."的方式提取所有的元素

console.log(a);

.

.

.

console.log(d);

console.log(args)     //数组结构就是能快速提取数组中的指定元素结构赋值都是一一对应的按照顺序的





