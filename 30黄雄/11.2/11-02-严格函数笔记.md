第十二课:严格函数

​	arguments:伪数组,只有下标，长度，元素是实参的映射

改变this指向

​    // 函数被执行时，内部会产生一个this

```
 // window.getNum();
```

严格模式:防止意外发生错误时，及时抛出该错误

​    1.在严格模式下，函数内部不会再意外地生成全局变量，而是抛出异常

例题:

```
function getName(a,b,a){
             'use strict'
             console.log(a);
              a = 5;

         }
         getName(1,3,5);
         console.log(a);
```



​	2.严格模式要求一个对象内的所有属性名在对象内必须唯一

例题:

```
"use strict"
         var obj = {
             a:1,
             a:2,
             b:4
         }
         console.log(obj.a);

         function Product(name, price) {
             'use strict'
             console.log(this); //null Prodcut Window
             this.name = name;
             this.price = price;
         }
```

​	3.call(null) call(undefined):严格模式下会抛出错误，非严格模式会自动转换全局

例题:

```
Product.call(undefined, '张三', 15)
```

​	4.构造函数使用严格模式,必须使用new

例题:

```
function Product(name, price) {
             'use strict'
             this.name = name;
             this.price = price;
         }
         new Product('张三',18);
```

​	构造函数:Person( );

​	普通函数小驼峰:getNameAge；

​	普通变量: personAge person_age；

​	常量:Pl