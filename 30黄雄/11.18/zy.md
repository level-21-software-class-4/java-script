元素(elements)

查看body中有几个元素节点:

```
console.log(document.body.childElementCount);
console.log(document.body.children);
```

查看子节点:

```
console.log(document.body.firstChild);//所有节点中的第一个节点
console.log(document.body.firstElementChild);//找第一个元素节点
```

查看节点方法:

```
//查看父节点:
console.log(document.body.children[0].parentElement);
//查看上一个节点:
console.log(document.body.children[1].previousElementSibling);
//查看下一个节点:
console.log(document.body.children[1].nextElementSibling);
DOM节点方法:

```
//1.根据class获取元素: 返回: HTMLCollection (数组)
console.log(document.getElementsByClassName('H2'));

//2.根据name属性获取元素对象: NodeList(伪数组)：只能获取下标和长度
console.log(document.getElementsByName('username'));

//3.根据标签名获取元素对象:返回 HTMLCollection
console.log(document.getElementsByTagName('input'));

//4.根据id属性获取元素对象，返回当前元素对象
console.log(document.getElementById('thirdH2'));
```
添加子元素节点
        // div.appendChild(newH2);

删除子节点:removeChild(元素对象:h2)
        // var h2 = document.getElementById('h2');
        // div.removeChild(h2);

改:replaceChild
        // div.replaceChild(newH3,h2);

在...之前插入一个节点
        div.insertBefore(newH3,h2);