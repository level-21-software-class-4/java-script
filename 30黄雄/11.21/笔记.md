#### innerHTML与innerText的区别

- innerHTML获取的是文本内容和标签，但是innerText是只获取文本内容

- innerHTML具备解析字符串的能力，但是innerText没有

#### dom获取表单的值

1. ##### **获取**text输入框的值

var p = document.getElementById("test").parentNode

2、parentElement获取父节点

parentElement和parentNode一样，只是parentElement是ie的标准。

var p1 = document.getElementById("test").parentElement;

3、offsetParent获取所有父节点