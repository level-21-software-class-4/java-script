	//闭包 = 内层函数 + 引用的外层函数变量
        //当外部想要使用闭包的变量时，需要return
        // function outer(){
        //     var a = 10;
        //     return function(){
        //         console.log(a);
        //     }
        // }
        // var fn = outer();
        // fn();
        
        //函数内部可以访问外部的全局变量
        //函数外部无法读取函数内部的变量
        // let age = 10;
        // function huhu(){
        //     console.log(age);
        // }
        // huhu();

        //链式作用域结构
        //首先子对象会在自身的作用域寻找变量，如果没有则一层一层的向父级寻找。、
         // var name = '小明'
         // function man() {
         //     console.log(name)
         //     var age = '20';
         //     (function getage() {
         //         console.log(age)
         //     }())
         // }
         // man()

         //闭包的作用
         //访问局部变量
        //  function man(){
        //     var age = 20;
        //     return function(){
        //         console.log(age);
        //     }
        //  }
        //  //console.log(age); //报错
        // var huhu = man();
        // huhu();

        //保存局部变量
        //因为垃圾回收机制的存在，函数执行完成后里面的局部变量会被销毁
        // function get(){
        //     var num = 0;
        //     return function(){
        //         console.log(num++);
        //     }
        // }
        // var aa = get();
        // aa(); // 0
        // aa(); // 1

        //实现类的封装
        // function man(){
        //     var name = '小明';
        //     var age = 15;
        //     return {
        //         getName : function (){
        //             console.log(name);
        //         },
        //         setName : function (newName){
        //             name = newName;
        //         }
        //     }  
        // }
        // var huhu = new man();
        // huhu.getName();
        // huhu.setName('张三');
        // huhu.getName();

        //注意点：内存泄漏 （正常情况下，因为垃圾回收机制，局部变量在函数执行完毕之后其生命周期也结束，
        //所以内存会被回收释放。但是因为我们使用了闭包，导致变量一直保存在内存，无法得到释放，造成内存的消耗。）
        //退出函数前，将不使用的局部变量删除，或者解除函数的引用
        // function num(){
        //     var num = 0;
        //     return function(){
        //         console.log(num++);
        //     }
        // }
        // var huhu = num();
        // huhu();
        // huhu();
        // huhu = null; //解除函数的引用，此时变量num释放