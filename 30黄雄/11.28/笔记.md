1.addEventListener("eventType” ,fun)2.element.onEventType = fun 区别: 

1.addEventListener在同一元素上的同一事件类型添加多个事件，不会被覆盖，而onEventType会覆盖。

2.addEventListener可以设置元素在捕获阶段触发事件，而onEventType不能 事件捕获与事件冒泡 默认情况下，事件会在冒泡阶段执行 addEventListener(eventType,fun,boolean); 

//默认false:冒泡阶段触发;true:捕获阶段触发 阻止事件默认行为 为一个可以跳转到百度的a标签设置点击事件︰去掉事件默认行为 e.preventDefault() 或 return false 事件委托 通过e.tatiget将子元素的事件委托给父级处理。

属性 描述 onblur 元素失去焦点时触发

 onchange 该事件在表单元素内容改变时触发

 onfocus 元素即将获取焦点时触发 

onfcusin 元素即将失去焦点时触发 

onfocusout 元素获取用户时触发 

onreset 表单重置时触发 

onserach 用户搜索输入文本域时触发

 onselect 用户选取获取文本时触发 

onsubmit 表单提交时触发