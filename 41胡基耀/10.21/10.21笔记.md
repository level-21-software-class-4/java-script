对像

对象： 类(人类，鱼类，鸟类) 万物对象:属性和行为

属性:增删改查 1.查 . 一级属性 二级属性往后[][] console.log(obj.name) 查 使用 [] console.log(obj['name']['lastName'])

    2.增 obj.原来obj中没有的属性，相当于将该属性添加到对象obj中
    obj.gender = '男'
    console.log(obj);

    3.删 delete  一般删之前要对该属性进行判断 
    delete obj.age
    console.log(obj);
构造函数 = new 对像

    普通函数命名规则：小驼峰:getName()
    构造函数命名规则：大驼峰:GetName()
箭头函数

匿名函数： function (){}

arguments:只有长度，只能找下标和元素

箭头函数： var aa = (形参列表) => {函数体} arrow function

函数只有一个return语句时，省略{}和return关键字,直接写返回值

当函数只有一个形参时，可以省略圆括号

var ee = () => '这是一个箭头函数'; //无参时，圆括号不可以省略

无返回值,一般不用箭头函数