# JQuery

##### 其他事件

解绑事件：$(this).off('要解绑的事件')

只触发一次事件：$('元素').one('事件名',function(){

})



### 事件对象

事件对象什么时候产生：触发事件的时候 this



##### 直接绑定

hover合成事件：

鼠标移动的时候：mouseover mouseout(合成事件hover)

事件的链式调用,为一个对象绑定多个事件：

```js
$('button').mouseover(
        function () {
            $(this).css('color','red');
        }
    ).mouseout(
        function () {
            $(this).css('color','black');
        }
    ).click(
        function () {
            console.log('click');
        }
    )
```

bind():可以为多个事件绑定同一个函数：

```js
 $('button').bind('click   ',function () {
        console.log('这是bind');
    })

```

on():绑定

on可以为动态添加的元素进行事件绑定

```js
$(document).on('click','button',function () {
        console.log('这是新创建的元素');
    })
```



#### JQuery事件冒泡

##### JQuery实现不了事件捕获

阻止事件冒泡：**event.stopPropagation()**

阻止默认行为：event.preventDefault()
