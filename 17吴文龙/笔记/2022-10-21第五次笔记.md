函数:
     匿名函数： function (){}
     箭头函数:var aa =   (形参列表)   =>  {函数体} arrow function

     无返回值,一般不用箭头函数
     如果箭头函数返回一个对象,那么需要在对象外面加上括号
     例: 
         var aa = function (n1, n2) {
             var sum = n1 + n2
             return sum;
         }
        箭头:
         var bb = (n1, n2) => {
             var sum = n1 + n2;
             return n1+n2;
         }
         var cc = (n1, n2) => n1 + n2; //函数只有一个return语句时，省略{}和return关键字,直接写返回值
         当函数只有一个形参时，可以省略圆括号
         var dd = (num) => {
             var res = 1;
             for (var i = 1; i <= num; i++) {
                 res *= i;
             }
             return console.log(res);;
         }

         var ee = () => '这是一个箭头函数';  //无参时，圆括号不可以省略
对象:
         对象：  类(人类，鱼类，鸟类)   万物对象:属性和行为
         字面量表达式创建对象
          var obj = {
             name:{
                firstName:'李',
                lastName:'华'
            },
             age:18,
             'qq number': 4619163491
         }  
         属性:增删改查

         1.查 . 一级属性  二级属性往后[][]
         1.1查  使用 []
         console.log(obj['name']['lastName']);  
         增 obj.原来obj中没有的属性，相当于将该属性添加到对象obj中
         obj.gender = '男'
         console.log(obj);

         删 delete  一般删之前要对该属性进行判断 
         delete obj.age
         console.log(obj);

         构造函数   =  new 对象

         普通函数命名规则：小驼峰:getName()
         构造函数命名规则：大驼峰:GetName()
         var obj = new GetName()   //生成了一个对象
         console.log(obj.age);
        
        