# 事件

#### 绑定事件的方法

ele.onXXX = function(event){}

obj.addEventListener(type,fn,bool);

obj.attachEvent('on'+type,fn,bool);

#### 取消事件绑定

第一种方式：document.onclick = null;

第二种方式：obj.detachEvent(事件名称，事件函数);

第三种方式：obj.removeEventListener(事件名称，事件函数，是否捕获);




#### 事件对象

Event 对象代表事件的状态，比如事件在其中发生的元素、键盘按键的状态、鼠标的位置、鼠标按钮的状态。

事件通常与函数结合使用，函数不会在事件发生前被执行！

当一个事件发生的时候，和当前这个对象发生的这个事件有关的一些详细信息（包括导致事件的元素、事件的类型、以及其它与特定事件相关的信息等。这个对象是在执行事件时，浏览器通过函数传递过来的。）都会被临时保存到一个指定的地方——event对象，供我们在需要的时候调用