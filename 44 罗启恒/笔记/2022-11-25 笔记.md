将dom对象转成jquery对象
    // var jqDiv = $(div);
    // $(jqDiv).click(
    //     function () {
    //         alert('这是js包装集对象弹窗')
    //     }

jquery对象 -->dom对象

基础选择器：标签选择器
    // $('div')

id:# class: . 

选择所有元素:document之下的
    // console.log($('*'));

群组选择器
    console.log($('#box,.btn'));

后代:所有后代（不止一代）的元素 $('#parent div')
    // console.log($('#parent div'));

子代(只找后面一代)：$('#parent>div')
    // console.log($('#color>img'));

相邻选择器:只能找后面一个元素
    // console.log($('.blue+button'));

同辈选择器:往后找多个
    console.log($('#color~div'));