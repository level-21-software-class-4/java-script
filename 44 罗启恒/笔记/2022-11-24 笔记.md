**事件冒泡**:微软提出了名为**事件冒泡**的事件流。事件按照从最特定的事件目标到最不特定的事件目标(document对象)的顺序触发。可以想象把一颗石头投入水中，泡泡会一直从水底冒出水面。也就是说，事件会从最内层的元素开始发生，一直向上传播，直到document对象。

**事件捕获**:网景提出另一种事件流名为**事件捕获**。事件从最不精确的对象(document 对象)开始触发，然后到最精确(也可以在窗口级别捕获事件，不过必须由开发人员特别指定)，与事件冒泡相反，事件会从最外层开始发生，直到最具体的元素。同样形象的比喻一下可以想象成警察逮捕屋子内的小偷，就要从外面一层层的进入到房子内。

#### 事件阻止冒泡

```js
ev.stopPropagation(); //不支持ie9以下 DOM
event.cancelBubble = true; //ie独有 ie

封装取消冒泡的函数：stopBubble(event)
```

但是有些事件本身是不冒泡的：（表单事件）focus,blur,change,submit,reset,select 等事件不冒泡

#### 事件流

​		事件有三个阶段，首先发生的是捕获阶段，然后是目标阶段，最后才是冒泡阶段，对于捕获和冒泡，我们只能干预其中的一个，通常来说，我们可能会干预事件冒泡阶段，而不去干预事件捕获阶段。

#### 阻止默认行为

​		默认事件：表单提交，a标签跳转，右键菜单等

**事件委托**：利用事件冒泡，和事件源对象进行处理

优点：性能高 灵活

#### JS常用事件

###### 1.点击事件

onclick：单击事件

ondblclick：双击事件

###### 2.焦点事件

onblur：失去焦点

onfocus:元素获得焦点。

###### 3.加载事件

onload：一张页面或一幅图像完成加载。

###### 4.鼠标事件

onmousedown： 鼠标按钮被按下。

onmouseup： 鼠标按键被松开。

onmousemove： 鼠标被移动。

onmouseover： 鼠标移到某元素之上。

onmouseout ：鼠标从某元素移开。

mouseenter： 

mouseleave：

contextmenu:

鼠标按下时，通过事件对象 event中的属性 button 或 which 可以获取鼠标按键的编号e.button 事件对象中的 button属性可以获取鼠标按键的编号e.which 也可以获取鼠标的按键编号 0 左键 1滚轮 2右键

DOM3规定：click事件只能监听左键，只能通过mousedown和mouseup来判断鼠标键

如何解决mousedown和click的冲突

###### 5.键盘事件

onkeydown ：某个键盘按键被按下。

onkeyup： 某个键盘按键被松开。

onkeypress ：某个键盘按键被按下并松开。

###### 6.选择和改变

onchange ：域的内容被改变。

onselect ：文本被选中。

###### 7.表单事件

onsubmit ：确认按钮被点击。

onreset： 重置按钮被点击