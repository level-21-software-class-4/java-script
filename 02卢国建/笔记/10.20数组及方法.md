// 1.1 concat:连接,数组合并
const arr = [1,2,3]
const arr1 = [4,5,6]
const new_arr = arr.concat(arr1);
// 1.2 用...args:剩余运算符,将剩下的参数都放进args数组中也可以完成拼接
const new_arr = [...arr,...arr1]

// 2.1 join:将数组转成字符串 2.2 将字符串转成数组 string.split('')
const arr = [1, 2, 3, 4, 5, 6]
console.log(arr.join);
var str = arr.join(',') // ('表示用什么来接连')
console.log(str);

//3. 映射方法map(),
const arr = [1, 2, 3, 4, 5, 6]
const arr1 = arr.map(
	function (element) {
    	return element + 1
    }
)
console.log(arr1);
  
// 4.将数组的单词全转为大写
const countries = ['Finland', 'Sweden', 'Norway', 'Denmark', 'Iceland']
const new_countries = countries.map(
	function (e){
    	return e.toUpperCase()
    }
)
console.log(new_countries);


// 5.filter过滤:返回数组中符合条件的数据
const new_arr = arr.filter(
	function (e){
    	return e%2==0  //true：返回 false：不符合条件=>过滤
    }
)
console.log(new_arr);


// 6.reduce(),reduceRight():归纳汇总： 返回一个总数据
const arr = [1, 2, 3, 4, 5, 6]
var sum = arr.reduceRight(
	function (v1,v2){
    	return v1*v2
    }
)
console.log(sum);

// 7.1 every():只要有一个false，返回

var isMatch = arr.every(
	function (e){
		return e>1
    }
)
console.log(isMatch);
// 7.2 some():
var isMatch = arr.some(
	function (e){
		return e==6
    }
)
console.log(isMatch);
2.短路或：短路与
var a = 4;
var b = 5;
(a===5) && (++b) 
// 若 && 条件 第一个返回 false 后面不执行
// 若 || 条件 第一个返回 true 后面的不执行 形成短路
3. 二维数组
//二维数组
const arrayOfArray =  [[1, 2, 3], [4, 5, 6]]
//增删改查 与一维数组类似
console.log(arrayOfArray[1][1]);

//删
rrayOfArray[1].splice(1,1)
console.log(arrayOfArray);