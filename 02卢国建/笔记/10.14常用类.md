基本数据类型

 Number  - Integer,float

 Boolean    Null     Undefined    Symbol     String 

\n:换行
\t:Tab键
\\:斜杠
\':单引号
\":双引号

字符号下标
第一个元素:  string[0]		最后一个元素: string[string.length-1]


concat:将两个数组合并在一起
indexof:查找数组的元素，返回-1表示不存在,与之相同的查找方法还有includes,lastindexof
toString():将数组转成字符串
slice():表示切片，参数个数：0 1 2  返回一个数组对象
splice(): 接收 0 2 多 个参数 表示移除，替换元素

Math: 舍入   ceil(向上取整) floor(向下取整)  round(四舍五入)
//&  同1为1，有0为0  |:有1为1， 同0为0

       var str = `JavaScript`;
        
        const arr = str.split('S')  //将字符串以（）分割，返回是数组
        console.log(arr);
        // console.log(String.prototype);