对象

定义对象

字面量

var obj = {
    name:value;
    'String':value;
}
构造函数

function GetName() {
    this.name = values;
    this.age = values;
}
工厂模式（设计模式）

function GetValues(name,age) {
    var that = { }
    that.name = name
    that.age = age
    return that
   }
生成对象

var obj = new obj();

const nameArr = Object.keys(users)

nameArr.forEach(key => users[key].isLoggedIn ? count++ : count)

增删查

查：obj.name

增：obj.原来obj中没有的属性，相当于将该属性添加到对象obj中

删：delete 一般删之前要对该属性进行判断

对象解构

let{name:personName, age:personAge} = obj（取别名）

嵌套解构

let { name: personName, skill: { sname: skillName } } = obj