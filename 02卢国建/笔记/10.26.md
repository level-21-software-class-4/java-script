包装类
构造函数的原理

​ function Test(){

this{}

this = {

​ name: '张三'

​ age:16

​ }

​ this.name = '张三'

​ this.age = 16

​ return this

​ }

​ var test = new Test();

for ... of :遍历

​ for (e of Object.keys(obj)) {

​ console.log(e);

}

将键值对成对变成数组放入外层数组(二维数组)

​ const arr = Object.entries(obj)

​ console.log(arr);

​ or(e of Object.entries(obj)){

​ console.log(e);

​ }

obj本身是不可迭代的对象

​ for(e of obj){

​ console.log(e);

}

assign:浅拷贝（复制的是同一个地址，创建一个新的栈指向同一个堆），浅拷贝还具有合并对象的作用（将后面的对象向第一个对象合并，有相同属性的会进行替换）

原始数据类型： number, boolean, string, null, undefined (symbol,bigint)

包装类只有：number，boolean，string, null,undefined是没有包装类的

包装类：自动转成对象

var num = 5; 原始数据类型

num.a = 7; 1. new Number(5).a = 7

​ 2.delete new Number(5)

console.log(num.a); new Number(5).a undefined