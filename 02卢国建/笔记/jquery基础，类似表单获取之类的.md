在JavaScript中，onload表示文档加载完成后再执行的一个事件。window.onload只能调用一次，如果多次调用，则只会执行最后一个。

```html
window.onload = function(){
……
}

```

对于JavaScript的onload事件来说，只有当页面所有DOM元素以及所有外部文件（图片、外部 CSS、外部JavaScript等）加载完成之后才会执行。这里的所有DOM元素，指的是HTML部分的代码。

#### ready事件

		在jQuery中，ready也表示文档（document）加载完成后再执行的一个事件。

```html
$(document).ready(function(){
	……
})

```

		对于jQuery的ready事件来说，只要页面所有DOM元素加载完成就可以执行，不需要再等外部文件 （图片、外部CSS、外部JavaScript）加载完成。

#### read事件的四种写法

```
//写法1：
$(document).ready(function(){
……
})
//写法2：
jQuery(document).ready(function(){
……
})
//写法3(最常用)：
$(function(){
……
})
//写法4：
jQuery(function(){
……
})

```



#### 鼠标事件

在jQuery中，常见的鼠标事件如下表:

| 事件      | 说明         |
| --------- | ------------ |
| click     | 鼠标单击事件 |
| dblclick  | 鼠标双击事件 |
| mouseover | 鼠标移入事件 |
| mouseout  | 鼠标移出事件 |
| mousedown | 鼠标按下事件 |
| mouseup   | 鼠标松开事件 |
| mousemove | 鼠标移动事件 |
|           |              |

​	

hover:mouseover+mousedown

	jQuery 事件比JavaScript事件只是少了“on”前缀。例如鼠标单击事件在JavaScript中是onclick，而在jQuery中是 click。
	
		jQuery事件的语法很简单，我们都是往事件方法中插入一个匿名函数function(){}

## jQuery Dom 操作

​		jQuery也提供了对HTML节点的操作，而且在原生js的基础之上进行了优化，使用起来更加方便。
​		常用的从几个方面来操作，查找元素（选择器已经实现）；创建节点对象；访问和设置节点对象的值，以及属性；添加节点；删除节点；删除、添加、修改、设定节点的CSS样式等。**注意：以下的操作方式只适用于jQuery对象。**

#### **4.1** **操作元素的属性**

###### 4.1.1 获取属性

| 方法           | 说明                                                         | 举例                              |
| -------------- | ------------------------------------------------------------ | --------------------------------- |
| attr(属性名称) | 获取指定的属性值，操作checkbox时<br />选中返回checked，没有选中返回undefined。 | attr('checked')<br />attr('name') |
| prop(属性名称) | 获取具有true和false两个属性的属性值                          | prop('checked')                   |

属性的分类：

- 固有属性：

- 返回值是bool的属性: checked ,selected, disabled

- 自定义属性

- 4.1.2 设置属性

  ```html
  attr('属性名','属性值')
  ```

  

  ###### 4.1.3 移除属性

  ```html
  removeAttr('属性名')
  ```

  #### 4.2 操作元素的样式

  ​		对于元素的样式，也是一种属性，由于样式用得特别多，所以对于样式除了当做属性处理外还可以有专门的方法进行处理。

  | 方法                   | 说明                        |
  | ---------------------- | --------------------------- |
  | attr('class')          | 获取属性的值，即样式名称    |
  | attr('class','样式名') | 修改class属性的值，修改样式 |
  | addClass('样式名')     | 添加样式名称                |
  | **css()**              | 添加具体的样式              |
  | removeClass(class)     | 移除样式名称                |

  其中，css()表示增加具体样式：

  ```html
  1) css('样式名','样式值')
    例: css('color','red')
  
  2) css({'样式名':'样式值','样式名2':'样式值2'})
    例: css({'background-color':'red','color':'red'})
  ```

#### 4.3 操作元素内容

| 方法              | 说明                               |
| ----------------- | ---------------------------------- |
| html()            | 获取元素的html内容                 |
| html('html内容')  | 设定元素的内容                     |
| text()            | 获取元素的内容，不识别html标签     |
| text('text 内容') | 设置元素的文本内容，不识别html标签 |
| val()             | 获取元素的值（表单元素）           |
| val('值')         | 设定元素的值                       |

常见的表单元素(可以操作的):文本框，密码框，单选框，复选框，隐藏域，文本域，下拉框

非表单元素:div,span,h1~h6,table,tr,td,li,p等

#### 4.4 创建元素

​		在jQuery创建元素很简单，直接使用核心函数即可

```html
$('元素内容');
```

```
$('<p>this is a paragraph!!!</p>')
```

#### 4.5 添加元素

| 方法                           | 说明                                                         |
| ------------------------------ | ------------------------------------------------------------ |
| prepend(content)               | 在被选元素内部的开头插入元素或内容，被追加的content参数，可以是字符，HTML元素标记。 |
| $(content).prependTo(selector) | 把content元素或内容加入selector元素开头                      |
| append(content)                | 在被选元素内部的结尾插入元素或内容， 被追加的content参数，可以是字符，HTML元素标记。 |
| $(content).appendTo(selector)  | 把content元素或内容插入slector元素内，默认是在尾部           |
| before()                       | 在元素前插入指定的元素或内容: $(selector).before(content)    |
| after()                        | 在元素后插入指定的元素或内容: $(selector).after(content      |

#### 4.6 删除元素

| 方法     | 说明                                                 |
| -------- | ---------------------------------------------------- |
| remove() | 删除所选元素或指定的子元素，包括整个标签和内容一起删 |
| empty()  | 清除所选元素的内容                                   |

#### 4.7 遍历元素

each()

$(selector).each(function(index,element)):遍历元素

参数function为遍历时的回调函数

index 为遍历元素的序列号，从0开始

element为当前的元素(===this)，此时是dom元素

