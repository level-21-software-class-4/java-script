 //继承

        //原型链继承
        //父类中新增的实例和子类的都可以使用
        //可以在子类中增加实例属性，如果要新增加原型属性和方法需要在new 父类构造函数的后面
        // function Father(){
        //     this.say = function(){
        //         console.log('父类');
        //     }
        // };
        // function Son(){};
        // Son.prototype = new Father();
        // var son = new Son();
        // son.say();

    
        //圣杯模式
        // function Father(){};
        // function Son(){};
      
        // function inherit(Target,Origin){
        //     function F(){};
        //     F.prototype =  Origin.prototype;
        //     Target.prototype = new F();
        //     Target.prototype.constructor = Target;
        //     Target.prototype.uber = Origin;
        // }
        // inherit(Son,Father);
        // Father.prototype.age = 25;
        // Son.prototype.age = 10;
        // console.log(Father.prototype.age);
        // console.log(Son.prototype.age);

