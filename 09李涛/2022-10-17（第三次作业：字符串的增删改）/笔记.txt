const：
	1、const 的本质: const 定义的变量并非常量，并非不可变，它定义了一个常量引用一个值。
	2、const 用于声明一个或多个常量，声明时必须进行初始化，且初始化后值不可再修改。
	3、const 定义常量的值不能通过再赋值修改，也不能再次声明。

push():
	JavaScript push() 方法

	push() 方法可向数组的末尾添加一个或多个元素，并返回新的长度。

	注意： 新元素将添加在数组的末尾。

	注意： 此方法改变数组的长度。

	数组中添加新元素：
	 例：
	var fruits = ["Banana", "Orange", "Apple", "Mango"];
	fruits.push("Kiwi")
	fruits 结果输出：

	Banana,Orange,Apple,Mango,Kiwi
	
unshift():
	unshift() 方法可向数组的开头添加一个或更多元素，并返回新的长度。

	注意： 该方法将改变数组的数目。
	提示： 在数组起始位置添加元素请使用 unshift() 方法。
	
	将新项添加到数组起始位置:
	 例：
	var fruits = ["Banana", "Orange", "Apple", "Mango"];
	fruits.unshift("Lemon","Pineapple");
	fruits 将输出：

	Lemon,Pineapple,Banana,Orange,Apple,Mango

sort():

	sort() 方法能够根据一定条件对数组元素进行排序。如果调用 sort() 方法时没有传递参数，则按字母	顺序对数组中的元素进行排序。
	  例：
	var a=['a','e','d','b','c'];//定义数组
	a.sort();//按字母顺序对元素排序
	console.log(a);//返回数组[a,b,c,d,e]
	
	使用 sort() 方法时，应该注意下面几个问题。

	1) 所谓的字母顺序，实际上是根据字母在字符编码表中的顺序进行排列的，每个字符在字符表中都有	一个唯一的编号。

	2) 如果元素不是字符串，则 sort() 方法试图把数组元素都转换成字符串，以便进行比较。

	3) sort() 方法将根据元素值进行逐位比较，而不是根据字符串的个数进行排序。
	  例：
	var a=['aba','baa','aab'];//定义数组
	a.sort();//按字母顺序对元素进行排序
	console.log(a);//返回数组[aab,aba,baa]

	4) 在任何情况下，数组中 undefined 的元素都被排序在末尾。

	5) sort() 方法是在原数组基础上进行排序操作的，不会创建新的数组。

	sort() 方法不仅按字母顺序进行排序，还可以根据其他顺序执行操作。这时就必须为方法提供一个函数	参数，该函数要比较两个值，然后返回一个用于说明这两个值的相对顺序的数字。排序函数应该具有	两个参数 a 和 b，，其返回值如下。

	如果根据自定义评判标准，a 小于 b，在排序后的数组中 a 应该出现在 b 之前，就返回一个小于 0 的	值。

	如果 a 等于 b，就返回 0。

	如果 a 大于 b，就返回一个大于 0 的值。

pop:
	pop() 方法用于删除并返回数组的最后一个元素。
	语法：arrayObejct.pop()

	pop() 方法将删除 arrayObject 的最后一个元素，把数组长度减 1，并且返回它删除的元素的值。如	果数组已经为空，则 pop() 不改变数组，并返回 undefined 值。

splice:
	splice方法可以用来对js的数组进行删除，添加，替换等操作。
	  例：
	
        //splice():嫁接(裁掉不需要，需要的接上去)
        //delete count

        //splice(index,delecount,insertvalue):下标index开始，删除delecount个元素
        //再添加inservalue
        //只删除，  替换，   添加
        // arr.splice(3,0,'嫁','接',666);   //替换作用

        // console.log(arr);

	1. 删除功能，第一个参数为第一项位置，第二个参数为要删除几个。

	用法：array.splice(index,num)，返回值为删除内容，array为结果值。

	2. 插入功能，第一个参数（插入位置），第二个参数（0），第三个参数（插入的项）。

	用法：array.splice(index,0,insertValue)，返回值为空数组，array值为最终结果值。

	3. 替换功能，第一个参数（起始位置），第二个参数（删除的项数），第三个参数（插入任意数量的		项）。

	用法：array.splice(index,num,insertValue)，返回值为删除内容，array为结果值。





