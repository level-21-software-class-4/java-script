定义对象

字面量

    var obj = {
        name:value;
        'String':value;
    }

构造函数

    function GetName() {
        this.name = values;
        this.age = values;
    }

工厂模式（设计模式）

    function GetValues(name,age) {
        var that = { }
        that.name = name
        that.age = age
        return that
       }

生成对象

var obj = new obj();

## 6.2 增删查

查：obj.name

增：obj.原来obj中没有的属性，相当于将该属性添加到对象obj中

删：delete  一般删之前要对该属性进行判断

## 6.3 对象解构

let{name:personName, age:personAge} = obj（取别名）


四、对象解构

```js

let person = {
    name:'Zoe',
    age:27
}
//不使用解构
let personName = person.name, personAge = person.age

//使用解构:可以在一个类似对象字面量的结构中，声明多个变量。同时执行多个赋值操作
let {name:personName, age:personAge} = person;

//让变量直接使用属性名称
let {name,age} = person

log(personName,personAge)

```

​	解构赋值不一定需要与对象的属性匹配。赋值的时候可以忽略某些属性，而如果引用的属性不存在，则为undefined。此时可以在解构的同时定义默认值

```js
let {name, job} = person  //此时没有job属性
let {nam, job='student'} //设定默认值
```

​	解构并不要求变量必须在解构表达式声明,如果是事先给的，则必须包含在一对括号中

```js
let personName, personAge;
({name:personName,age:personAge} = person )

```

还可以使用嵌套解构获取
