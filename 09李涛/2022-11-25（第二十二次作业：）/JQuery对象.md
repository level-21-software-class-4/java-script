# [JQuery](https://so.csdn.net/so/search?q=JQuery&spm=1001.2101.3001.7020)对象

#### [JQuery](https://so.csdn.net/so/search?q=JQuery&spm=1001.2101.3001.7020)对象的基本方法：

###### (1) get(); 取得所有匹配的元素

###### (2) get(index); 取得其中一个匹配的元素 $(this).get(0) 等同于 $(this)[0]

###### (3) Number index(jqueryObj); 搜索子对象

###### (4) each(callback); 类似[foreach](https://so.csdn.net/so/search?q=foreach&spm=1001.2101.3001.7020)，不过遍历的是元素数组

如：

```js
$("img".each(function(index){

  this.src = "test" + index + ".jpg";

});

使用 return false; return true; 代表 break、continue的功能
```

###### (5) length、size(); 都是返回元素总数值

###### (6) jQuery.noConflict(true);  重设 jquery 默认的符号

如：

 

```js
 var dom = {};

  dom.query = jQuery.noConflict(true);

这时将用 dom.query 代替 $
```

