# DOM文档对象模型：

#### 	操作style样式：

​			获取行间样式的时候需要进行初始化否则无法获取！ 

```js
 var div = document.querySelector('#box');
 var btn = document.querySelector('#btn');
```

## setAttribute

###### 元素.setAttribute('属性名',属性值)

设置元素的行间属性，如果原来有这个行间属性会覆盖原来的行间属性
可以通过该方法给元素新增自定义行间属性

```js
var oBox = document.getElementById("box");
oBox.setAttribute("width","200px");//给oBox对象新增行间属性width="200px"，不能覆盖css样式中的width属性值
oBox.setAttribute("class","myDiv");//给oBox对象新增行间属性class="myDiv"
```

## getAttribute


元素.getAttribute('属性名')
获取元素的行间属性对应的属性值，不能获取css样式对应的属性值
如果获取的属性不存在返回null

```js
var oBox = document.getElementById("box");
console.log(oBox.getAttribute("width"));//null
```

