# jQuery

jQuery是一个javaScript库极大的简化了JavaScript编程

#### jQuery语法

jQuery语法是通过选取HTML元素，并对选取的元素执行某些操作

基础语法：

`$(selectot).action()`

美元符号定义jQuery

选择符（selectot）“查询”和“查找”HTML元素

jQuery的action（）执行对元素的操作

`$(this),hide()`  //隐藏当前元素

`$("p").hide()` //隐藏所有<p>元素

`$("p.test").hide()` //隐藏所有class="test"的<p元素

`$("#test").hide() //隐藏id="test"的元素`

文档就绪事件

`$(function(){`

​     `//jQuery代码`

`})`

#### jQuery选择器

jQuery选择器允许对HTML元素组或当个元素进行操作

jQuery选择器都以美元符号开头$()

##### 元素选择器

在页面中选取所有<p>元素：

`$("p")`

用户点击按钮后，所有<p>元素隐藏：

`$(function(){`

​	`$("button").click(function(){`

​		`$("p").hide();`

`});`

`});`

##### #id选择器

通过id选取元素语法：

$("#test")

用户点击按钮后有id="test"属性的元素将被隐藏

`$(function(){`

​	`$("button").click(function(){`

​		`$("test").hide();`

`});`

`});`

##### class选择器

通过class选择：

`$(".test")`

用户点击按钮所有的class="test"属性的元素都隐藏：

`$(function(){`

​	`$("button").click(function(){`

​		`$(".test").hide();`

`});`

`});`