# 1.javascrip的语法

​	js可以有几种写法：
​	1、写在script标签内
​	2、写在外部js文件里面
​	3、写在标签内部的
​	

js的数据类型
	数据类型 就是我可以了解到的是描述数据的类型
	
	js基本的数据类型 ：数字类型，字符串型，布尔型，undefined，null
	对象类型 数组 
	
	1、数字类型（Number）
	只有一种数字类型，数字 可以是小数 ，也可以的整数
	以0开头 默认使用8进制来表示我的这个数字
	以0x开头 默认使用16进制来表述我的这个数字
	如果以-开头 默认以负数
	如果我带有e：以科学计数法来解析我的这个数字
	2、字符串型（string）
	字符串是存储字符的变量，用来表示文本的数据类型，程序中的字符串是包含单引号/双引号的，由单引号来界定我双引号中包含的字符串 反过来
	es6模板字符串("`")
	
	3、布尔类型（boolean）
	
	一般是用在流程控制语句中，字符串和数字类型都是无穷多个，然而我们的布尔数据类型只有两个：true 和 false
	这两个个值一般用于说明某个事物是真或者假
	js一般用布尔类型来比较所得到的结果
	
	4、null（空）
	null
	关键字null是一个特殊的值，它表示变量为空值，用来定义空的或者是不存在的引用。
	如果试图去引用一个没有定义的值，就会返回一个null。
	这里注意一点：null并不等于"" 或者0
	
	5、undefined（未定义）
	这个值表示变量不含有值，没有定义的值，或者被定义了一个不存在的属性值
	！null和undefined区别：
	null它表示一个变量被赋予一个空值，而undefined是表示变量还没有被赋值


​	

数据类型间转换
	1、显示数据类型转换
		1.1转数字类型：
		1.Number()：可以将任意类型的参数转换为数字类型，遵循以下规则：
		1、如果它是一个布尔值true和false将被分别转成1和0；
		2、如果它是以个数字，返回它本身
		3、如果是null，返回0；
		4、如果是undefined，返回NaN
		5、如果是一个字符串：
			1）如果这个字符串只包含数字，则直接将它转成10进制数字（忽略前面的0）
			2）如果有有效浮点格式，将它转成一个浮点数值
			3）如果是空字符串,转换为0
			4）如果以上都不符合==>NaN
	
		2.parseInt(string,num):可以解析一个字符串，返回一个整数
		
		1）忽略字符串前面所有的空格，直到找到第一个非空字符为止
		2）如果第一个字符不是数字或者“-” 直接返回NaN
		3）如果第一个字符是数字，它解析到遇到的第一个不是数字的字符为止
		4）如果上面解析完结果是以0开头，就将它当成一个八进制来解析
		5）如果以0x开头，则当成十六进制来解析。
		6）如果我指定了num参数，那么 它就以num进制来解析
		
		3.转字符串类型：
		1.String():将任意的一个类型的值转换为字符串，遵循一下下规则：
		1）如果是null，==>"null"
		2)如果是undefined ==>"undefined"
		toFied(num):可以把Number类型四舍五入为指定小数位的字符串。
		返回的字符串，num保留小数位
		Boolean():如果这个值是空字符串（""）、数字零（0）、undefined或者null 会返回false，否则返回true
		!空格并不是空字符串
		
	2、隐式数据类型转换
		js的数据类型非常弱的，在使用算术运算符的时候,运算符两边的数据类型可以是任意的  这是因为js的引擎它在代码运行之前偷偷把数据类型进行转换 ，这种转换我们称之为隐式转换