# JavaScript

## 一、JavaScript基础

### 创建JavaScript

```html
<head>
    <script type="text/javascript">
	</script>
</head>

```

### 打印

```html
<script>
//打印在控制台
 console.log()
//打印在网页
 document.write()
//弹窗输出
 alert()
</script>
```

### 输入

```javascript
prompt('请输入')
```

## 二、变量

### 全局变量

```javascript
var name=n
```

### 局部变量

```javascript
let name =n
```

### 区别

 let在一个作用域内只能声明一次， var可以声明多次

## 三、

### 条件分支if-else

```javascript
if(1表达式：结果是布尔类型){

        }else if(2表达式：结果是布尔类型){

        }else if(3表达式：结果是布尔类型){
          
       }else{
        }
```

### while循环

```javascript
whlie(){
    
}
continue:跳出当前循环
break:跳出循环
```

### for循环

```javascript
for(let i=n;i>num;i++){
    
}
```

### Switch-case

```javascript
switch (值) {
            case n:
               
                break;
            case n:
               
                break;
            default:
                break;
        }

```

## 四、基本数据类型

### 类型

#### number

#### boolean

#### string:

##### 	字符串转义	

```javascript
\n:换行
\t:Tab键
\\:斜杠
\':单引号
\":双引号
```

##### 	string常用方法

```javascript
var js='javascript'
//获取字符串长度
js.length
//将英文字母转化为大写
js.toUpperCase
//将英文转化为小写
js.toLowerCase()
//截取字符串
substr(起始索引位置,截取字符串个数)
substring(起始索引位置,截止索引位置(不包含)) 
//拆分字符串
split()
//去除开头或者结尾的空格
trim()
//查看是否字符串中是否包含
includes()
indexof()
```



#### null

#### undefined(未定义)

### 查看数据类型

```javascript
typeof 变量名
```

### 数据类型转换

```javascript
Number():转换成数字，失败会变成 NaN(非数)
parseInt(str, radix):将（radix）进制 转成 10进制
parseFloat(string)
String(mix)
toString(radix)：将十进制数的a转换成radix进制的数
Boolean()

toFixed(n):保留小数点后n位
isNaN(): 是不是非数，是数字的话-->false  不是数字-->true
```

### 常用类

#### Math

```javascript
Math.min()

Math.max()
//向上取整
Math.ceil()
//向下取整
Math.floor()
//四舍五入
Math.round()
```

#### 随机函数

```javascript
random()
```

#### Time

```javascript
getFullYear()
getMonth()
getDate()
getHuours()

```

## 五、引用类型

### 数组

#### 创建数组

```javascript
//空数组
const arr = Array()
const arr = []
//带元素数组
const numbers =  [0, 3.14, 9.81, 37, 98.6, 100];
const fruits = ['banana', 'orange', 'mango', 'lemon']
const arr = [
    'Asabeneh',
    250,
    true,
    { country: 'Finland', city: 'Helsinki' },
    { skills: ['HTML', 'CSS', 'JS', 'React', 'Python'] }
]
//使用字符串的split方法创建数组
let js = 'JavaScript'
const charsInJavaScript = js.split('')
```

#### 增

```javascript
//在数组末尾添加元素
arr.push(4,5,6) 
//往数组头部添加元素
arr.unshift(4,5,6)
```

#### 删

```javascript
//删除第一个元素
arr.shift()
//删除最后一个元素
arr.pop()
```

#### 改

#### 查

#### 排序

```javascript
//根据字符串顺序排序，不是按照数字大小排序
arr.sort()
// 通过冒泡排序， 从小到大  从大到小
  const arr = [45,74,23,85,23,54,85];
  for(let i = 0; i<arr.length-1;i++){
       for(let j = 0; j<arr.length-1-i;j++){
                if(arr[j]>arr[j+1]){
                    [arr[j],arr[j+1]] = [arr[j+1],arr[j]]
                }
            }
        }
```



#### 数组中的常用方法

```javascript
//concat:将两个数组合并在一起
const firstList = [1, 2, 3]
const secondList = [4, 5, 6]
const thirdList = firstList.concat(secondList)
//查找数组的元素，返回-1表示不存在,与之相同的查找方法还有includes,lastindexof
indexof()
includes()
lastindexof
//将数组转成字符串
toString()
//添加指定字符后转成字符串 转成json常用
join()
//
slice()
//下标index开始，删除delecount个元素,再添加inservalue
splice(index,delecount,insertvalue)
//反转数组
reverse()
```

####  数组解构

```javascript
const arr = [1,2,3,4];
var a = arr[0];
var b = arr[1];
var d = arr[3];
var[a,...args] = arr;
```

#### 数组方法

```javascript
// concat:连接,数组合并
  const arr1=[1,2,3]
  const arr2=[4,5,6]
  const new_arr=arr1.concat(arr2)
//join:将数组转成字符串
  const arr=[1,2,3,4,5,6]
  arr.join('')-->123456
//将字符串转成数组 
  string.split('')
//遍历数组
  arr.forEach(
             //匿名函数  
             function (element){
                console.log(element);
            }
       )
//映射方法
   const arr = [1, 2, 3, 4, 5, 6]
        const arr1 = arr.map(
            function (element) {
                return element + 1
            }
        )
//filter过滤:返回数组中符合条件的数据
   const new_arr = arr.filter(
            function (e){
                return e%2==0  //true：返回 false：不符合条件=>过滤
            }
        )   
//reduce(),reduceRight():归纳汇总： 返回一个总数据
   var sum = arr.reduceRight(
            function (v1,v2){
                return v1*v2
            }
        )
//every():只要有一个不满足条件就返回false
// some():只要有一个满足条件就返回true
   var isMatch = arr.every(
            function (e){
                return e>1
            }
        )
```

## 六、函数

```javascript
//创建函数
function fn(){
    
}
//...args:剩余运算符,将剩下的参数都放进args数组中
//arguments:伪数组,只有长度，只能找下标和元素
```

#### 匿名函数

```javascript
  var aa=function (num) {
            var res = 1;  //result:定义积： 1*1*2*3*4*5
            for (var i = 1; i <= num; i++) {
                res *= i; //res = res * i
            }
            return res;
        }
```

#### 箭头函数

```javascript
var aa=(参数)=>{
    
}
```

## 七、对象

#### 创建对象

```javascript
var obj = new  GetName()
//字面量
	var obj {
        name=''
        age:
        make:function(){
            
        }
    }
//构造函数方式
	function obj(name,age){
        this,name=name;
        this.age=age;
    }
var Obj=new obj(name,age)
//工厂模式
     function GetValues(name,age) {
         var that = { }
         that.name = name
         that.age = age
         return that
     }
     var that = GetValues('张三',16)
     console.log(typeof that);
//原型模式
//混合模式
```

#### 增

```javascript
//obj.原来obj中没有的属性，相当于将该属性添加到对象obj中
        // obj.gender = '男'
        // console.log(obj);
```

#### 删

```javascript
//delete  一般删之前要对该属性进行判断 
        // delete obj.age
        // console.log(obj);
```

#### 改

#### 查

#### 对象解构

```javascript
     var obj = {
         name: '张三',
         age: 16,
         skill: {
             sname: 'Java',
             year: '2022'
         }
     }
     //取数据
     let{name:personName, age:personAge} = obj
     //嵌套解构
     let { name: personName, skill: { sname: skillName } } = obj
```

#### 对象方法

```javascript
//key
//values
//entries
//assign:浅拷贝
	//浅拷贝:复制的是对象的地址手写一个对象的深拷贝
	//深拷贝:复制的的对象本身
	Object.assign(对象一,对象二,对象三)
	
```

#### 包装类

#### for...of遍历

**`for...of`语句**在[可迭代对象](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Iteration_protocols)（包括 [`Array`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array)，[`Map`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Map)，[`Set`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Set)，[`String`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String)，[`TypedArray`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/TypedArray)，[arguments](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Functions/arguments) 对象等等）上创建一个迭代循环，调用自定义迭代钩子，并为每个不同属性的值执行语句

```javascript
for(变量 of 可迭代对象){
    
}
```

#### 标准内置对象

##### Map

对象保存键值对，并且能够记住键的原始插入顺序。任何值（对象或者[基本类型](https://developer.mozilla.org/zh-CN/docs/Glossary/Primitive)）都可以作为一个键或一个值。

```javascript
var map=new Map();
//增加
map.set(key,values)
//删除
map.delete(key)
//清空数据
map.clear()
//
for(let [k,v] of map){
    
}

///////
		//Map根据===运算符判断键是否相等，但NaN 是与 NaN 相等的

```

## 八、JS预编译

### 全局预编译 GO

- 全局预编译发生在页面加载完成时执行
- GO创建步骤

```javascript
var x = 1,
    y = z = 0;

function add (n) {
  return n = n + 1;
}

y = add(x);
function add (n) {
  return n = n + 3;
}

z = add(x)
//1.创建GO(Global Object，全局执行期上下文，在浏览器中为window)对象
	GO{}
//2.寻找var变量声明，并赋值维undefined
	GO{
        x:undefined
        y:undefined
        z:undefined
    }
//3.寻找function函数声明，并赋值函数体
	GO{
        x:undefined
        y:undefined
        z:undefined
        add: function add (n) { return n = n + 1; } => function add (n) { return n = n + 3;
    }	
//4.执行代码
        GO{
        x:undefined
        y:undefined
        z:undefined
        add:function add (n) { return n = n + 3;
    }	

```

### 函数预编译AO

## 九、作用域链

## 十、闭包

### 闭包作用

实现公有变量 
可以做**缓存（存储结构**)
可以实现封装（继承）
模块化开发，防止污染全局变量

### 闭包缺点

当函数保存在外部时，将会生成闭包，闭包会导致原有作用域链不释放，从而造成内存泄漏

## 十一、call apply bind

```javascript
function Person(name, age) {
      this.name = name;
      this.age = age; 
        }
//call apply
Person.call(obj,'张三',18);
Person.apply(obj, ['张三', 18]);
const module = {
    x: 42,
    getX: function () {
        console.log(this);
        return this.x;  //window.x
    }
};
//bind
var x=99;
const unboundGetX = module.getX;
        console.log(unboundGetX()); //99
        const boundGetX = unboundGetX.bind(module);//函数调用bind方法，返回一个带this指向的新函数
        console.log(boundGetX());
```



## 十二、继承

### 原型链继承

会将父级上的所有属性和方法统统继承过来：过多地继承没用的属性

```javascript
function Grand() {
    tskill = '拉二胡'
}  //爷爷
function Parent() {
    mskill = '吃饭'
} //爸爸
	var grand = new Grand();
	Parent.prototype = grand;//必须写在子类实例化之前 
	var parent = new Parent();
	Parent.prototype.mskill = '做饭';//子类添加属性或方法必须在继承(原型重写之后)
```

### 构造函数继承

```javascript
function Parent(name,age){
    this.name = name; //child.name
    this.age = age; //child.age
}
function Child(name,age,sex){
    //call
    Parent.call(this,name,age);
    this.sex = sex;
}
var child = new Child('张三',18,'male');
console.log(child.name); 
```

### 共享原型继承

```javascript
//当子类继承父类的属性或行为发生改变时，父类与之发生相同的改变
function Grand(){
    Grand.prototype.name = '爷爷';
}
function Parent(){
    Parent.prototype.name = '爹';
	Parent.prototype.age = 40;
}
function Child(){
}
function inherit(Target,Origin){
    Target.prototype = Origin.prototype
}
inherit(Child,Parent);
Child.prototype.age = 100; //共享
var child = new Child();
console.log(child.name);
console.log(child.age);
var parent = new Parent();
console.log(parent.age);
```

### 圣杯继承

```javascript
function Parent(){
    Parent.prototype.name = '爹';
	Parent.prototype.age = 40;
}
function Child(){
}
function inherit(Target, Origin) {
    function F() { } //临时构造函数
    F.prototype = Origin.prototype;
    // var f = new F();//实例化:复制一份
    // Target.prototype = f; //独立的f，里面有父类所有的属性和方法
    Target.prototype = new F();
    //归位
    //构造器归位
    Target.prototype.constructor = Target;
    //超类
    Target.prototype.uber = Origin;
        }
inherit(Child, Parent);//执行方法
```

