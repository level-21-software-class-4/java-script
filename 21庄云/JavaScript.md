# JavaScript

## 一、JavaScript基础

### 创建JavaScript

```html
<head>
    <script type="text/javascript">
	</script>
</head>

```

### 打印

```html
<script>
//打印在控制台
 console.log()
//打印在网页
 document.write()
//弹窗输出
 alert()
</script>
```

### 输入

```javascript
prompt('请输入')
```

## 二、变量

### 全局变量

```javascript
var name=n
```

### 局部变量

```javascript
let name =n
```

### 区别

 let在一个作用域内只能声明一次， var可以声明多次

## 三、

### 条件分支if-else

```javascript
if(1表达式：结果是布尔类型){

        }else if(2表达式：结果是布尔类型){

        }else if(3表达式：结果是布尔类型){
          
       }else{
        }
```

### while循环

```javascript
whlie(){
    
}
continue:跳出当前循环
break:跳出循环
```

### for循环

```javascript
for(let i=n;i>num;i++){
    
}
```

### Switch-case

```javascript
switch (值) {
            case n:
               
                break;
            case n:
               
                break;
            default:
                break;
        }

```

## 四、基本数据类型

### 类型

#### number

#### boolean

#### string:

##### 	字符串转义	

```javascript
\n:换行
\t:Tab键
\\:斜杠
\':单引号
\":双引号
```

##### 	string常用方法

```javascript
var js='javascript'
//获取字符串长度
js.length
//将英文字母转化为大写
js.toUpperCase
//将英文转化为小写
js.toLowerCase()
//截取字符串
substr(起始索引位置,截取字符串个数)
substring(起始索引位置,截止索引位置(不包含)) 
//拆分字符串
split()
//去除开头或者结尾的空格
trim()
//查看是否字符串中是否包含
includes()
indexof()
```



#### null

#### undefined(未定义)

### 查看数据类型

```javascript
typeof 变量名
```

### 数据类型转换

```javascript
Number():转换成数字，失败会变成 NaN(非数)
parseInt(str, radix):将（radix）进制 转成 10进制
parseFloat(string)
String(mix)
toString(radix)：将十进制数的a转换成radix进制的数
Boolean()

toFixed(n):保留小数点后n位
isNaN(): 是不是非数，是数字的话-->false  不是数字-->true
```

### 常用类

#### Math

```javascript
Math.min()

Math.max()
//向上取整
Math.ceil()
//向下取整
Math.floor()
//四舍五入
Math.round()
```

#### 随机函数

```javascript
random()
```

#### Time

```javascript
getFullYear()
getMonth()
getDate()
getHuours()

```

## 五、引用类型

### 数组

#### 创建数组

```javascript
//空数组
const arr = Array()
const arr = []
//带元素数组
const numbers =  [0, 3.14, 9.81, 37, 98.6, 100];
const fruits = ['banana', 'orange', 'mango', 'lemon']
const arr = [
    'Asabeneh',
    250,
    true,
    { country: 'Finland', city: 'Helsinki' },
    { skills: ['HTML', 'CSS', 'JS', 'React', 'Python'] }
]
//使用字符串的split方法创建数组
let js = 'JavaScript'
const charsInJavaScript = js.split('')
```

#### 增

```javascript
//在数组末尾添加元素
arr.push(4,5,6) 
//往数组头部添加元素
arr.unshift(4,5,6)
```

#### 删

```javascript
//删除第一个元素
arr.shift()
//删除最后一个元素
arr.pop()
```

#### 改

#### 查

#### 排序

```javascript
//根据字符串顺序排序，不是按照数字大小排序
arr.sort()
// 通过冒泡排序， 从小到大  从大到小
  const arr = [45,74,23,85,23,54,85];
  for(let i = 0; i<arr.length-1;i++){
       for(let j = 0; j<arr.length-1-i;j++){
                if(arr[j]>arr[j+1]){
                    [arr[j],arr[j+1]] = [arr[j+1],arr[j]]
                }
            }
        }
```



#### 数组中的常用方法

```javascript
//concat:将两个数组合并在一起
const firstList = [1, 2, 3]
const secondList = [4, 5, 6]
const thirdList = firstList.concat(secondList)
//查找数组的元素，返回-1表示不存在,与之相同的查找方法还有includes,lastindexof
indexof()
includes()
lastindexof
//将数组转成字符串
toString()
//添加指定字符后转成字符串 转成json常用
join()
//
slice()
//下标index开始，删除delecount个元素,再添加inservalue
splice(index,delecount,insertvalue)
//反转数组
reverse()
```

####  数组解构

```javascript
const arr = [1,2,3,4];
var a = arr[0];
var b = arr[1];
var d = arr[3];
var[a,...args] = arr;
```

#### 数组方法

```javascript
// concat:连接,数组合并
  const arr1=[1,2,3]
  const arr2=[4,5,6]
  const new_arr=arr1.concat(arr2)
//join:将数组转成字符串
  const arr=[1,2,3,4,5,6]
  arr.join('')-->123456
//将字符串转成数组 
  string.split('')
//遍历数组
  arr.forEach(
             //匿名函数  
             function (element){
                console.log(element);
            }
       )
//映射方法
   const arr = [1, 2, 3, 4, 5, 6]
        const arr1 = arr.map(
            function (element) {
                return element + 1
            }
        )
//filter过滤:返回数组中符合条件的数据
   const new_arr = arr.filter(
            function (e){
                return e%2==0  //true：返回 false：不符合条件=>过滤
            }
        )   
//reduce(),reduceRight():归纳汇总： 返回一个总数据
   var sum = arr.reduceRight(
            function (v1,v2){
                return v1*v2
            }
        )
//every():只要有一个不满足条件就返回false
// some():只要有一个满足条件就返回true
   var isMatch = arr.every(
            function (e){
                return e>1
            }
        )
```

## 六、函数

```javascript
//创建函数
function fn(){
    
}
//...args:剩余运算符,将剩下的参数都放进args数组中
//arguments:伪数组,只有长度，只能找下标和元素
```

#### 匿名函数

```javascript
  var aa=function (num) {
            var res = 1;  //result:定义积： 1*1*2*3*4*5
            for (var i = 1; i <= num; i++) {
                res *= i; //res = res * i
            }
            return res;
        }
```

#### 箭头函数

```javascript
var aa=(参数)=>{
    
}
```

## 七、对象

#### 创建对象

```javascript
var obj = new  GetName()
//字面量
	var obj {
        name=''
        age:
        make:function(){
            
        }
    }
//构造函数方式
	function obj(name,age){
        this,name=name;
        this.age=age;
    }
var Obj=new obj(name,age)
//工厂模式
     function GetValues(name,age) {
         var that = { }
         that.name = name
         that.age = age
         return that
     }
     var that = GetValues('张三',16)
     console.log(typeof that);
//原型模式
//混合模式
```

#### 增

```javascript
//obj.原来obj中没有的属性，相当于将该属性添加到对象obj中
        // obj.gender = '男'
        // console.log(obj);
```

#### 删

```javascript
//delete  一般删之前要对该属性进行判断 
        // delete obj.age
        // console.log(obj);
```

#### 改

#### 查

#### 对象解构

```javascript
     var obj = {
         name: '张三',
         age: 16,
         skill: {
             sname: 'Java',
             year: '2022'
         }
     }
     //取数据
     let{name:personName, age:personAge} = obj
     //嵌套解构
     let { name: personName, skill: { sname: skillName } } = obj
```

#### 对象方法

```javascript
//key
//values
//entries
//assign:浅拷贝
	//浅拷贝:复制的是对象的地址手写一个对象的深拷贝
	//深拷贝:复制的的对象本身
	Object.assign(对象一,对象二,对象三)
	
```

#### 包装类

#### for...of遍历

**`for...of`语句**在[可迭代对象](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Iteration_protocols)（包括 [`Array`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array)，[`Map`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Map)，[`Set`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Set)，[`String`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/String)，[`TypedArray`](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/TypedArray)，[arguments](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Functions/arguments) 对象等等）上创建一个迭代循环，调用自定义迭代钩子，并为每个不同属性的值执行语句

```javascript
for(变量 of 可迭代对象){
    
}
```

#### 标准内置对象

##### Map

对象保存键值对，并且能够记住键的原始插入顺序。任何值（对象或者[基本类型](https://developer.mozilla.org/zh-CN/docs/Glossary/Primitive)）都可以作为一个键或一个值。

```javascript
var map=new Map();
//增加
map.set(key,values)
//删除
map.delete(key)
//清空数据
map.clear()
//
for(let [k,v] of map){
    
}

///////
		//Map根据===运算符判断键是否相等，但NaN 是与 NaN 相等的

```

## 八、JS预编译

### 全局预编译 GO

- 全局预编译发生在页面加载完成时执行
- GO创建步骤

```javascript
var x = 1,
    y = z = 0;

function add (n) {
  return n = n + 1;
}

y = add(x);
function add (n) {
  return n = n + 3;
}

z = add(x)
//1.创建GO(Global Object，全局执行期上下文，在浏览器中为window)对象
	GO{}
//2.寻找var变量声明，并赋值维undefined
	GO{
        x:undefined
        y:undefined
        z:undefined
    }
//3.寻找function函数声明，并赋值函数体
	GO{
        x:undefined
        y:undefined
        z:undefined
        add: function add (n) { return n = n + 1; } => function add (n) { return n = n + 3;
    }	
//4.执行代码
        GO{
        x:undefined
        y:undefined
        z:undefined
        add:function add (n) { return n = n + 3;
    }	

```

### 函数预编译AO

## 九、作用域链

## 十、闭包

### 闭包作用

实现公有变量 
可以做**缓存（存储结构**)
可以实现封装（继承）
模块化开发，防止污染全局变量

### 闭包缺点

当函数保存在外部时，将会生成闭包，闭包会导致原有作用域链不释放，从而造成内存泄漏

## 十一、call apply bind

```javascript
function Person(name, age) {
      this.name = name;
      this.age = age; 
        }
//call apply
Person.call(obj,'张三',18);
Person.apply(obj, ['张三', 18]);
const module = {
    x: 42,
    getX: function () {
        console.log(this);
        return this.x;  //window.x
    }
};
//bind
var x=99;
const unboundGetX = module.getX;
        console.log(unboundGetX()); //99
        const boundGetX = unboundGetX.bind(module);//函数调用bind方法，返回一个带this指向的新函数
        console.log(boundGetX());
```



## 十二、继承

### 原型链继承

会将父级上的所有属性和方法统统继承过来：过多地继承没用的属性

```javascript
function Grand() {
    tskill = '拉二胡'
}  //爷爷
function Parent() {
    mskill = '吃饭'
} //爸爸
	var grand = new Grand();
	Parent.prototype = grand;//必须写在子类实例化之前 
	var parent = new Parent();
	Parent.prototype.mskill = '做饭';//子类添加属性或方法必须在继承(原型重写之后)
```

### 构造函数继承

```javascript
function Parent(name,age){
    this.name = name; //child.name
    this.age = age; //child.age
}
function Child(name,age,sex){
    //call
    Parent.call(this,name,age);
    this.sex = sex;
}
var child = new Child('张三',18,'male');
console.log(child.name); 
```

### 共享原型继承

```javascript
//当子类继承父类的属性或行为发生改变时，父类与之发生相同的改变
function Grand(){
    Grand.prototype.name = '爷爷';
}
function Parent(){
    Parent.prototype.name = '爹';
	Parent.prototype.age = 40;
}
function Child(){
}
function inherit(Target,Origin){
    Target.prototype = Origin.prototype
}
inherit(Child,Parent);
Child.prototype.age = 100; //共享
var child = new Child();
console.log(child.name);
console.log(child.age);
var parent = new Parent();
console.log(parent.age);
```

### 圣杯继承

```javascript
function Parent(){
    Parent.prototype.name = '爹';
	Parent.prototype.age = 40;
}
function Child(){
}
function inherit(Target, Origin) {
    function F() { } //临时构造函数
    F.prototype = Origin.prototype;
    // var f = new F();//实例化:复制一份
    // Target.prototype = f; //独立的f，里面有父类所有的属性和方法
    Target.prototype = new F();
    //归位
    //构造器归位
    Target.prototype.constructor = Target;
    //超类
    Target.prototype.uber = Origin;
        }
inherit(Child, Parent);//执行方法
```

## 十三、异常

```javascript
try{
    //放置可能出错的代码
}catch(error){
    //对捕获到的错误进行处理
}finall{
   //无论是否有捕获到错误，始终执行的代码
}
/////每次只能捕获一次异常
```

## 十四、正则表达式

### 创建正则表达式

```javascript
1.字面量:var re=/.../;
2.构造函数:var re=new RegExp(...)
```

### 编写一个正则表达式的模式

```javascript
//简单模式:仅能匹配ABC按照顺序出现的情况
/abc/.exec( "Hi, do you know your abc's?")
//使用特殊字符
^:/^a/--以a为开头
$:/a$/--以为结尾
\b:/oo\b/ --以oo为结尾，例moon->flase moo->true
x(?=y):x被y跟随时匹配x
x(?!y):x没有被y紧跟随时匹配x
(?<=y)x:x跟随y的情况下匹配x
(?<!y)x:x不跟随y时匹配x
.:匹配除行终止符之外的任何单个字符
\d:匹配任何数字 (阿拉伯数字)==>[0-9]
\D:匹配任何非数字 (阿拉伯数字) 的字符==>[^0-9]
\w:	匹配基本拉丁字母中的任何字母数字字符，包括下划线==> [A-Za-z0-9_]
\W:匹配任何不是来自基本拉丁字母的单词字符
\s:匹配单个空白字符，包括空格、制表符、换页符、换行符和其他 Unicode 空格==>[ \f\n\r\t\v\u00a0\u1680\u2000-\u200a\u2028\u2029\u202f\u205f\u3000\ufeff]
\S:匹配除空格以外的单个字符==> [^ \f\n\r\t\v\u00a0\u1680\u2000-\u200a\u2028\u2029\u202f\u205f\u3000\ufeff]                              \t:匹配水平制表符
\r:匹配回车符。
\n:匹配换行符。
\v：匹配垂直制表符。
\f：匹配换页符。
\0:匹配null字符
|:x|y--匹配 "x" 或 "y" 任意一个字符
*:x*--匹配0次或更多次
+:x+--匹配1次或更多次
?:x?--匹配0次或1次
{n,m}：最少匹配n次，最多匹配m次
[xyz]:代表x或y或z
():分组 -->$1,$2引用分组的值
----------
g:/.../g全局匹配
i:/.../i不区分大小写
m:/.../m换行//
```

![](https://gitee.com/snailclass/tuchuang1/raw/master/img/1470710362109508-2022-11-217:10:37.gif)

## 十五、BOM浏览器对象模型

浏览器对象模型主要包括window、history、location和document等对象，其中window对象是整个BOM的顶层对象。

### windows对象

```javascript
//document:窗口中当前显示的文档对象
//history:保存窗口最近加载的URL
//location:当前窗口的URL
----------------------
常见使用方法
prompt():显示可提示用户输入的对话框
alert():显示带有一个提示消息和一个确定按钮的警示框
confirm():显示一个带有提示信息、确定和取消按钮的确认框
close():关闭浏览器窗口
open():打开一个新的浏览器窗口，加载给定URL所指定的文档
setTimeout():在设定的毫秒数后调用函数或计算表达式；执行一次
setInterval():按照设定的周期（以毫秒计）来**重复调用**函数或表达式
clearInterval():取消重复设置，与setInterval对应
```

#### setTimeout实现抽奖功能

```javascript
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>setTimeout实现抽奖功能</title>
		<style type="text/css">
			#myTable{margin: 0px auto; width: 500px; height: 500px; }
			#myTable td{border: solid 1px red; width: 100px; height: 100px; 
			text-align:center; font-weight: bold;}
		</style>
		<script type="text/javascript">
			//alert(Math.random());
			function GetLuckyResult()
			{
				var num = Math.ceil(Math.random()*25);
				var obj = document.getElementById("td" + num);
				obj.style.backgroundColor = "#FF0000";
				obj.style.color = "black";
				document.getElementById("info").innerHTML = obj.innerHTML;
			}		
			function MyStart()
			{
				document.getElementById("info").innerHTML = "正在抽奖中...";
				for(var i = 1;i<=25;i++)
				{
					var obj = document.getElementById("td" + i);
					obj.style.backgroundColor = "white";
					obj.style.color = "black";
				}
				setTimeout("GetLuckyResult()",3000);
			}
		</script>
	</head>
	<body>
		<div style="text-align: center;">
			<input type="button" value="开始抽奖" onclick="MyStart();">
			<span id="info"></span>
		</div>
		<br/>
		<table id="myTable">
			<tr>
				<td id="td1">谢谢参与</td>
				<td id="td2">谢谢参与</td>
				<td id="td3">谢谢参与</td>
				<td id="td4">谢谢参与</td>
				<td id="td5">谢谢参与</td>
			</tr>
			<tr>
				<td id="td6">谢谢参与</td>
				<td id="td7">谢谢参与</td>
				<td id="td8">谢谢参与</td>
				<td id="td9">谢谢参与</td>
				<td id="td10">谢谢参与</td>
			</tr>
			<tr>
				<td id="td11">谢谢参与</td>
				<td id="td12">谢谢参与</td>
				<td id="td13">500万大奖</td>
				<td id="td14">谢谢参与</td>
				<td id="td15">谢谢参与</td>
			</tr>
			<tr>
				<td id="td16">谢谢参与</td>
				<td id="td17">谢谢参与</td>
				<td id="td18">谢谢参与</td>
				<td id="td19">谢谢参与</td>
				<td id="td20">谢谢参与</td>
			</tr>
			<tr>
				<td id="td21">谢谢参与</td>
				<td id="td22">谢谢参与</td>
				<td id="td23">谢谢参与</td>
				<td id="td24">谢谢参与</td>
				<td id="td25">谢谢参与</td>
			</tr>
		</table>
	</body>
</html>
```

#### setTimeout实现时间的延时切换图片

```javascript
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>setTimeout实现时间的延时切换图片</title>
		<script type="text/javascript">
			function changeImg()
			{
				document.getElementById("myImg").src = "img/knfd.jpg";
			}
			setTimeout("changeImg()",3000);
		</script>
	</head>
	<body>
		<img id="myImg" src="img/536.jpg" width="300" height="300">
	</body>
</html>
```

#### setInterval实现抽奖功能

```javascript
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>setInterval实现抽奖功能</title>
		<style type="text/css">
			#myTable{margin: 0px auto; width: 500px; height: 500px; }
			#myTable td{border: solid 1px red;
			width: 100px; height: 100px; 
			text-align:center; font-weight: bold;}
		</style>
		//JS代码
	</head>
	<body>
		<div style="text-align: center;">
			<input type="button" value="开始抽奖" onclick="MyStart();">
			<span id="info"></span>
		</div>
		<br/>
		<table id="myTable">
			<tr>
				<td id="td1">谢谢参与</td>
				<td id="td2">谢谢参与</td>
				<td id="td3">谢谢参与</td>
			</tr>
			<tr>
				<td id="td4">谢谢参与</td>
				<td id="td5">iPhone14</td>
				<td id="td6">谢谢参与</td>
			</tr>
			<tr>
				<td id="td7">谢谢参与</td>
				<td id="td8">谢谢参与</td>
				<td id="td9">谢谢参与</td>
			</tr>
		</table>
	</body>
</html>
```



### history对象

```javascript
back():后退一个页面，相当于浏览器后退按钮
forward():前进一个页面，相对于浏览器前进按钮
go():打开一个指定位置的页面
```

#### history对象实现网页的前进后退

```js
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title></title>
	</head>
	<body>
		<h1>我是第一个页面</h1>
		<a href="Demo04_02.html">我要跳转到第二个页面</a>
	</body>
</html>
```

```js
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title></title>
	</head>
	<body>
		<h1>我是第二个页面</h1>
		<a href="Demo04_03.html">我要跳转到第三个页面</a>
		<br/><br/>
		<a href="javascript:history.back();">后退</a>
		<br/><br/>
		<a href="javascript:history.forward();">前进</a>
	</body>
</html>
```

```js
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>history对象</title>
	</head>
	<body>
		<h1>我是第三个页面</h1>
		<br/><br/>
		<a href="javascript:history.back();">后退</a>
		<br/><br/>
		<a href="javascript:history.go(-2);">后退二步</a>
	</body>
</html>
```

### location对象

```javascript
href属性:获取或设置网页地址
reload方法:重新加载当前页面，相当于浏览器的刷新按钮
```

#### location对象实现页面刷新和跳转

```js
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>location对象</title>
		<script type="text/javascript">
			function MyRefresh()
			{
				location.reload();
			}
			function ChangeUrl()
			{
				var objValue = document.getElementById("webUrl").value;
				if(objValue.length == 0)
					return;
				location.href = objValue;
			}
		</script>
	</head>
	<body>
		<input type="button" value="刷新" onclick="MyRefresh();" />
		<select id="webUrl" onchange="ChangeUrl();">
			<option value="">--请选择--</option>
			<option value="http://www.baidu.com">百度</option>
			<option value="http://www.alibaba.com">阿里巴巴</option>
			<option value="http://www.qq.com">腾讯</option>
		</select>
	</body>
</html>
```

## 十六、DOM文档对象模型

### DOM常用API

#### 文档级就节点

document:DOM中最大的节点

document.documentElement

document.head

document.title（可修改）：获取标题

document.body

#### 子节点

##### 文本节点+元素节点

document.body.childNodes//返回nodelist类数组

document.body.childElementCount//返回元素子节点的长度

document.body.hasChildNodes()//判断是否有子节点:返回是否有子节点，有返回true，没有返回false

|           | 元素节点 | 属性节点 | 文本节点 |  注释节点  |
| :-------: | :------: | :------: | :------: | :--------: |
| nodeType  |    1     |    2     |    3     |     8      |
| nodeName  |  元素名  |  属性名  |  #text   |  #comment  |
| nodeValue |   null   |  属性值  |  文本值  | 注释的文本 |
|           |          |          |          |            |

##### 父子兄弟节点的访问

```javascript
//返回第一个子节点
document.body.firstChild
//返回第一个元素子节点
document.body.firstElementChild
//返回最后一个子节点
document.body.lastChild
// 返回最后一个元素子节点
document.body.lastElementChild
//下一个兄弟节点(包含文本节点);
document.body.children[0].nextSibling;
//下一个元素兄弟节点
document.body.children[0].nextElementSibling;
//上一个兄弟节点(包含文本节点);
document.body.children[1].previousSibling;
//上一个元素兄弟节点
document.body.children[1].previousElementSibling;

```

### 元素访问方法（DOM 0)

```javascript
//根据标签名
document.getElementsByTagName('标签名')
//根据class属性
document.getElementsByClassName('classname')
//根据id属性
document.getElementById('ianame')
//根据name属性
document.getElementsByName('name')
```

### 元素访问方法 (DOM1)

```javascript
//querySelector(CSS选择器)方法返回文档中匹配指定CSS选择器的一个元素
--document.querySelector('aaa')
 //querySelectorAll(CSS选择器)方法返回文档中匹配的所有CSS选择器的元素
--document.querySelectorAll('aaa')

```

### DOM创建节点&添加节点&删除节点&克隆节点

```javascript
//创建节点
--创建元素节点
var newNode = document.createElement('h2')-->父元素
--创建文本节点
var newTextNode = document.createTextNode('我是document创建的H2')-->子元素
//添加子元素
newNode.appendChild(newTextNode);
//删除
//替换
newTextNode.replaceChild()
//插入
.insertBefore()
//克隆:收参数，默认是false，表示浅拷贝，只拷贝标签，不拷贝内容
createTextNode()
```

### 获取属性节点的值

```javascript
```

### innerHTML与innerText

- innerHTML获取的是文本内容和标签，但是innerText是只获取文本内容

- innerHTML具备解析字符串的能力，但是innerText没有

## 十七、事件

### 事件绑定与取消

#### 传统事件

兼容性好，但是不能绑定多个同类型事件。

```js
<body>
    <div onclick="show(1)">我是div</div>
    <div onclick="show(2)">我是div2</div>
    <button id="btn1"></button>
    <script>
        //传统事件绑定1，耦合度比较高
        function show(num) {
            console.log('I am Div'+num);
        }
        var btn1 = document.getElementById('btn1');
        btn1.onclick = function () {
            console.log('我是传统事件绑定2');
        }
		//取消绑定
		btn.onclick = null
    </script>
</body>
```



#### 现代事件

绑定多个同类型事件时会依次执行，但是兼容性较差

###### addEventListener:DOM事件的绑定

```js
//绑定事件
var fn = fuction(){
    console.log(123)
}
//addEventListener(事件,处理函数,布尔值):支持ie9及以上的浏览器和其他浏览器
addEventListener('click',fn)
//取消事件
	//DOM
	btn.removeEventListener('click',fn);
	//IE
	btn.detachEvent('onclick',fn);
```

###### attachEvent:IE事件绑定

```js
var fn = fuction(){
    console.log(123)
}
//attachEvent(事件,fn,布尔值):支持IE10及以下的浏览器
attachEvent('onclick',fn)
```

### 事件流

事件有三个阶段，首先发生的是捕获阶段，然后是目标阶段，最后才是冒泡阶段，对于捕获和冒泡，我们只能干预其中的一个，通常来说，我们可能会干预事件冒泡阶段，而不去干预事件捕获阶段。

![img](https://img-blog.csdn.net/20170924010136220?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvYWxpZ2h0bWFu/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/Center)

**事件冒泡**:微软提出了名为**事件冒泡**的事件流。事件按照从最特定的事件目标到最不特定的事件目标(document对象)的顺序触发。可以想象把一颗石头投入水中，泡泡会一直从水底冒出水面。也就是说，事件会从最内层的元素开始发生，一直向上传播，直到document对象。

**事件捕获**:网景提出另一种事件流名为**事件捕获**。事件从最不精确的对象(document 对象)开始触发，然后到最精确(也可以在窗口级别捕获事件，不过必须由开发人员特别指定)，与事件冒泡相反，事件会从最外层开始发生，直到最具体的元素。同样形象的比喻一下可以想象成警察逮捕屋子内的小偷，就要从外面一层层的进入到房子内

#### 事件阻止冒泡

```js
ev.stopPropagation(); //不支持ie9以下 DOM
event.cancelBubble = true; //ie独有 ie

封装取消冒泡的函数：stopBubble(event)
```

但是有些事件本身是不冒泡的：（表单事件）focus,blur,change,submit,reset,select 等事件不冒泡

### 事件对象

### 事件委托（代理）

​		把原本需要绑定在子元素的响应事件（click、keydown…）委托给父元素，让父元素担当事件[监听](https://so.csdn.net/so/search?q=监听&spm=1001.2101.3001.7020)的职务。事件代理的原理是DOM元素的事件冒泡。

​		利用事件冒泡，和事件源对象进行处理。

### 阻止默认行为

​		默认事件：表单提交，a标签跳转，右键菜单等

```js
return false; //以对象属性的方式注册的事件才生效  DOM0



ev.perventDefault();//阻止默认行为 DOM浏览器, ie9以下不支持
ev.returnValue = false //阻止默认行为 DOM与IE

//封装阻止默认事件的函数:cancelHandler(event);

//阻止鼠标右键弹出菜单
<script>
        document.oncontextmenu = ()=>{
            console.log('a');
            //ev.perventDefault();
             //ev.returnValue = false
            // return false;
        }
    </script>


//阻止标签跳转
<a href="javascript:void(false)">测试</a>
<script>
        var a = document.getElementsByTagName('a')[0];
        a.onclick = (ev)=>{
            return false;
            ev.perventDefault();
            ev.returnValue = false;
        }
    </script>

```

### JS常用事件

###### 1.点击事件

onclick：单击事件

ondblclick：双击事件

###### 2.焦点事件

onblur：失去焦点

onfocus:元素获得焦点。

###### 3.加载事件

onload：一张页面或一幅图像完成加载。

###### 4.鼠标事件

onmousedown： 鼠标按钮被按下。

onmouseup： 鼠标按键被松开。

onmousemove： 鼠标被移动。

onmouseover： 鼠标移到某元素之上。

onmouseout ：鼠标从某元素移开。

mouseenter： 

mouseleave：

contextmenu:

鼠标按下时，通过事件对象 event中的属性 button 或 which 可以获取鼠标按键的编号e.button 事件对象中的 button属性可以获取鼠标按键的编号e.which 也可以获取鼠标的按键编号 0 左键 1滚轮 2右键

DOM3规定：click事件只能监听左键，只能通过mousedown和mouseup来判断鼠标键

如何解决mousedown和click的冲突

###### 5.键盘事件

onkeydown ：某个键盘按键被按下。

onkeyup： 某个键盘按键被松开。

onkeypress ：某个键盘按键被按下并松开。

###### 6.选择和改变

onchange ：域的内容被改变。

onselect ：文本被选中。

###### 7.表单事件

onsubmit ：确认按钮被点击。

onreset： 重置按钮被点击

## 十八、jQuery

 JQuery下载 https://www.jq22.com/jquery-info122

### 1.jQuery对象

​	**JQuery包装集对象**:简称JQuery对象，通过JQuery框架获取的对象，注意：只有JQuery对象才能使用JQuery提供的方法。

​	JQuery可以说是DOM对象的补充，JQuery规定：无论是一个还是一组，都封装成一个JQuery包装集，比如获取包含一个元素的JQuery包装集。

```html
var jqObj = $('#testDiv');
```

### 2.JQuery选择器

#### 基础选择器

|        选择器        |             名称              |                          举例                           |
| :------------------: | :---------------------------: | :-----------------------------------------------------: |
|       id选择器       |              #id              |           $('#testDiv'):选择id为testDiv的元素           |
| 元素名称(标签)选择器 |            element            |                $('div'):选择所有div元素                 |
|       类选择器       |            .class             |          $('.blue'):选择所有class=blue的的元素          |
|     选择所有元素     |               *               |                 $('*'):选择页面所有元素                 |
|      群组选择器      | selector1,selector2,selector3 | $('#testDiv, span, blue'):同时选择多个选择器匹配的元素. |
|                      |                               |                                                         |

#### 层次选择器

| 选择器     | 名称                | 举例                                               |
| ---------- | ------------------- | -------------------------------------------------- |
| 后代选择器 | ancestor descendant | $('#parent div'):选择id为parent的元素的所有div元素 |
| 子代选择器 | parent > child      | $('#parent>div'):选择id为parent的直接div子元素     |
| 相邻选择器 | prev + next         | $('.blue + img'):选择css类为blue的下一个img元素    |
| 同辈选择器 | prev ~sibling       | $('.blue ~ img'):选择css类为blue的之后的img元素    |

#### 属性选择器

| **选择器**         | 说明                                                         |
| ------------------ | ------------------------------------------------------------ |
| E[attr]            | 选择元素E，其中E元素必须带有attr属性                         |
| E[attr = “value”]  | 选择元素E，其中E元素的attr属性取值是value                    |
| E[attr!= “value”]  | 选择元素E，其中E元素的attr属性取值不是value                  |
| E[attr ^= “value”] | 选择元素E，其中E元素的attr属性取值是以“value”开头的任何字符  |
| E[attr $=“value”]  | 选择元素E，其中E元素的attr属性取值是以“value”结尾的任何字符  |
| E[attr *= “value”] | 选择元素E，其中E元素的attr属性取值是包含“value”的任何字符    |
| E[attr\|="value"]  | 选择元素E，其中E元素的attr属性取值等于“value”或者以“value”开头 |
| E[attr ~= “value”] | 选择元素E，其中E元素的attr属性取值等于“value”或者包含“value” |

#### 伪类选择器

##### 表单伪类选择器

| Forms          | 名称      | 举例                                                         |
| -------------- | --------- | ------------------------------------------------------------ |
| 表单选择器     | :input    | 查找所有input元素: $(':input');<br />注意：会匹配所有的input, textarea,select和button元素 |
| 文本框选择器   | :text     | 查找所有的文本框: $(':text')                                 |
| 密码框选择器   | :password | 查找所有密码框                                               |
| 单选按钮选择器 | :radio    | 查找所有单选按钮                                             |
| 复选框选择器   | :checkbox | 查找所有复选框                                               |
| 提交按钮选择器 | :submit   | 查找所有提交按钮                                             |
| 图像域选择器   | :image    | 查找所有图像域                                               |
| 重置按钮选择器 | :reset    | 查找所有重置按钮                                             |
| 按钮选择器     | :button   | 查找所有按钮                                                 |
| 文本域选择器   | :file     | 查找所有文本域                                               |

##### 表单属性伪类选择器

|   选择器   |                          说明                          |
| :--------: | :----------------------------------------------------: |
|  :checked  | 选取所有被选中的表单元素，一般是**单选框**或**复选框** |
| :selected  |     选取被选中的表单元素的选项，一般是**下拉列表**     |
|  :enabled  |                 选取所有可用的表单元素                 |
| :disabled  |                选取所有不可用的表单元素                |
| :read-only |               选取所有的表单元素除了只读               |
|   :focus   |               选取所有获得焦点的表单元素               |

### 2.过滤

```javascript
//类名过滤
$().hasClass(“类名”)
//下标过滤
$().eq(n)
//判断过滤
$().is()
//反向过滤
$().not()
//选择器过滤
$().filter()
//has方法
$().has()
```

### 4.事件

#### onload事件

在JavaScript中，onload表示文档加载完成后再执行的一个事件。window.onload只能调用一次，如果多次调用，则只会执行最后一个。

```html
window.onload = function(){
……
}

```

#### ready事件

​		在jQuery中，ready也表示文档（document）加载完成后再执行的一个事件。

```JavaScript
$(document).ready(function(){
	……
})
-->$(......
)

```

#### 鼠标事件

在jQuery中，常见的鼠标事件如下表:

| 事件      | 说明         |
| --------- | ------------ |
| click     | 鼠标单击事件 |
| dblclick  | 鼠标双击事件 |
| mouseover | 鼠标移入事件 |
| mouseout  | 鼠标移出事件 |
| mousedown | 鼠标按下事件 |
| mouseup   | 鼠标松开事件 |
| mousemove | 鼠标移动事件 |

```javascript
hover:mouseover+mousedown
$().hover{
    function(){
       //当鼠标移入时，触发此函数 
    }
    function(){
        //当鼠标移出时，触发此函数 
    }
}
```



#### 键盘事件

在jQuery中，常用的键盘事件共有2种。 

键盘按下：keydown 

键盘松开：keyup 

#### 表单事件

##### 1. focus和blur

focus表示获取焦点时触发的事件，而blur表示失去焦点时触发的事件，两者是相反操作。 

focus和blur这两个事件往往都是配合一起使用的。例如用户准备在文本框中输入内容时，此时它会获得 

光标，就会触发focus事件。当文本框失去光标时，就会触发blur事件。 

并不是所有的HTML元素都有焦点事件，具有“获取焦点”和“失去焦点”特点的元素只有两种。 

1. **表单元素（单选框、复选框、单行文本框、多行文本框、下拉列表）** 

2. **超链接** 

判断一个元素是否具有焦点很简单，我们打开一个页面后按Tab键，能够选中的就是带有焦点特性的元 

素。在实际开发中，焦点事件（focus和blur）一般用于单行文本框和多行文本框，其他地方比较少见。

##### 2. select

在jQuery中，当我们选中“单行文本框”或“多行文本框”中的内容时，就会触发select事件。

##### **3. change** 

在jQuery中，change事件常用于“具有多个选项的表单元素”。 

1. 单选框选择某一项时触发。 
2. 复选框选择某一项时触发。 
3. 下拉菜单选择某一项时触发。

### 5.jQuery事件对象

#### 事件绑定

```javascript
//直接绑定
$(selector).event(function(){})
```

### 6.jQuery获取内容和属性

```javascript
//设置或返回所选元素的文本内容
$().text
//设置或返回所选元素的内容（包括 HTML 标签）
$().html()
//设置或返回表单字段的值
$().val()
------------
//获取及设置属性值
$().attr(属性名，更改值)
```

### 7.添加元素

```javascript
//在被选元素的结尾插入内容
append()
//在被选元素的开头插入内容
prepend()
//在被选元素之后插入内容
after()
//在被选元素之前插入内容
before()
```

### 8.删除元素
