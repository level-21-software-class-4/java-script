### map&set

###### map

1.Map:键值对，类似于对象

​            键是唯一的

2.

/创建map对象
        //var map = new Map();
        

        //添加
        //map.set(1,'呼呼');
        //map.set(2,"拉拉");
        //console.log(map);
    
        //查看 v
        //console.log(map.get(1));
    
        //删除
        //map.delete(1);
        //console.log(map);
    
       //返回map存在的键的数组
       //console.log(map.keys());
       
       //返回map存在的值的数组
       //console.log(map.values());
    
       //返回map中存在的键值对的数组
       //console.log(map.entries());
    
       //查看键是否存在
       //console.log(map.has(1));
    
       //查看map的长度
       //console.log(map.size);

特殊情况：

```
// console.log(+0===-0);  //true
// console.log(Object.is(+0,-0));  //false
// console.log(Object.is(+0,0));


// let map = new Map();

// map.set(-0, 123);  //不管是+0,0,-0,当成了同一个键，
// map.get(+0);

```

 //map转数组 []

```
//const arr = [...map]
// const arr = []
// for(let e of map.entries()){
//   arr.push(e)
// }





//数组转map
// const arr = [['name', 'zoe'], ['age', 18]]
// var map = new Map([
//   ['name', 'zoe'], ['age', 18]
//   ])
```

###### set

对象类似于数组，且成员的值都是唯一的

去重
     

```
   // const arr = [1,3,3,5,7,9];
  // const arr2 = [...new Set(arr)];
 // console.log(arr2);
```



        //Map 和 Set 都不允许键重复
        //Set 不能通过迭代器来改变Set的值，因为Set的值就是键

set 转数组

```
const arr = [...set]
```

数组转set:数组去重

```
const arrs = [1,3,54,5,6,5,5,5,5,5,5,5]
        console.log(arrs.length);

        var sets = new Set(arrs);
```

3.WeakSet

WeakMap:弱引用(直接回收)，强引用(不会强制回收)

```
// var weakset = new WeakSet()

        // var name = 'Joe'
        // var map = new Map();
        // map.set(name,'Zoe')

        // var weakmap = new WeakMap();
        // weakmap.set(name,'Zoe')
```

