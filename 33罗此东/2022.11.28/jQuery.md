# jQuery 

##### 属性

###### arrt:

attr('属性名称')    获取指定的属性值，操作checkbox时选中返回checked，没有选中返回undefined。attr('checked')

###### prop:

prop('属性名称') 获取具有true和false两个属性的属性值prop('checked')

固有属性：

返回值是bool的属性: checked ,selected, disabled



##### 元素的增删改查

1.在目标元素添加子元素：$('目标元素').prepend($子元素);

1.1添加子元素到目标元素： $子元素.prependTo($('目标元素'))；

1.2添加子元素到目标元素：$子元素.appendTo($('目标元素'));

1.3添加同级元素：

在目标元素前插入新元素：$('目标元素').after($新元素)；

在目标元素后插入新元素：$('目标元素').before($新元素)；



2.删除整个元素：$('属性名').remove();

2.1删除选择属性的子元素，自身会保留：$('属性名').empty();

##### each

遍历元素

$(selector).each(function(index,element))

其中element ==this  为当前元素