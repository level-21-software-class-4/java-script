# JQuery

#### 事件对象

事件对象什么时候产生：触发事件的时候 this

###### 事件绑定

1.直接绑定

2.事件的链式调用，为一个对象绑定多个事件

```js
    $('button').mouseover(
        function () {
            $(this).css('color','red');
        }
    ).mouseout(
        function () {
            $(this).css('color','black');
        }
    ).click(
        function () {
            console.log('click');
        }
    )
```

3.bind():可以为多个事件绑定同一个函数

```js
$('button').bind('click   ',function () {
        console.log('这是bind');
    })
```

4.on():绑定

  on可以为动态添加的元素进行事件绑定

```js
 $('.newdiv').click(function () {
        var $btn = $('<button>创建的按钮</button>');
        $(this).append($btn);
        console.log('这是在div中');
    })
    
    $(document).on('click','button',function () {
        console.log('这是新创建的元素');
    })
```

5.one：只触发一次事件

  最后一个-1 倒数第二个-2

  $('button').eq(-1).one('click',function () {

​    alert('一次事件');

  })