**什么是 AJAX 请求** 

AJAX 即Asynchronous Javascript And XML（异步 JavaScript 和 XML），是指一种创建交互式网页应用的网页开发 

技术。

ajax 是一种浏览器通过 js 异步发起请求，局部更新页面的技术。 【这的异步指的是不需要等待服务器的响应用户即可接着操作】

Ajax 请求的局部更新，浏览器地址栏不会发生变化

局部更新不会舍弃原来页面的内容

**好处：**

- Ajax请求页面数据不会被丢弃，发生请求给服务器后服务器可以按照JSON数据交换格式返回数据，我们可以决定在页面的任何位置输出这些数据

- 页面不会有刷新的感觉，只有局部变化，然人看的也舒服
- **不会有页面的跳转**，【不会有页面跳转的感觉】普通的请求就会有这些感觉
- 不需要等待服务器的响应用户即可接着操作

**原生 AJAX 请求的示例：**

```js
<script type="text/javascript"> 
    // 在这里使用 javaScript 语言发起 Ajax 请求，
  
    javaScriptAjax function ajaxRequest() { 
    // 1、我们首先要创建 
    XMLHttpRequest var xmlhttprequest = new XMLHttpRequest(); 
    // 2、调用 open 方法设置请求参数 
    xmlhttprequest.open("GET","http://localhost:8080/16_json_ajax_i18n/ajaxServlet?action=javaScriptAj ax",true) //true表示异步，反之同步
    // 4、在 send 方法前绑定 onreadystatechange 事件，处理请求完成后的操作。 
        xmlhttprequest.onreadystatechange = function(){ 
            if (xmlhttprequest.readyState == 4 && xmlhttprequest.status == 200) { 
                var jsonObj = JSON.parse(xmlhttprequest.responseText); 
    // 把响应的数据显示在页面上 
                document.getElementById("div01").innerHTML = "编号：" + jsonObj.id + " , 姓名：" + jsonObj.name; 
            } 
        } 
    // 3、调用 send 方法发送请求 
    xmlhttprequest.send(); 
    } 
</script> 
```

