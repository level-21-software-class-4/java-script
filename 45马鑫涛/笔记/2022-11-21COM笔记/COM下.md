#  COM下

###### clone(克隆)

cloneNode() 方法可创建指定的节点的精确拷贝。

cloneNode() 方法 拷贝所有属性和值。

该方法将复制并返回调用它的节点的副本。

如果传递给它的参数是 true，它还将递归复制当前节点的所有子孙节点。

否则（也就是默认值，或者false），它只复制当前节点

###### checked

checked 是 和的一种属性，表示该项是不是被选择了。

1.objs[i].checked=true的意思就是该input元素的checked的值是true，就是该input被选中。 2.objs[i].checked=false的意思就是该input元素的checked的值是false，就是该input不被选中