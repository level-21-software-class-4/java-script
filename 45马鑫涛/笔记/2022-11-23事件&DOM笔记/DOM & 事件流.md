#  DOM & 事件流

###### DOM 0

当有多个同类型事件触发时，只会触发一个 ，兼容性较高(同名会被覆盖)

实例：

```
var btn = document.querySelector('button');
点击

 function aler() {

  btn.onclick = function () {

  alert('全是帅哥');

}

 }

优先级更高

 btn.onclick = function () {

  alert('全是学霸');

}
```

###### DOM 2

当有多个同类型事件触发时，会依次触发， 兼容性较差

##### 事件流

事件从最外层开始捕获，直到当前元素（触发事件的对象），再从当前元素向外冒泡到document

addEventListener('click',fn2,false)：默认false，表示冒泡 true：捕获