JQuery的优点： 1.代码简洁 2.完美兼容 3. 轻量级 4. 强大的选择器 5. 完善的AJAX 6. 丰富的插件 “简洁与高效”是jQuery最大的特点。 Dom与Jquery包装集对象 1.将DOM对象转成JQuery对象 var jqDiv = (*d**i**v*);(jqDiv).click( function () { alert('这是js包装集对象弹窗') }

```
2.jquery对象 -->dom对象
console.log($('.div')[0]);
```

JQuery选择器 基础选择器：标签选择器 $('div')

```
id:# class: . 

选择所有元素:document之下的
console.log($('*'));

群组选择器
console.log($('#box,.btn'));
层次选择器
    1.后代:所有后代（不止一代）的元素 $('#parent div')
    2.子代(只找后面一代)：$('#parent>div')
    3.相邻选择器:只能找后面一个元素
    4.同辈选择器:往后找多个
伪类选择器
    获取所有表单元素 $(':input')
    
    标签
     console.log($('input'));

    获取密码框
    console.log($(':password'));
    
    表单属性伪类选择器
    console.log($('input:checked'));
    console.log($(':selected'));
```