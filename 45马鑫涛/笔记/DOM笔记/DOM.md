# DOM

###### 节点

1.节点类型：元素节点、文本节点、属性节点

2.获取元素

```
根据id获取HTML元素：
document.getElementById("id名")

根据标签名获取HTML元素：
document.getElementsByTagName("标签名");

根据类名获取HTML元素：(返回HTMLCollection 数组)
document.getElementsByClassName("类名");

根据name属性获取元素对象: NodeList(伪数组)：只能获取下标和长度

console.log(document.getElementsByName('username'));
```

3.节点的增删改

例子：

```
<div id="box">
        <h2 id="h2">我是将被删除的h2</h2>  <!--替换的h3 -->
    </div>
    <script>
        var div = document.querySelector('#box');
        
        //创建元素节点
        var newH2 = document.createElement('h2');
        var txt = document.createTextNode('我是新添加的H2');
        newH2.appendChild(txt);
        // 添加子元素节点
        div.appendChild(newH2);

        //删除子节点:removeChild(元素对象:h2)
        // var h2 = document.getElementById('h2');
        // div.removeChild(h2);

        
        // var newH3 = document.createElement('h3');
        // var txt = document.createTextNode('替换的h3');
        // var h2 = document.getElementById('h2');
        // newH3.appendChild(txt);
        // //改:replaceChild
        // // div.replaceChild(newH3,h2);
        // //在...之前插入一个节点
        // div.insertBefore(newH3,h2);


    </script>
```

添加新节点：

```
 appendChild()：添加新元素到尾部。

insertBefore()：添加新元素到该元素之前。
```

删除节点：

```
删除HTML节点：removeChild()
```

替换：

```
replaceChild()
```