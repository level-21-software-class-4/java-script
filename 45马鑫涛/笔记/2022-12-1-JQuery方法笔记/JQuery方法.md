#  JQuery方法

eq()：跟据下标找元素,从0开始

$('').eq()

detach():保留事件

```
   var $btn = $('button').eq(2).detach();
        $('button').eq(1).click(function () {
            $(this).after($btn);
        })
```

clone(bool):返回一个复制元素,默认是false：只克隆元素。不克隆事件，参数为bool=true:表示将事件一起复制,

```
      $('button').eq(4).click(
            function () {
                var $btn3 = $('button').eq(2).clone(true);
                $(this).after($btn3);
            }
        )
```

replaceWith():替换jQuery对象

```
$('button').eq(3).replaceWith($('<button>新点击</button>'))
```