BOMB（Browser Object Model）即浏览器对象模型，它提供了独立于内容而与浏览器窗口进行交互 的对象，其核心 对象是 window。 BOM 由一系列相关的对象构成，并且每个对象都提供了很多方法与属性。 navigator.appName //得到浏览器名称 location.href //当前网页地址，修改可跳转到另一个页面 screen.width//屏幕宽 screen.height//屏幕高 history //保存网页浏览记录 history.back()//跳转到上一个网页 history.forward()//跳转到下一个网页 history.go(val) //val为-1 跳转到上一个页面,val为1 跳转到下一个页面

```
 window（顶层窗口，以上方法都属于window下）
 confirm("内容")确认框
 alert("内容")提示框
 prompt("显示内容","默认值")//输入对话框
 open("新窗口的地址","一般不写","窗口特征,宽度或高度")//打开一个新窗口
 close()//关闭窗口
 window.opener//得到创建这个窗口的窗口
定时器:
     setInterval("js代码",毫秒数)//每几毫秒执行一次来重复调用函数或表达式；
     clearInterval(定时器名称)//清除setInterval设置的定时器
     setTimeout("js代码",毫秒数)//延迟多少毫秒执行
     clearTimeout(定时器名称)//清除setTimeout设置的定时器
```