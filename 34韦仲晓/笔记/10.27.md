Map
Map 对象存有键值对，其中的键可以是任何数据类型。

Map 对象记得键的原始插入顺序。

Map 对象具有表示映射大小的属性。

//创建
var map = new Map(); --键值对，类似对象
//增
map.set(name,'jojo');
//删
    //delete:根据键删除,如果找到该键，就删除成功，返回true,否则返回false.
    map.delete(key):
    //clear:清空数据，无返回值
    var re = map.clear() 
//改
map.set(5, 'Joe')
//查
	//1 键
    for (let e of map.keys()){
        console.log(e);
    }
	//2 值
    for (let e of map.values()){
        console.log(e);
    }
	//3 返回一个数组的迭代对象
    for (let e of map.entries()){
        console.log(e);
    }
	//4 返回一个数组的迭代对象
    for (let e of map){
        console.log(e);
    }
//如何引用变量作为键--使用中括号
var name = 'FF'
var obj = {
    name:'DIO'	-- name:'DIO'
    [name]:'DIO'	--FF:'DIO'
}
//map会自动判断是变量或字符串
map.set(name,'BOBO')	--FF => 'BOBO'
//size	获取 Map 对象中某键的值。
obj.assign 合并对象
将所有可枚举的自有属性从一个或多个源对象复制到目标对象，返回修改后的对象。

const target = { a: 1, b: 2 };
const source = { b: 4, c: 5 };

const returnedTarget = Object.assign(target, source);

console.log(target);
// expected output: Object { a: 1, b: 4, c: 5 }

console.log(returnedTarget === target);
// expected output: true
set
//创建
var set = new Set();
//增
set.add(5);
//删
set.delete(5)
//set转数组
const arr = [...set]
//数组转set:数组去重
const arrs = [1,3,54,5,6,5,5,5,5,5,5,5]
//WeakSet WeakMap:弱引用(直接回收)，强引用(不会强制回收)
var weakset = new WeakSet()