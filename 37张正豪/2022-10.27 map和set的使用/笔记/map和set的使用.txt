从对象转为 Map ：new Map(Object.entries(obj))
从 Map 转为对象：Object.fromEntries(map.entries())

const AA = new set();//创建一个set对象
const a = “a”//创建一个新变量
AA.add(a)//向set添加值  add()添加值

typeof AA;  //返回 对象的类型

AA instanceof Set; //判断AA是否属于Set

clear();//删除Set中所有元素
delete();//删除指定元素
entries()	返回 Set 对象中值的数组
has()	如果值存在则返回 true
keys()	返回 Set 对象中值的数组
size	返回元素计数
