# 函数

- 无参函数
- 有参函数

```js
//有参(形参)
function fn(A,B,...args){
//arguments:伪数组
// for (let index = 0; index < arguments.length; index++) {
//     console.log(arguments[index]);
// }
console.log(A);
console.log(B);
console.log(args);        
}

var a = 4; //everyone makes mistakes  
fn(1,2,3,4,5,6,7,8,9);
```

- 匿名函数

```js
 //匿名函数： function (){}
function (num) {
    var res = 1;  //result:定义积： 1*1*2*3*4*5
    for (var i = 1; i <= num; i++) {
        res *= i; //res = res * i
    }
     return res;
}

```

- 函数表达式

```js
//表达式
var aa =  function(num){
    var res = 1;
    for(var i = 1; i <= num ; i++){
        res*=i;
    }
    return res;
}
var res = aa(num);
console.log(res);
console.log(typeof aa);  //number boolean array function(对象)


function fn(){
    //arguments:只有长度，只能找下标和元素
    //console.log(arguments);
}
fn(1,2,3,4,5,6,7,8,9,0)


```

- 箭头函数 (注意：无返回值,一般不用箭头函数 )

```js
//箭头函数：  var aa =   (形参列表)   =>  {函数体} arrow function
var aa = function (n1, n2) {
var sum = n1 + n2
return sum;
}
```

