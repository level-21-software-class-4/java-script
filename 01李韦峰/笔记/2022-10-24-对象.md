# 对象

- 对象：  类(人类，鱼类，鸟类)  
- 万物对象(  ): 属性和行为

## 1.1.1字面量表达式创建对象

```js
var obj = {
    name:{
        firstName:'李',
        lastName:'四'
    },
    age:18,
    'qq number': 1234560220
} 
```

## 1.1.2 属性:增删改查

```js
 //1.查 . 一级属性  二级属性往后[][]
console.log(obj.name);
console.log(obj.age);
//1.1查  使用 []
console.log(obj['name']['lastName']);  

//增 
// obj.原来obj中没有的属性，相当于将该属性添加到对象obj中
obj.gender = '男'
console.log(obj);

//删 delete  一般删之前要对该属性进行判断 
delete obj.age
console.log(obj);


```



## 1.2 构造函数方式

```js
//2.1  this{}  ：new
//2.2 this赋值 (自己操作)
// 例
function Students(name,age){
    this.name = name;
    this.age = age;
}

var zj = new Students('Zoe',16)

```

## 1.3 工厂模式  (设计模式)

```js
function GetValues(name,age) {
    var that = { }
    that.name = name
    that.age = age
    return that
}

var that = GetValues('张三',18)
console.log(typeof that);
```

1.4 原型模式 

1.5 混合模式

## 2.1 对象解构

```js
var obj = {
    name: '张三',
    age: 16,
    skill: {
        sname: 'Java',
        year: '2022'
    }
}

//2.1.1 取数据
// 直接打印
console.log(obj.skill[1].sname)
// 用变量接收
let personName = obj.name, personAge = obj.age

//2.1.1 取别名
// 用变量接收
let{name:personName, age:personAge} = obj



//2.2嵌套解构
let { name: personName, skill: { sname: skillName } } = obj
console.log(personName);
console.log(skillName);
```

## 3.1 对象中的方法

- object.keys() -> keys:返回的是键的数组

```js
const ArrName = Object.keys( ObjectName )
```

- obeject.values() 

```js
const ArrName = Object.values( ObjectName )
```

- object.entries()
- object.**assign**() 合并对象

```js
//assign:拷贝，浅拷贝(复制的是对象的地址),(手写一个对象的深拷贝)深拷贝(复制的的对象本身)
var obj = {
    name: '张三',
    age: 16,
    gender: '男'
}
var obj1 = {
    name: '李四',
    title: 'student'
}
var obj2 = {
    name:'王五'
}
var obj3 = {
    name:'刘六'
}

//assign:浅拷贝
 var obj2 = Object.assign(obj);
// 打印出结果属性为 ： age gender name title
// 重复相同的属性值会以最后一个值返回，例如上面返回为：
// age：16
// gander : '男'
// name : '刘六'
// title : 'student'


//assign：1.浅拷贝一个对象 2.合并对象(target,sources....)
console.log(Object.assign(obj,obj1,obj2,4,false,null,[12,3,34],{name:'张三'}));
console.log(obj[0]);
```



- object.hasOwnProperty()