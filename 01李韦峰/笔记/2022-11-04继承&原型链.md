# 继承

1. 原型链继承

```js
//父类Shape （属性有:name,length,width,方法有:Intro 我是XX形）
// 子类： Circle(属性有:r)
// 原型链继承
function Shape(name,length,width){
    this.name = name;
    this.length = length;
    this.width = width;
}
function Circle(r){
    this.r = r;
}
Shape.prototype.Intro = function(){
    console.log('我是'+this.name);
}
var shape = new Shape('正方形',10,10);
Circle.prototype = shape;
var circle = new Circle(5);
circle.Intro();
```

缺点：

2. 构造函数继承:使用call

```js
function Shape(name,length,width){
    this.name = name;
    this.length = length;
    this.width = width;
    this.Intro = function(){
    console.log('我是'+this.name);
    }
}
function Circle(name,length,width,r){
    Shape.call(this,name,length,width);// 关键用call
    this.r = r;
}
var circle = new Circle('正方形',10,10,5);
circle.Intro();
```



3. 共享原型继承

```js

```

4. 圣杯(永恒)模式

```js
function Shape(name,length,width){ 
    this.name = name;
    this.length = length;
    this.width = width;
}
Shape.prototype.Intro = function(){
    console.log('我是'+this.name);
}

function Circle(r){
    this.r = r;
}

function inherto(Target,Origin){
    function TempFunc(){}
    TempFunc.prototype = Origin.prototype;
    var tempfunc = new TempFunc();
    Target.prototype = tempfunc;
    Target.prototype.constructor = Target; // 归为
    Target.prototype.uber = Origin; // 超类

}

inherto(Circle,Shape);
var shape = new Shape('正方形',10,10);
Circle.prototype = shape;
var circle = new Circle(5);
circle.Intro();
```

