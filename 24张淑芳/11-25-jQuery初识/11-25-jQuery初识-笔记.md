

**jQuery获取对象(伪数组)**

```js
$('div');
```

**DOM对象和jQuery对象转换**

```js
//DOM转jQuery
$(DOM对象);
//jQuery转DOm
$('div')[index];
$('div').get(index);
```

**jQuery选择器**

$('选择器')

![](https://gitee.com/Agares1024/img/raw/master/img/1669560164419-2022-11-2722:42:45.png)

**属性选择器**

```js
$('li[class]'); //带有class属性的li
```

**jQuery修改样式**

```
$('div').css('background','pink');
```

**隐式迭代**

jQuery会把匹配的所有元素内部进行遍历循环，执行相应的方法



![](https://gitee.com/Agares1024/img/raw/master/img/1669561795378-2022-11-2723:09:56.png)



**层次选择器**

![](https://gitee.com/Agares1024/img/raw/master/img/1669561897599-2022-11-2723:11:38.png)

**鼠标经过**

$().mouseover(fn);

**鼠标离开**

$().mouseout(fn);