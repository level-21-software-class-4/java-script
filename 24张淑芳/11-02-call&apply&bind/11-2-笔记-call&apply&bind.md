## call,apply,bind

改变this指向

```js
Person.call(obj,'张三',18);//this：window => obj
Person.apply(obj, ['张三', 18]);
const boundGetX = unboundGetX.bind(module);//函数调用bind方法，返回一个带this指向的新函数
```

### 严格模式  'use strict'

防止意外发生错误时，及时抛出该错误

1.在严格模式下，函数内部不会再意外地生成全局变量，而是抛出异常

```js
function getName(a,b,a){
    'use strict'
    console.log(a);
    //a = 5;  --err
}
```

2.严格模式要求一个对象内的所有属性名在对象内必须唯一

3.call(null) call(undefined):严格模式下会抛出错误，非严格模式会自动转换全局

```js
Product.call(undefined, '张三', 15)  --err
```

4.构造函数使用严格模式,必须使用new

```js
function Product(name, price) {
    'use strict'
    this.name = name;
    this.price = price;
}
new Product('张三',18);
```

