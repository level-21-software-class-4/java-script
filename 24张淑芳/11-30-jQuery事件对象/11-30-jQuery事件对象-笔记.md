#### 事件绑定

```js
//直接绑定
    $('button').click(
        fn
    )
//bind--可以绑定多个事件
    $('button').bind('click dbclick',fn){
        ...
    }
//链式调用
    $('button').click(
        fn
    ).dbclick(
        fn
    )
//on()--可以为动态添加的元素绑定事件
	$('document').on('click','button',fn)
```

#### 事件解绑

```js
//不写参数就全部取消
$(this).off(要取消的事件);

```

#### 一次事件

```js
//只执行一次
$('button').one('click',fn)
```

#### 事件对象

**事件触发时产生事件对象**

target:事件源对象 

currentTarget:当前调用的对象 

delegateTarget: 委托对象



#### 阻止默认行为

```js
//
return false;
//
event.preventDefault
```

#### 右键菜单事件

```js
$(document).contextmenu(
    function () {
        //禁用右键菜单
        return false;
    }
)
```



keydown()键盘按下触发事件

e.shiftKey  -- 按下shift

