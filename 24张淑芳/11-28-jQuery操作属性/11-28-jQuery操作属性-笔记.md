## jQuery操作属性



#### 1、增删改查

```js
//attr--获取指定属性值
	$('#name').attr('要查询的属性');
	//设置属性
	$('属性名').attr('属性值');
	//移除属性
    removeAttr('属性名')
//prop--获取bool类属性值
	$('#name').prop('要查询的属性');
```

**属性分类：**

​	1、固有属性

​	2、返回值是bool类型（checked ,selected, disabled）

​	3、自定义属性

#### 添加样式

```js
$('').css({'样式名','样式值'})
```

#### 操作属性内容

```js
//dom--innerHTML
$('').html();
//dom--innerText
$('').text();
//获取表单元素的值
$('').val();
```

#### 创建元素

```js
//创建
var $h1 = $('<h1>不要让学习停下来</h1>');
//添加
	//将子元素添加到f1前面
    $('#id').prepend($h1);
	$h1.prependTo($('#id'));
	//放在最末尾
	$('#id').append($('h1'));
	$('#id').appendTo($('h1'))；
    //添加到同辈，在#id之后
    $('#id').afetr($h1);
	//在#id之前
	$('#id').before($h1);
```

#### 删除

```js
//remove--全部删除，包括子元素和标签本身
$('#id').remove();
//empty--只删除子元素，残留标签
$('#id').remove();
```

#### 设置值

```js
$('#div').html('<span><strong>北京</strong></span>');
$('#op').val('poo');
```

