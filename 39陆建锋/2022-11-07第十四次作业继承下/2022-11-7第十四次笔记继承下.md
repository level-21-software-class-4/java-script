圣杯模式最终版
        function Parent() { }
        function Child() { }
        Parent.prototype.name = 'cp';
        var inherit = (function() {
            var Temp = function(){};  //Temp
            return function (Target, Origin) {
                Temp.prototype = Origin.prototype;
                Target.prototype = new Temp();
                //归位
                Target.prototype.constructor = Target;
                //超类
                Target.prototype.uber = Origin.prototype;
            }

        }())//立刻执行

        inherit(Child,Parent);
        console.log(Child.prototype.uber.prototype);

        function Inherit(obj){
            obj.xs = '小七';
            return obj;
        }
        var obj = {
            name:'cp',
            gf:'xw'
        };
        var inherit = new Inherit(obj);

ES6(2015)新增的语法糖：  class继承（语法糖）:基于原型链的继承
    /JS最标准的实现模式 => 基于原型的构造函数模式
         function Parent(name,age) {
             this.name = name;
             this.age = age;
          }
         function Child() { }
         Parent.prototype.name = 'cp';
         var parent = new Parent();
         Child.prototype = parent;
         var child = new Child();
         child.name = 'pc';
         console.log(Parent.prototype);


Java class使用
         class Parent{
             constructor(){
                  this.name = name;
                  this.age = age;
             }
             eating(){
                 console.log('吃饭了');
             }
         }
         //sex,name,age
         //使用extends
         class Child extends Parent{
             constructor(sex){
                 super(name,age); //调用父类的constructor
                 this.sex=sex;
             }
             running(){
                 console.log('paobu');
             }
         }
         var child = new Child('male');


        //this  正则 异常 DOM BOM


        //.(ES6) ==> []
        // var obj = {
        //     name: 'cp',
        //     age: 18
        // }
        // const arr = [1, 2, 3, 4, 5, 6, 7]
        // // console.log(obj.name);
        // // console.log(obj[age]);

    //for...in(obj =>键)    for...of..(不可迭代无效)
        //对象：keys(), values(), entries()
         for (var [key, value] of Object.entries(obj)) {
             console.log(key + ':' + value);
         }

         var obj1 = {
             no1:'1a',
             no2:'2a',
             no3:'3a'
         }
         for (let i = 1; i <= 3; i++) {
             console.log(obj1['no'+i]);  // no is not defined
         }

        in hasOwnProperty()
        function Fun(name){
            this.name = name;
        }
        Fun.prototype.age = 18;
        var fun = new Fun('cp');
        console.log(fun);
        console.log('name' in fun);  //true
        console.log('age' in fun);  //true

        console.log(fun.hasOwnProperty('name')); //构造函数
        console.log(fun.hasOwnProperty('age')); // 原型




    链式调用
         var obj ={
             mon: function (){
                 console.log('周一');
                 return this;
             },
             tue: function (){
                 console.log('周二');
                 return this;
             }
         }
         obj.mon().tue();

         function mon(){
             console.log('周一');
             return function (){
                 console.log('周二');
                 return function(){
                     console.log('周三');
                 }
             }
         }
         mon()()();