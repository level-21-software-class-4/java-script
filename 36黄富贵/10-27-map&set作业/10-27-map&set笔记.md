第九课

1.引用型:function, array, obj, 

​    // ES6: map set

`console.log(Map.prototype);`

//Map:键值对，类似于对象

2.delete:map.delete(key):根据键删除,如果找到该键，那就删除成功，返回true,否则返回false.

3. var re = ma.clear() //清空数据，无返回值

4. obj:不可迭代   let[k,v]  = [键,值]

    

   ```
       for(let[k,v] of ma){
   
          console.log(k,v);
   ```

    例：map转数组 []

   ```
   const arr = [...map]
   
       const arr = []
   
       for(let e of map.entries()){
   
       arr.push(e)
   
        }
   ```

   数组转map

   ```
     const arr = [['name', 'zoe'], ['age', 18]]
   
       var map = new Map([
   
       ['name', 'zoe'], ['age', 18]
   
      ])
   ```

   //set:(装不重复的元素)数组

   //set,map:+0,-0,0 :同一个元素 NaN

   5.WeakSet 和WeakMap:弱引用(直接回收)，强引用(不会强制回收)

   ```
     var weakset = new WeakSet()
   
       var name = 'Joe'
       var map = new Map();
       map.set(name,'Zoe')
   	var weakmap = new WeakMap();
   	weakmap.set(name,'Zoe')
   ```

   