第十三课:原型(prototype)	

原型的定义:

1.prototype (显示原型) 专属于函数 (箭头函数除外) 的一个属性,原型对象。用来给将来new出来的实例作为父级使用。

2.__proto__ (隐式原型) 所有的数据类型都具有这个属性,原型对象,隐式原型。用于指向构造当前实例的构造函数中的prototype;

call apply bind 有什么区别:改变this指向

constructor:构造器

构造函数和实例对象之间：没有直接关系

​    构造函数的原型:Person.prototype

​    实例对象的原型:person1.__proto__

所有方法都放在原型上定义，部分需要用户配置的属性定义在构造、

例题:

```
function Phone(color, brand) {
             this.color = color;
             this.brand = brand;
              this.screen = screen;

         }
         //原型本身也是一个对象 增删改查
         console.log(Phone.prototype);

        //增 add properties
         Phone.prototype.screen = '18:9';
         console.log(Phone.prototype);

         //修改原型上的属性
         Phone.prototype.screen = '20:10';
         console.log(Phone.prototype.screen);

         //删
         delete Phone.prototype.screen;
         console.log(Phone.prototype);

         Phone.prototype.Intro = function (){
             console.log('这是一款'+this.color+this.screen+this.brand+'手机');
         }

```

实例化:如果构造函数上与原型属性重复，优先选取构造函数的属性

构造函数的原型是可以修改的

​    constructor构造器也可以修改