第十四课:继承

​	构造函数的属性优先于原型上的属性

实例化

​     var fun = new Fun();

​	Fun.prototype.name = '3'

原型重写

```
 Fun.prototype = {};

​     Fun.prototype.name = '2'

​     var fun1 = new Fun();



​     console.log(fun.name);  

​     console.log(fun1.name);
```

原型链  继承:继承父类的属性和方法

 	原型链继承：会将父级上的所有属性和方法统统继承过来：过多地继承没用的属性

1.添加属性,必须在重写之后

例题:

```
Parent.prototype.mskill = '做饭';
       
        
         console.log(parent.tskill);
         console.log(grand.tskill);
         console.log(parent.mskill);

         Child.prototype.lskill = '学习';
         Child.prototype = parent;
         Child.prototype.lskill = '睡觉';

         var child = new Child();
         console.log(child.lskill);
```

2.构造函数继承:使用call

例题:

```
function Parent(name,age){
             this.name = name; //child.name
             this.age = age; //child.age
         }

         function Child(name,age,sex){
             call
             Parent.call(this,name,age);
             this.sex = sex;
         }
         var child = new Child('张三',18,'male');
         console.log(child.name);
```

3.共享原型继承

例题:

```
function Grand(){}
         Grand.prototype.name = '爷爷';

         function Parent(){
         }
         Parent.prototype.name = '爹';
         Parent.prototype.age = 40;

         function Child(){
         }

         function inherit(Target,Origin){
             Target.prototype = Origin.prototype;
         }

         inherit(Child,Parent);
         Child.prototype.age = 100; //共享
         // Parent.prototype.age = 18;
         var child = new Child();
         console.log(child.name);
         console.log(child.age);
         var parent = new Parent();
         console.log(parent.age);
```

4.圣杯(永恒)模式

例题:

```
function Parent() {
        }
        Parent.prototype.name = '爹';
        Parent.prototype.age = 40;

        function Child() {
        }

        function inherit(Target, Origin) {
            function F(){} //临时构造函数
            F.prototype = Origin.prototype;
            // var f = new F();//实例化:复制一份
            // Target.prototype = f; //独立的f，里面有父类所有的属性和方法
            Target.prototype = new F();
            //归位
            //构造器归位
            Target.prototype.constructor = Target;
            //超类
            Target.prototype.uber = Origin;

        }
        //Parent.prototype.constructor
        inherit(Child, Parent);
        var child = new Child();
        console.log(child.__proto__.uber);
```

