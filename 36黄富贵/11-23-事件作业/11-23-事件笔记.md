JS第二十一课事件笔记

//DMO  0当有多个同类型事件触发时，只会触发一个 ，兼容性较高

```
var btn = document.querySelector('button');
    //点击
    function aler() {
        btn.onclick = function () {
        alert('全是帅哥');
    }
    }
    //优先级更高
    btn.onclick = function () {
        alert('全是学霸');
    }
```

//DOM 2 :当有多个同类型事件触发时，会依次触发， 兼容性较差

```
var btn = document.querySelector('button');
    //addEventListener('事件类型','')

    //DOM:谷歌 double click
    btn.addEventListener('click',fn);
    btn.addEventListener('mousemove',fn1);
    function fn() {
        alert('2班');
        // btn.removeEventListener('click',fn);
    }
    function fn1() {
        alert('4班');
    }
```

//事件取消

btn.removeEventListener('click',fn);

//冒泡

 // body.addEventListener('click',fn1,true);

  1.事件从最外层开始捕获，直到当前元素（触发事件的对象），再从当前元素向外冒泡到document

  2.事件会自发冒泡(微软)（从里层向外层冒泡，直到document）

  3.事件捕获(网景)（从最外层向里层）

```
function fn1() {
         alert('这是最外层body里面的点击事件')
     }

    //addEventListener('click',fn2,false)：默认false，表示冒泡 true：捕获
    div.addEventListener('click',fn2,false);
    function fn2() {
        alert('这是外层div里面的点击事件')
    }
    div1.addEventListener('click',fn,false);
    function fn() {
        alert('这是内层div里面的点击事件')
    }
    html.addEventListener('click',fn3,false);
    function fn3() {
        alert('这是html里面的点击事件')
    }
```

