第十八课浏览器对象模型(Browser Object Model)笔记

第一步:定义方法

```
function fn(){

}
```

第二步获取随机数:

```
var num = parseInt(Math.random()*n+1);//表示获取几的随机数就是n-1
```

第三步获取对应的td对象:

```
var tdObj = document.getElementById('td'+num);//tdObj表示定义的td对象
```

第四步设置背景颜色:

```
tdObj.style.backgroundColor = 'red';
```

第五步执行显示结果到span:

```
document.getElementById('sp').innerText = ''+tdObj.innerText;
```

for循环初始化背景

```
for (let i = 1; i <= n; i++) {

​        var tdObj = document.getElementById('td' + i);

​        tdObj.style.backgroundColor = 'white';

​      }//n具体数值看自己定义的
```

设置抽奖提示语:

```
document.getElementById('sp').innerText = '正在抽奖中...'
```

最后设置一个定时器，多少秒出结果:

```
setTimeout(fn,n)
```

