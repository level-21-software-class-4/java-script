jquery方法笔记

1.eq()：跟据下标找元素,从0开始

```
$('button').eq(2).click(()=>alert('按钮3'));
```

2.each(index,element):表示遍历jQuery对象集 中的所有元素对象

```
$('button').each(
            function (index) {
                if(index==2){
                    $(this).click(()=>{alert('按钮')});
                }
                
            }
        )
```

3.detach():保留事件

```
var $btn = $('button').eq(2).detach();
        $('button').eq(1).click(function () {
            $(this).after($btn);
        })
```

4.clone(bool):返回一个复制元素,默认是false：只克隆元素。不克隆事件，参数为bool=true:表示将事件一起复制,

```
$('button').eq(4).click(
            function () {
                var $btn3 = $('button').eq(2).clone(true);
                $(this).after($btn3);
            }
        )
```

5.replaceWith():替换jQuery对象，

```
$('button').eq(3).replaceWith($('<button>新点击</button>'));
```

6.hasClass(属性值):查找jQuery对象中的class有没有该属性值

```
console.log($('div').eq(0).hasClass('blue'));
```

7.全选

```
$('#selectAll').click(
            function () {
                //点击全部选中
                if($(this).is(':checked')){
                    $('[class=fruit]').prop('checked',true);
                }else{
                    $('[class=fruit]').prop('checked',false);
                }
                
            }
        )
```

8.反选

```
$('#selectAll1').click(function () {
            if($(this).is(':checked')){
                console.log('111');
                $('[class="fruit"]').each(
                    function () {
                        if($(this).is(':checked')){
                            $(this).prop('checked',false);
                        }else{
                            $(this).prop('checked',true);
                        }
                    }
                )
            }
        })
```

9.反向过滤not():去除符合条件的

```
$('button').not('[class="btn"]').click(
            function () {
                alert('剩下的按钮点击')
            }
        )
```

10.filter():筛选符合条件的

```
$('button').filter('[class="btn"]').click(
            function () {
                alert('剩下的按钮点击')
            }
        )
```

11.has( ):查找子代中是否有该元素，如果有，为父元素绑定事件

```
$('.btn1').click(
            function (e) {
                e.stopPropagation();
            }
        )
        $('div').eq(0).has('[class="btn2"]').click(
            function () {
                alert('点击')
            }
        );
```

