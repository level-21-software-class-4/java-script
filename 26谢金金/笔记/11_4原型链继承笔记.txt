## 原型链继承

通过实例化一个新的函数，子类的原型指向了父类的实例，子类就可以调用其父类原型对象上的私有属性和公有方法。（本质就是重写了原型对象）
## 构造函数继承

即在子类型构造函数的内部调用超类型构造函数。
## 圣杯(永恒)模式
注意！构造器归位
圣杯模式的存在是用来继承已有原型对象 (A.prototype)中的成员（主要是公用方法），
同时根据自己的需求修改原型对象 (A.prototype)以定制符合我们要求的构造函数B，这个修改对已有的实例 (a1,a2,…)不会产生影响。

例子：function Parent() {
        }
        Parent.prototype.name = '爹';
        Parent.prototype.age = 40;

        function Child() {
        }

        function inherit(Target, Origin) {
            function F(){} //临时构造函数
            F.prototype = Origin.prototype;
            // var f = new F();//实例化:复制一份
            // Target.prototype = f; //独立的f，里面有父类所有的属性和方法
            Target.prototype = new F();
            //归位
            //构造器归位
            Target.prototype.constructor = Target;
            //超类
            Target.prototype.uber = Origin;

        }
        inherit(Child, Parent);
        var child = new Child();
        console.log(child.name);
        console.log(Child.prototype.constructor);
        var parent = new Parent();
        console.log();
        console.log(parent.name);