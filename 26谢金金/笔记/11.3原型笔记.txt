.md 0 → 100644 
构造函数和实例对象之间：没有直接关系

构造函数的原型:Person.prototype

实例对象的原型:person1.__proto__

所有方法都放在原型上定义，部分需要用户配置的属性定义在构造

原型：

增：函数名.prototype.变量名 = 值 (变量名相同时为修改)

删：delete 函数名.prototype.变量名

原型的重写:

```
     fun.prototype = {
         // constructor:fun(手动添加构造器,为添加构造器时原型的构造器会被覆盖)
         name: 'peter',
         age: 25
     }
```

对象都有原型， 原型是对象  原型的原型为(Object):Object 显示为 null
//2.prototype,__proto__,constructor 有什么联系
        // __proto__对象上属性 ---->prototype 原型对象  所以可以说__proto__属性就是原型对象(prototype)，
        //原型对象本身也有__proto__，
        //指向Object.prototype，最后Object.prototype.__proto__指向了null。null是原型链的顶端。
        //constructor这个属性来讲，只有prototype对象才有。
