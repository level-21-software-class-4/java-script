1.html元素获取

```
//根据class获取元素: 返回: HTMLCollection (数组)
  console.log(document.getElementsByClassName('H2'));
//根据name属性获取元素对象: NodeList(伪数组)：只能获取下标和长度
  console.log(document.getElementsByName('username'));
//根据标签名获取元素对象:返回 HTMLCollection
   console.log(document.getElementsByTagName('input'));
//根据id属性获取元素对象，返回当前元素对象
    console.log(document.getElementById('thirdH2'));
//id或class,获取元素对象
console.log(document.querySelector('.class名'));//.获取class对象元素
console.log(document.querySelector('#id名'));//#获取id对象元素
```

2.元素节点

```
//创建元素节点
  var 元素对象 = document.createElement('h2');
  var txt = document.createTextNode('文本内容');
  元素对象.appendChild(txt);
// 添加子元素节点
  div.appendChild(newH2);
//删除子节点:removeChild(元素对象)
//改:replaceChild(元素对象(新),元素对象(旧))
```
