# 2022-10-26

## assign

1.Object.assign方法用于对象的合并，将源对象（ source ）的所有可[枚举](https://so.csdn.net/so/search?q=枚举&spm=1001.2101.3001.7020)属性，复制到目标对象（ target ）。

```javascript
var target = { a: 1 };
var source1 = { b: 2 };
var source2 = { c: 3 };
Object.assign(target, source1, source2);
target // {a:1, b:2, c:3}
```

3.Object.assign方法的第一个参数是目标对象，后面的参数都是源对象。

注意，如果目标对象与源对象有同名属性，或多个源对象有同名属性，则后面的属性会覆盖前面的属性。

```javascript
var target = { a: 1, b: 1 };
var source1 = { b: 2, c: 2 };
var source2 = { c: 3 };
Object.assign(target, source1, source2);
target // {a:1, b:2, c:3}
```

3.由于undefined和null无法转成对象，所以如果它们作为参数，就会报错。

```javascript
Object.assign(undefined) //  报错
Object.assign(null) //  报错
```

4.如果只有一个参数，Object.assign会直接返回该参数。

```javascript
var obj = {a: 1};
Object.assign(obj) === obj // true
```

5.如果该参数不是对象，则会先转成对象，然后返回。

```javascript
typeof Object.assign(2) // "object"
```

Object.assign方法的第一个参数是目标对象，后面的参数都是源对象。

注意，如果目标对象与源对象有同名属性，或多个源对象有同名属性，则后面的属性会覆盖前面的属性。

## 包装类

1. 原始数据类型： number, boolean, string, null, undefined ,es6新加的(symbol,bigint)
2. JS 提供了 三个 特殊的引用类型：[Boolean](https://so.csdn.net/so/search?q=Boolean&spm=1001.2101.3001.7020)、Number、String（**undefined**和**null**没有自己的包装类）
3. null,undefined是没有包装类的
4. 所有的包装类都有 valueOf() 和 toString() 方法，还有charAt(), [indexOf](https://so.csdn.net/so/search?q=indexOf&spm=1001.2101.3001.7020)()等方法。

JS为我们提供了三个包装类，通过这三个包装类可以将基本数据类型的数据转换为对象

```js
String()
　　　　-　可以基本数据类型字符串转换为String对象

　　Number()
　　　　-　可以基本数据类型数字转换为Number对象

　　Boolean()
　　　　-　可以基本数据类型布尔值转换为Boolean对象
---------------------------------------------
      创建基本数据类型的对象
　　var str = new String('hello')　　//　str是一个值为'hello'的对象　即：String{'hello'}
　　var str1 = 'hello'
　　var num = new Number(3)　　//　num是一个值为3的对象　即：String{'hello'}
　　var num1 = 3
　　var bool = new Boolean(true)　　//　bool是一个值为true的对象　即：String{'hello'}
　　var bool2 = true
　　console.log(str)　　//　String {'hello'}
　　console.log(num)　　//　Number {3}
　　console.log(bool)　　//　Boolean {true}
　　console.log(typeof str)　　//　object
　　console.log(str === str1)　　//　false　　(str是对象，而str1是字符串)
----------------------------------------------------
算出字符串的所占用字节的长度，
如: str  ='live' 长度为4      str='live生活'   长度为8 
（提示:unicode<=255的占1字节，unicode>255的占用2字节, str.charCodeAt(i)查看i的字节码）
   
var str = 'Live哈哈哈'
var count = 0
for (let index = 0; index < str.length; index++) {
str.charCodeAt(index)>255 ? count+=2 : count++
```

