# 2022-11-04

## 继承

### 构造函数继承

this总是指向调用某个方法的对象，但是使用call()和apply()方法时，就会改变this的指向。只不过apply第二个参数必须传入的是一个数组，而call 第二个参数可以是任意类型。

```js
//使用call构造函数继承:使用call
function Test(name,age){
            this.name = name;
            this.age = age;
        }
        function obj (name,age) { 
            Test.call(this,name,age);
        }

        var obj  = new obj('张三',18)
        console.log(obj);
```

### 原型链继承：会将父级上的所有属性和方法统统继承过来：过多地继承没用的属性

```js
function Grand() {}  //爷爷
function Parent() {} //爸爸
Grand.prototype.tskill = '拉二胡';
var grand = new Grand();
Parent.prototype = grand; //原型重写,如果
var parent = new Parent();
Parent.prototype.tskill = '弹琴'; //添加属性,必须在重写之后

console.log(parent.tskill);//弹琴
console.log(grand.tskill);//弹琴

///////////////////////////////////////

function Grand() {}     //爷爷
function Parent() {}    //爸爸
Grand.prototype.tskill = '拉二胡';
var grand = new Grand();
//Parent.prototype = grand; //注释如果取消会改变结果
var parent = new Parent();
Parent.prototype.tskill = '弹琴';    //添加属性,必须在重写之后

console.log(parent.tskill);//弹琴
console.log(grand.tskill);//拉二胡
```

​	
