# 2022-11-30

## iquery 事件对象

JavaScript 在事件处理函数中默认传递了 event 对象，也就是事件对象。但由于浏览器的兼容性，开发者总是会做兼容方面的处理。[jQuery](https://so.csdn.net/so/search?q=jQuery&spm=1001.2101.3001.7020) 在封装的时候，解决了这些问题，并且还创建了一些非常好用的属性和方法。

#### 一．事件对象

事件对象就是 event 对象，通过处理函数默认传递接受。之前处理函数的 e 就是 event
事件对象，event 对象有很多可用的属性和方法

```js
//通过处理函数传递事件对象
$('input').bind('click', function (e) { //接受事件对象参数
alert(e);
});
```

**通过 event.type 属性获取触发事件名**

```js
$('input').click(function (e) {
alert(e.type);
});
```

通过 event.target 获取绑定的 DOM 元素

```js
$('input').click(function (e) {
alert(e.target);
});
```

通过 event.data 获取额外数据，可以是数字、字符串、数组、对象

```js
$('input').bind('click', 123, function () { //传递 data 数据
alert(e.data); //获取数字数据
});
```

## jQuery事件绑定扩展

jQuery中提供了四种绑定事件的方法，分别是bind、live、delegate、on，对应的解除监听的函数分别是unbind、die、undelegate、off：

#### on()绑定(首选)

on() 方法在被选元素及子元素上添加一个或多个事件处理程序。

**event**:必需。规定要从被选元素移除的一个或多个事件或命名空间。由空格分隔多个事件值。必须是有效的事件

**data**:可选。规定只能添加到指定的子元素上的事件处理程序（且不是选择器本身，比如已废弃的 delegate() 方法）。

**function**:可选。规定当事件发生时运行的函数。

**map**:规定事件映射 (*{event:function, event:function, ...})*，包含要添加到元素的一个或多个事件，以及当事件发生时运行的函数。

```js
语法：$(selector).on(event,childSelector,data,function,map)

------------------------给<p>添加一个单击事件-----------------------------------
    $("p").on("click",function(){
    alert("该段落p已单击");
});
```

#### bind绑定

**type:**必需。事件类型，如click、change、mouseover等;

**data:**可选。传入监听函数的参数，通过event.data取到。可选;

**function:**必需。规定当事件发生时运行的函数(监听函数)，可传入event对象，这里的event是jQuery封装的event对象，与原生的event对象有区别，使用时需要注意。

***map\*:**规定事件映射 (*{event:function, event:function, ...})*，包含要添加到元素的一个或多个事件，以及当事件发生时运行的函数

```js
语法：$(selector).off(event,selector,function(eventObj),map)

------------------------给<p>添加一个单击事件-----------------------------------
    $("p").bind("click",function(){
    alert("单击p元素");
});
```

