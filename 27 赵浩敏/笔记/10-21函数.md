# 							10-21函数

## 1.1函数概念

在 JS 里面，可能会定义非常多的相同代码或者功能相似的代码，这些代码可能需要大量重复使用。虽然 for循环语句也能实现一些简单的重复操作，但是比较具有局限性，此时我们就可以使用 JS 中的函数。

 函数：就是封装了一段**可被重复调用执行的代码块**。通过此代码块可以**实现大量代码的重复使用。**2.2 函数的使用

#### 1.2声明函数

```js
// 声明函数
function 函数名() {
    //函数体代码
}
//定义一个函数，输入n，求n!的阶乘 5!=1*2*3*4*5
 // var num = parseInt(prompt("请输入一个数"));  //输入一个数

        // function aa(num) {
        //     var res = 1;  //result:定义积： 1*1*2*3*4*5
        //     for (var i = 1; i <= num; i++) {
        //         res *= i; //res = res * i
        //     }
        //     return res;
        // }
function 是声明函数的关键字,必须小写

由于函数一般是为了实现某个功能才定义的， 所以通常我们将函数名命名为动词，比如 getSum



        //匿名函数： function (){}
        // function (num) {
        //     var res = 1;  //result:定义积： 1*1*2*3*4*5
        //     for (var i = 1; i <= num; i++) {
        //         res *= i; //res = res * i
        //     }
        //     return res;
        // }


        //表达式
        // var aa =  function(num){
        //     var res = 1;
        //     for(var i = 1; i <= num ; i++){
        //         res*=i;
        //     }
        //     return res;
        // }
        // var res = aa(num);
        // console.log(res);
        // console.log(typeof aa);  //number boolean array function(对象)

        // function fn(){
        //     //arguments:只有长度，只能找下标和元素
        //     //console.log(arguments);

        // }

        // fn(1,2,3,4,5,6,7,8,9,0)


        //箭头函数：  var aa =   (形参列表)   =>  {函数体} arrow function
        // var aa = function (n1, n2) {
        //     var sum = n1 + n2
        //     return sum;
        // }

        // var bb = (n1, n2) => {
        //     var sum = n1 + n2;
        //     return n1+n2;
        // }

        // var cc = (n1, n2) => n1 + n2; //函数只有一个return语句时，省略{}和return关键字,直接写返回值


        // var aa = function (num) {
        //     var res = 1;
        //     for (var i = 1; i <= num; i++) {
        //         res *= i;
        //     }
        //     return res;
        // }

        //当函数只有一个形参时，可以省略圆括号
        // var dd = (num) => {
        //     var res = 1;
        //     for (var i = 1; i <= num; i++) {
        //         res *= i;
        //     }
        //     return console.log(res);;
        // }

        // var ee = () => '这是一个箭头函数';  //无参时，圆括号不可以省略


        // console.log(aa(5,5));
        // console.log(bb(5, 5));
        // console.log(cc(5, 5));
        // console.log(dd(5,5));
        // console.log(ee());


        //无返回值,一般不用箭头函数

```

## 1.3调用函数

// 调用函数
函数名();  // 通过调用函数名来执行函数体代码
1
2
调用的时候千万不要忘记添加小括号

口诀：函数不调用，自己不执行

注意：声明函数本身并不会执行代码，只有调用函数时才会执行函数体代码。

## 1.4函数的参数



```js
  //对象：  类(人类，鱼类，鸟类)   万物对象(陈鹏):属性和行为
        //字面量表达式创建对象
        // var obj = {
        //     name:{
        //         firstName:'陈',
        //         lastName:'鹏'
        //     },
        //     age:18,
        //     'qq number': 1658656456
        // }  
        //属性:增删改查
        //1.查 . 一级属性  二级属性往后[][]
        // console.log(obj.name);
        // console.log(obj.age);
        //1.1查  使用 []
        // console.log(obj['name']['lastName']);  

        //增 obj.原来obj中没有的属性，相当于将该属性添加到对象obj中
        // obj.gender = '男'
        // console.log(obj);

        // obj.age = 19
        // console.log(obj);

        //删 delete  一般删之前要对该属性进行判断 
        // delete obj.age
        // console.log(obj);

        //构造函数   =  new 对象

        //普通函数命名规则：小驼峰:getName()
        //构造函数命名规则：大驼峰:GetName()
        function GetName(){
            this.name = '陈鹏'
            this.age = 18
        };


        var obj = new GetName()   //生成了一个对象
        console.log(obj.age);

        //设计模式：12种  工厂模式
        
        

        
```

函数参数语法
形参：函数定义时设置接收调用时传入

实参：函数调用时传入小括号内的真实数据



参数的作用 : 在函数内部某些值不能固定，我们可以通过参数在调用函数时传递不同的值进去。

函数参数的运用：

// 带参数的函数声明
function 函数名(形参1, 形参2 , 形参3...) { // 可以定义任意多的参数，用逗号分隔
  // 函数体
}
// 带参数的函数调用
函数名(实参1, 实参2, 实参3...); 
1
2
3
4
5
6
调用的时候实参值是传递给形参的
形参简单理解为：不用声明的变量
实参和形参的多个参数之间用逗号（,）分隔
函数形参和实参数量不匹配时


注意：在JavaScript中，形参的默认值是undefined。
1
小结：

函数可以带参数也可以不带参数
声明函数的时候，函数名括号里面的是形参，形参的默认值为 undefined
调用函数的时候，函数名括号里面的是实参
多个参数中间用逗号分隔
形参的个数可以和实参个数不匹配，但是结果不可预计，我们尽量要匹配

## 1.5函数的返回值

return 语句
返回值：函数调用整体代表的数据；函数执行完成后可以通过return语句将指定数据返回 。
1
// 声明函数
function 函数名（）{
    ...
    return  需要返回的值；
}
// 调用函数
函数名();    // 此时调用函数就可以得到函数体内return 后面的值
1
2
3
4
5
6
7
在使用 return 语句时，函数会停止执行，并返回指定的值
如果函数没有 return ，返回的值是 undefined

## 1.6 arguments的使用

 当不确定有多少个参数传递的时候，可以用 arguments 来获取。JavaScript 中，arguments实际上它是当前函数的一个内置对象。所有函数都内置了一个 arguments 对象，arguments 对象中存储了传递的所有实参。arguments展示形式是一个伪数组，因此可以进行遍历。伪数组具有以下特点：

具有 length 属性

按索引方式储存数据

不具有数组的 push , pop 等方法

注意：在函数内部使用该对象，用此对象获取函数调用时传的实参。

## 1.7 函数的两种声明方式

自定义函数方式(命名函数)

利用函数关键字 function 自定义函数方式

// 声明定义方式
function fn() {...}
// 调用  
fn();  
1
2
3
4
因为有名字，所以也被称为命名函数
调用函数的代码既可以放到声明函数的前面，也可以放在声明函数的后面
函数表达式方式(匿名函数）

利用函数表达式方式的写法如下：

// 这是函数表达式写法，匿名函数后面跟分号结束
var fn = function(){...}；
// 调用的方式，函数调用必须写到函数体下面
fn();
1
2
3
4
因为函数没有名字，所以也被称为匿名函数
这个fn 里面存储的是一个函数
函数表达式方式原理跟声明变量方式是一致的
函数调用的代码必须写到函数体后面

