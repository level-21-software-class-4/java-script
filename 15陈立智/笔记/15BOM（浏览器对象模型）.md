# BOM（浏览器对象模型）

BOM（Browser Object Model ）：是浏览器对象模型，它提供了独立于内容，而**与浏览器窗口进行交互**的对象，其**核心对象是** ***\*window\**** 。

BOM的顶端为window

## 1window的构成

（1）.document

（2）.location

（3）.navigation

（4）.screen

（5）.history

## 2.window

### （1）.常用的对象属性																 			

| 属性                                    | 说明                                                         |
| --------------------------------------- | ------------------------------------------------------------ |
| window                                  | 包含了对窗口自身的引用。等价于self属性。                     |
| defaultStatus                           | 设置或返回窗口状态栏中的默认文本。                           |
| innerheight                             | 返回窗口的文档显示区的高度。                                 |
| innerwidth                              | 返回窗口的文档显示区的宽度。                                 |
| self                                    | 返回对当前窗口的引用。等价于 Window 属性。                   |
| closed                                  | 返回窗口是否已被关闭                                         |
| top                                     | 返回最顶层的先辈窗口。                                       |
| status                                  | 设置窗口状态栏的文本。                                       |
| pageXOffset                             | 设置或返回当前页面相对于窗口显示区左上角的 X 位置。          |
| pageYOffset                             | 设置或返回当前页面相对于窗口显示区左上角的 Y 位置。          |
| parent                                  | 返回父窗口。                                                 |
| screenLeft，screenTop，screenX，screenY | 只读整数。声明了窗口的左上角在屏幕上的 x 坐标和 y 坐标。IE、Safari 和 Opera 支持 screenLeft 和 screenTop，而Firefox 和 Safari 支持 screenX 和 screenY。 |
| length                                  | 设置或返回窗口中的框架数量。                                 |
| name                                    | 设置或返回窗口的名称。                                       |
| opener                                  | 返回对创建此窗口的窗口的引用。                               |
| outerheight                             | 返回窗口的外部高度。                                         |
| outerwidth                              | 返回窗口的外部宽度。                                         |

### （2）.对象常用方法

| **方法**         | **说明**                                           |
| ---------------- | -------------------------------------------------- |
| onload()         | 当页面完全加载到浏览器上时，触发该事件。           |
| onscroll()       | 当窗口滚动时触发该事件。                           |
| onresize()       | 当窗口大小发生变化时触发该事件。                   |
| open()           | 打开一个新的浏览器窗口或查找一个已命名的窗口。     |
| **setTimeout()** | 在指定的毫秒数后调用函数或计算表达式。             |
| setInterval()    | 按照指定的周期（以毫秒计）来调用函数或计算表达式。 |
| clearTimeout()   | 取消由setTimeout()方法设置的定时器。               |
| vlearInterval()  | 取消由setInterval()设置的定时器。                  |

## 3.document

### （1）.常用的对象属性	

| 属性            | 方法                                                 |
| --------------- | ---------------------------------------------------- |
| documentElement | 指向HTML页面中的<html>元素。                         |
| title           | 获取文档的标题                                       |
| URL             | 获取URL                                              |
| domain          | 取得域名，并且可以进行设置，在跨域访问中经常会用到。 |
| referrer        | 取得链接到当前页面的那个页面的URL，即来源页面的URL。 |
| links           | 获取文档中所有带href属性的a元素                      |
| body            | 指向body元素                                         |
| images          | 获取所有img对象，返回HTMLCollection类数组对象        |
| forms           | 获取所有form对象，返回HTMLCollection类数组对象       |



### （2）.**查找元素的方法**	

| 方法                     | 说明                                                         |
| ------------------------ | ------------------------------------------------------------ |
| **getElementById()**     | 返回指定 id 的一个元素。                                     |
| getElementsByTagName()   | 返回一个`HTMLCollection`(伪数组)，包含指定标签名的所有元素。 |
| getElementsByClassName() | 返回一个HTML集合`HTMLCollection`(伪数组)，包含指定类名的所有元素。 |
| document.querySelector() | 返回文档中指定的CSS选择器的第一个元素                        |

### 									

































