## jQuery 事件

#### onload事件

在JavaScript中，onload表示文档加载完成后再执行的一个事件。window.onload只能调用一次，如果多次调用，则只会执行最后一个。

```html
window.onload = function(){
……
}

```

对于JavaScript的onload事件来说，只有当页面所有DOM元素以及所有外部文件（图片、外部 CSS、外部JavaScript等）加载完成之后才会执行。这里的所有DOM元素，指的是HTML部分的代码。

#### ready事件

​		在jQuery中，ready也表示文档（document）加载完成后再执行的一个事件。

```html
$(document).ready(function(){
	……
})

```

​		对于jQuery的ready事件来说，只要页面所有DOM元素加载完成就可以执行，不需要再等外部文件 （图片、外部CSS、外部JavaScript）加载完成。

#### read事件的四种写法

```
//写法1：
$(document).ready(function(){
……
})
//写法2：
jQuery(document).ready(function(){
……
})
//写法3(最常用)：
$(function(){
……
})
//写法4：
jQuery(function(){
……
})

```



#### 鼠标事件

在jQuery中，常见的鼠标事件如下表:

| 事件      | 说明         |
| --------- | ------------ |
| click     | 鼠标单击事件 |
| dblclick  | 鼠标双击事件 |
| mouseover | 鼠标移入事件 |
| mouseout  | 鼠标移出事件 |
| mousedown | 鼠标按下事件 |
| mouseup   | 鼠标松开事件 |
| mousemove | 鼠标移动事件 |
|           |              |

​	

hover:mouseover+mousedown

​	jQuery 事件比JavaScript事件只是少了“on”前缀。例如鼠标单击事件在JavaScript中是onclick，而在jQuery中是 click。

​		jQuery事件的语法很简单，我们都是往事件方法中插入一个匿名函数function(){}





#### 鼠标单击

​		单击事件不只是按钮才有，我们可以为任何元素添加单击事件！

```html
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title></title>
    <style>
        div {
            display: inline-block;
            width: 80px;
            height: 24px;
            line-height: 24px;
            font-family: "微软雅黑";
            font-size: 15px;
            text-align: center;
            border-radius: 3px;
            background-color: deepskyblue;
            color: White;
            cursor: pointer;
        }

       
    </style>
    <script src="https://s3.pstatp.com/cdn/expire-1-M/jquery/3.3.1/jquery.min.js"></script>
</head>

<body>
    <div>调试代码</div>
</body>

</html>
```



**练习:有单击事件:alert('我是单击')  有双击事件 alert('我是双击')**

**思考:如何触发双击事件时不触发单击事件**



#### 鼠标移入移出

​		当用户将鼠标移入到某个元素上面时，就会触发mouseover事件。如果将鼠标移出某个元素时，就 会触发mouseout事件。mouseover和mouseout平常都是形影不离的。

```js
//鼠标到文字上变色，移开变色
<div>Javascript</div>
```



**练习：一级菜单--》二级菜单**



#### 鼠标按下和松开

​		当用户按下鼠标时，会触发mousedown事件；当用户松开鼠标时，则会触发mouseup事件。 mousedown表示鼠标按下的一瞬间所触发的事件，而mouseup表示鼠标松开的一瞬间所触发的事 件。当然我们都知道，只有“先按下”才能“再松开”。 



#### 键盘事件

在jQuery中，常用的键盘事件共有2种。 

1. 键盘按下：keydown 

2. 键盘松开：keyup 

keydown表示键盘按下一瞬间所触发的事件，而keyup表示键盘松开一瞬间所触发的事件。对于键盘来 

说，都是先有“按下”才有“松开”，也就是keydown发生在keyup之前。

```html
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title></title>
    <script src="https://s3.pstatp.com/cdn/expire-1-M/jquery/3.3.1/jquery.min.js"></script>
    <script> 
        //统计输入框字符串的长度
    </script>
</head>

<body>
    <input id="txt" type="text" />
    <div>字符串长度为： <span id="num">0</span> </div>
</body>

</html>
```



#### 表单事件

##### 1. focus和blur

focus表示获取焦点时触发的事件，而blur表示失去焦点时触发的事件，两者是相反操作。 

focus和blur这两个事件往往都是配合一起使用的。例如用户准备在文本框中输入内容时，此时它会获得 

光标，就会触发focus事件。当文本框失去光标时，就会触发blur事件。 

并不是所有的HTML元素都有焦点事件，具有“获取焦点”和“失去焦点”特点的元素只有两种。 

1. **表单元素（单选框、复选框、单行文本框、多行文本框、下拉列表）** 

2. **超链接** 

判断一个元素是否具有焦点很简单，我们打开一个页面后按Tab键，能够选中的就是带有焦点特性的元 

素。在实际开发中，焦点事件（focus和blur）一般用于单行文本框和多行文本框，其他地方比较少见。

```html
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title></title>
    <script src="https://s3.pstatp.com/cdn/expire-1-M/jquery/3.3.1/jquery.min.js"></script>
    
</head>


<body><input id="search" type="text" value="百度一下，你就知道" /> <input type="button" value="搜索" /> </body>

</html>
```



##### 2. select

在jQuery中，当我们选中“单行文本框”或“多行文本框”中的内容时，就会触发select事件。

```html
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title></title>
    <script src="https://s3.pstatp.com/cdn/expire-1-M/jquery/3.3.1/jquery.min.js"></script>
    
</head>

<body>
    <input id="txt1" type="text" value="前端开发学习，jQuery框架学习" /><br />
    <textarea id="txt2" cols="20" rows="5">前端开发学习，jQuery框架学习</textarea>
</body>

</html>
```



##### **3. change** 

在jQuery中，change事件常用于“具有多个选项的表单元素”。 

1. 单选框选择某一项时触发。 
2. 复选框选择某一项时触发。 
3. 下拉菜单选择某一项时触发。

```html
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title></title>
    <script src="https://s3.pstatp.com/cdn/expire-1-M/jquery/3.3.1/jquery.min.js"></script>
</head>

<body>
    <div>
        <label><input type="radio" name="fruit" value="苹果" />苹果</label>
        <label><input type="radio" name="fruit" value="香蕉" />香蕉</label>
        <label><input type="radio" name="fruit" value="西瓜" />西瓜</label>
    </div>
    <p></p>
</body>

</html>
```



