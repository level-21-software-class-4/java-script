# 一 Map

Map:键值对，类似于对象，键是唯一的

创建map

var 变量名n=new map()  注：变量名n是一个对象

## 1.map增加

create

变量名n.set(键，值)

## 2.map删除

delete

var 变量名n=map.delete(键)注：返回为布尔值

## 3.map改

变量名n.set(要改的键，新值)

## 4.map查

1.keys只返回键

for (let e of map名.keys()){

​      console.log(e);

}

----------------------------

2.values只返回值

for (let e of map名.values()){

​      console.log(e);

}

----------------------------------------------------

3.entrise返回数组

for (let e of map名.entries()){

​      console.log(e);

}

---------------------------------------

4.返回所有

for (let e of map名){

​      console.log(e);

}

## 5.map转数组

const arr = [...map名]

for(let e of map.entries()){

arr.push(e)

}

## 6.数组转map

const 数组名 = [[键, 值], ['name', 18]]

var map = new Map([['键', '值'], ['age', 18]])

# 二 set数组

set:(装不重复的元素)数组

## 1.创建set

var 变量名 = new Set();

## 2.set增加

set.add(值)

## 3.set删除

set.delete(已有值)

## 4.set查

注：与map方法一致查看上面

## 5.set转数组

const 数组名 = [...set名]

## 6.数组转set（数组去重）

 var set名 = new Set(数组名);

三 WeakSet WeakMap:弱引用(直接回收)，强引用(不会强制回收)

var weakset = new WeakSet()