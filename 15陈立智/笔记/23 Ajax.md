## jQuery Ajax

**Ajax 是一种异步、无刷新(或者说局部页面刷新)技术。**

#### $.get()

语法：$.get(url, data, fn, type)

| 参数 | 说明                               |
| :--: | :--------------------------------- |
| url  | 必选参数，表示被加载的页面地址     |
| data | 可选参数，表示发送到服务器的数据   |
|  fn  | 可选参数，表示请求成功后的回调函数 |
| type | 可选参数，表示服务器返回的内容格式 |

参数type返回的内容格式包括：text、html、xml、json、script或default。



#### $.post()

语法：$.post(url,data,fn,type)

#### $.getJSON()

语法：$.getJSON(url ,data, function(data){……})

```json
        [
            {
                "name": "刘备",
                "sex": "男",
                "age": 24
            },
            {
                "name": "关羽",
                "sex": "男",
                "age": 24
            },
            {
                "name": "小乔",
                "sex": "女",
                "age": 23
            }
        ]
```



#### $.getScript()

语法：$.getScript(url, fn)

| 参数 | 说明                                     |
| ---- | ---------------------------------------- |
| url  | 必选参数，表示被加载的JavaScript文件路径 |
| fn   | 可选参数，表示请求成功后的回调函数       |

#### $.ajax()

上述方法都是ajax的简化版，它们能实现的功 能，.ajax()都能实现，因为$.ajax()是最底层的方法。

语法：$.ajax(options)

| **参数**    | **说明**                                                |
| ----------- | ------------------------------------------------------- |
| **url**     | **被加载的页面地址**                                    |
| **type**    | **数据请求方式，“get"或"post”，默认为"get"**            |
| **data**    | 发送到服务器的数据，可以是字符串或对象                  |
| dataType    | 服务器返回数据的类型，如：text、html、script、json、xml |
| beforeSend  | 发送请求前可以修改XMLHttpRequest对象的函数              |
| complete    | 请求“完成”后的回调函数                                  |
| **success** | 请求“成功”后的回调函数                                  |
| **error**   | 请求“失败”后的回调函数                                  |
| timeout     | 请求超时的时间，单位为“毫秒”                            |
| global      | 是否响应全局事件，默认为true（即响应）                  |
| async       | 是否为异步请求，默认为true（即异步）                    |
| cache       | 是否进行页面缓存，true表示缓存，false表示不缓存         |

