# DOM（文档对象模型）

| document.documentElement | 返回文档对象（document）的根元素的只读属性（如HTML文档的 <html> 元素）。 |
| ------------------------ | ------------------------------------------------------------ |
| document.body            | 返回html dom中的body节点 即<body>                            |
| document.head            | 返回html dom中的head节点 即<head>                            |
| document.title           | 返回html dom中的title节点 即<title>                          |

## 1.节点（node）

**属性：**
1.nodeType           表示节点类型

节点类型为1：元素节点

节点类型3：文本节点

节点类型2: 属性节点

2.nodeName

该属性取决于节点类型，如果是元素类型，值为元素的标签名

3.nodeValue

该属性取决于节点类型，如果是元素类型，值有null

4.childNodes 

获取所有子节点

5.parentNode
指向文档树中的父节点。包含在childNodes列表中所有的节点都具有相同的父节点，每个节点之间都是同胞/兄弟节点。

6.previousSibling
兄弟节点中的前一个节点

7.nextSibling
兄弟节点中的下一个节点

8.firstChild
childNodes列表中的第一个节点

9.lastChild
childNodes列表中的最后一个节点

## 2.元素(elements)

1.childElementCount

返回该元素在HTML文档里子元素的个数

2.children

返回一个数组，该数组存在该元素在HTML文档里出现的子元素

## 元素与节点的区别

所谓元素，即HTML文档里的标签，所以标签都可以称为元素，如<a>,<be>等，也就是说元素是个统称，所谓节点，即每个元素都可以称为一个节点，节点是唯一的。

总结：一个文档有多个元素但只有一个唯一节点。

比方：一个班（HTML文档）里有x人（元素）但是他们只有一个学号（节点）