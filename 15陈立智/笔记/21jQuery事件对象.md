## jQuery事件对象

### jQuery事件绑定扩展



#### 绑定事件的三种类型

##### 直接绑定(最简单也是最常用)

语法:

```js
$(selector).event(function(){})
```

##### bind绑定

语法:

```js
$(selector).off(event,selector,function(eventObj),map)
```

 ![img](https://upload-images.jianshu.io/upload_images/14415740-c70031137be2807f.png?imageMogr2/auto-orient/strip|imageView2/2/format/webp) 

```html
eg:对btn进行绑定点击事件
<button class="btn">btn1</button>
```



##### on()绑定

在jQuery中，我们可以使用on()方法为“已经存在的元素”绑定事件。

语法:

```js
$(selector).on(event,childSelector,data,function)
```

​		on绑定与前两个绑定最大的区别是on可以为动态创建的元素绑定事件

 ![img](https://upload-images.jianshu.io/upload_images/14415740-79322a66d202034e.png?imageMogr2/auto-orient/strip|imageView2/2/format/webp) 

```html
<--eg:对.btn进行绑定点击事件，点击.btn后会新增另一个.btn按钮-->
<button class="btn">btn1</button>
```



##### 合成事件hover

通常来说，mouseover和mouseout是一对形影不离的事件，而且实际工作中用的比较多。所以jQuery中定义了hover事件表示这两事件的触发

语法:

```js
$().hover(fn1, fn2)
```

参数fn1表示鼠标移入事件触发的处理函数，参数fn2表示鼠标移出事件触发的处理函数。



##### 解绑事件

我们可以使用off()方法来解除元素绑定的事件。jQuery的off()方法，有点类似于JavaScript的 

removeEventListener()方法。

语法：

```html
$(selector).off(type)    移除所有事件:off()
```

​	type是可选参数，表示事件类型。例如单击事件是"click"，按下事件是"mousedown"，以此类推。如果参数省略，表示移除当前元素中的所有事件。 

​	此外，off()方法不仅可以用来解除使用“基本事件”方式添加的事件，还可以用来解除“绑定事件”添加的事 

件。 

```html
<input id="btn" type="button" value="按钮"><br/> 
<input id="btn_off" type="button" value="解除"/>
```



##### 一次事件

语法:

```js
$(selector).one(type, fn)
```

```html
<--eg:使按钮只点击一次-->
<input id="btn" type="button" value="按钮" />
```

### 



### jQuery事件对象





#### 事件对象

​		当一个事件发生的时候，这个事件有关的详细信息都会临时保存到一个指定的地方，这个地方就是 event对象。每一个事件，都有一个对应的event对象。给大家打个比喻，我们都知道飞机都有黑匣子， 对吧？每次飞机出事（一个事件）后，我们都可以从黑匣子（event对象）中获取详细的信息。 

​		在jQuery中，我们可以通过event对象来获取一个事件的详细信息。以下是常用的属性：

|     属性     |       说明       |
| :----------: | :--------------: |
|     type     |     事件类型     |
|    target    |     事件元素     |
|    which     | 鼠标左，中，右键 |
| pageX，pageY |     鼠标坐标     |
|   shiftKey   | 是否按下shift键  |
|    altKey    |  是否按下alt键   |
|   keyCode    |      键码值      |
|              |                  |

##### event.type

在jQuery中，我们可以使用event对象的type属性来获取事件的类型。

##### event.which

在jQuery中，我们可以使用event对象的which属性来获取单击事件中鼠标的左、中、右键。 

说明：
event.which会返回一个数字，其中1表示左键，2表示中键，3表示右键。

```js
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title></title>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script> $(function () {
            $("a").mousedown(function (e) {
                switch (e.which) {
                    case 1: alert("你点击的是左键"); break;
                    case 2: alert("你点击的是中键"); break;
                    case 3: alert("你点击的是右键"); break;
                }
            })
        }) 
    </script>
</head>

<body><a href="https://www.baidu.com" target="_blank">百度一下</a> </body>

</html>
```



##### shiftKey/altKey/ctrlKey

```
eg:禁止使用Shfit、Ctrl、Alt键
```

#### 键码值keyCode

```
eg:获取“上、下、左、右”方向键
```

##### event.target

在jQuery中，我们可以使用event对象的target属性来获取触发事件的元素。 

**面试题：****event.target 和 event.currenttarget与delegatetarget的区别**





#### jQuery事件冒泡

**jQuery实现不了事件捕获**

阻止事件冒泡：**event.stopPropagation()**

阻止默认行为：event.preventDefault()

```js
eg:阻止form表单提交
$('form').submit(function (e) {  
　　e.preventDefault();
 
});
```

