## jQuery方法

### 元素方法

#### 遍历元素each()

在操作DOM时，很多时候我们需要对“同一类型”的所有元素进行相同的操作。如果使用JavaScript来实现，我们往往都是先获取元素的长度，然后使用循环来访问每一个元素，代码量比较大。在jQuery中，我们可以使用each()方法轻松实现元素的遍历操作。 

语法：

```js
$('li').each(function(index, element){ …… })
```

each()方法接收一个匿名函数作为参数，该函数有两个参数：index，element。 

index是一个可选参数，它表示元素的索引号（即下标）。通过形参index以及配合this关键字，我 

们就可以轻松操作每一个元素。此外注意一点，形参index是从0开始的。 

element是一个可选参数，它表示当前元素，可以使用(this)代替。也就是说，(element)等价于 

$(this)。 

如果需要退出each循环，可以在回调函数中返回false，也就是return false即可。





练习：为每个li设置不同的背景颜色 

var colors=["red","orange","yellow","green","blue"]; 

```
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title></title>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
</head>

<body>
    <ul>
        <li>1</li>
        <li>2</li>
        <li>3</li>
        <li>4</li>
        <li>5</li>
    </ul> <input id="btn" type="button" value="添加背景" />
</body>

</html>
```

#### DOM创建元素

- DOM创建:   var $p = $('<p>这是一段文本</p>>')
- 插入元素
- prepend()   和   prependTo()
- append() 和 appendTo()
- before() 和insertBefore()
- after() 和 insertAfter() 



#### 删除元素

##### remove()：全部删除

##### detach():删除，保留事件

##### empty():清空元素，保留标签

#### 复制元素

语法：

```
$().clone(bool): true:深拷贝将事件和元素一起拷贝 false：只拷贝元素 
```



#### 替换元素

##### replaceWith()

##### replaceAll()



#### 包裹元素

##### wrap()

##### wrapAll()

##### wrapInner()

### 过滤方法

####  类名过滤: hasClass()

​	类名过滤，指的是根据元素的class来过滤。在jQuery中，我们可以使用hasClass()方法来实现类名 

过滤。语法:

```html
$().hasClass(“类名”)
```

hasClass()方法一般用于判断元素是否包含指定的类名：如果包含，则返回true；如果不包含，则
返回false。



#### 下标过滤: eq()

​	下标过滤，指的是根据元素集合的下标来过滤。在jQuery中，我们可以使用eq()方法来实现下标过 滤。 

语法：

```js
$().eq(n)
```

​		说明： n是一个整数，从0开始。当取值为正整数时，eq(0)获取的是第1个元素，eq(1)获取的是第2个元素，……，以此类推。 当取值为负整数时，eq(-1)获取的是倒数第1个元素，eq(-2)获取的是倒数第2个元素，……，以此类 推。 

#### 判断过滤: is()

​	判断过滤，指的是根据某些条件进行判断，然后选取符合条件的元素。在jQuery中，我们可以使用is()方法来实现判断过滤。 

语法：

```
$().is(selector)
```

说明： 

参数selector是一个选择器。is()方法用于判断当前选择的元素集合中，是否存在符合条件的元素。 

如果存在，则返回true；如果不存在，则返回false。

```js
//判断元素是否可见 
$().is(":visible") 
//判断元素是否处于动画中 
$().is(":animated") 
//判断单选框或复选框是否被选中 
$().is(":checked") 
//判断当前元素是否第一个子元素 
$(this).is(":first-child") 
//判断文本中是否包含jQuery这个词 
$().is(":contains('jQuery')") 
//判断是否包含某些类名 
$().is(".select")
```

练习：全选反选

```js
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title></title>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script></script>
</head>

<body>
    <div>
        <p><label><input id="selectAll" type="checkbox" />全选/反选：</label></p> 
            <label><input type="checkbox" class="fruit" value="苹果" />苹果</label> 
		   <label><input type="checkbox" class="fruit" value="香蕉" />香蕉</label>
            <label><input type="checkbox" class="fruit" value="西瓜" />西瓜</label>
    </div>
</body>

</html>
```

#### 反向过滤: not():排除符合条件的

在jQuery中， 我们还可以使用not()方法来过滤“不符合条件”的元素，并且返回余下符合条件的元素。 

其中，not()方法可以使用选择器过滤，也可以使用函数过滤。 

语法：

```
$().not(selector或fn)
```



#### 选择器过滤: filter()

选择器过滤，指的是使用选择器来选取符合条件的元素。

语法：

```js
$().filter(selector or fn)
```



#### has():过滤子代元素

在jQuery中，表达式过滤除了可以使用filter()访问外，我们还可以使用has()方法。has()方法虽然没有filter()方法那么强大，**但是它的运行速度比较快**。

语法：

```
$().has(selector)
```

说明： 

参数selector是一个选择器。 has()方法与filter()方法功能相似，不过has()方法只有选择器过滤，没有函数过滤。因此我们可以把has() 方法看成是filter()方法的精简版。 



### 查找祖先元素

```js
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title></title>
    <style lang="">
        p {
            margin: 6px 0;
        }
    </style>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
</head>

<body>
    <div id="wrapper">
        <p>子元素</p>
        <p class="select">子元素</p>
        <div>
            <p>孙元素</p>
            <p class="select">孙元素</p>
        </div>
        <p>子元素</p>
        <p class="select">子元素</p>
    </div>
</body>

</html>
```



#### parent():找直接父级

#### parents()：找所有父级（）

#### parentsUntil('')：找

parentsUntil()方法是parents()方法的一个补充，它可以查找“指定范围”的所有祖先元 素，相当于在parents()方法返回的集合中截取一部分。

语法：

```
$().parentsUntil(selecotr)
```

说明： 

selector是一个可选参数，它是一个选择器，用来选择符合条件的祖先元素。 

### 查找后代元素

#### children()

在jQuery中，我们可以使用children()方法来查找当前元素的“子元素”。注意，children()方法**只能查找****子元素，不能查找其他后代元素**。

语法：

```
$().children(selector)
```

说明：

selector是一个可选参数，它是一个选择器，用来查找符合条件的子元素。当参数省略，表示选择所有子元素；当参数不省略时，表示选择符合条件的子元素。

#### find()

在jQuery中，我们可以使用find()方法来查找当前元素的“后代元素”。注意，**find()方法不仅能查找子元素，还能查找其他后代元素。**

语法：

```
$().find(selector)
```

说明： 

selector是一个可选参数，它是一个选择器，用来查找符合条件的后代元素。**当参数省略，表示选择所有后代元素**；当参数不省略时，表示选择符合条件的后代元素。 



### 向前查找兄弟元素

```
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title></title>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    
</head>

<body>
    <ul>
        <li>红：red</li>
        <li class="select">橙：orange</li>
        <li>黄：yellow</li>
        <li id="ad">绿：green</li>
        <li>青：cyan</li>
        <li class="select">蓝：blue</li>
        <li>紫：purple</li>
    </ul>
</body>

</html>
```



#### prev()

在jQuery中，我们可以使用prev()方法来查找某个元素的前一个“相邻”的兄弟元素。

语法：

```
$().prev()
```

#### **prevAll()**

在jQuery中，我们可以使用prevAll()方法来查找某个元素前面“所有”兄弟元素。注意，prev()只会查找前面相邻的兄弟元素，而prevAll()则会查找前面所有的兄弟元素。

语法：

```
$().prevAll(selector)
```

说明：selector是一个可选参数，它是一个选择器，用来查找符合条件的兄弟元素。当参数省略，表示选择前 

面所有兄弟元素；当参数不省略时，表示选择前面满足条件的兄弟元素。

#### prevUntil()

### 向后查找兄弟元素

#### next()

#### nextAll()

#### nextUntil()



#### $().siblings('p') :查找所有兄弟元素

















#### 4.8 index() 

在jQuery中，我们可以使用index()方法来获取当前jQuery对象集合中“指定元素”的索引值。 

```
$(selector).index()
```

说明： 

index()方法可以接受一个“jQuery对象”或“DOM对象”作为参数，不过一般情况下，我们很少会使用到参 

数。当index()不带参数时，一般指的是当前元素相对于父元素的索引值。 





练习：

1.分别使用filter实现带有class属性的文本（jQuery）变红

```js
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title></title>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script></script>
</head>

<body>
    <ul>
        <li>HTML</li>
        <li>CSS</li>
        <li>JavaScript</li>
        <li class="select">jQuery</li>
        <li>Vue.js</li>
    </ul>
</body>

</html>
```

