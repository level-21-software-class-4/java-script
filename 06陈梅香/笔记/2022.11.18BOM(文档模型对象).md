2022.11.18BOM(文档模型对象)

1.DOM是文档对象模型：当网页被加载时，浏览器会创建页面的文档对象模型。

2.通过 DOM，可访问 JavaScript HTML 文档的所有元素。

3.DOM 模型被构造为对象的树：通过可编程的对象模型，JavaScript 获得了足够的能力来创建动态的 HTML。

节点类型：

节点类型为1：元素节点

节点类型3：文本节点

节点类型2: 属性节点

获取html元素的三种模式

1、根据id获取HTML元素：document.getElementByld('id名')

2、根据标签名获取HTML元素：document.getElementsByTagName('标签名')

3、根据类名获取HTML元素：document.getElementsByClassName("类名")

元素(elements)

查看body中有几个元素节点

查看子节点

​     console.log(document.body.firstChild);//所有节点中的第一个节点

​    console.log(docuent.body.firstElementChild);//找第一个元素节点

 查看父节点

​    console.log(document.body.children[0].parentElement);

 上一个元素节点

​    console.log(document.body.children[1].previousElementSibling);

 下一个元素节点

​    console.log(document.body.children[1].nextElementSibling);

节点的增删改查

1、创建HTML元素
appendChild()：添加新元素到尾部

insertBefore()：添加新元素到该元素之前

2、删除HTMl节点
removeChild()

3、替换HTML元素
replaceChild()