2022.11.22克隆

一，DOM克隆
1，cloneNode (bool) :true表示内容一起克隆， false:只克隆标签
<body>
        <h2>派大星</h2>
​        <button id="copy">复制</button>&nbsp;
    <div id="box">  
    </div>
    <script>
        var text=document.querySelector('h2');
        var clone=text.cloneNode(true);
        var box=document.getElementById('box')
        var copy=document.getElementById('copy')
        copy.onclick=()=>{
            box.appendChild(clone);
        }
    </script>
</body>
二，innerHTML 与 innerTEXT 区别
1， innerHTML获取标签所包含的HTML+文本信息(从标签起始位置到终止位置全部内容，包括HTML标签，但不包括自身)
2， innerText获取标签所包含的文本信息（从标签起始位置到终止位置的内容，去除HTML标签，但不包括自身）
<body>
    <button id="btn1">innerHTML</button>
    <button id="btn2">innerText</button>

    <div id="box1">
        <h2 id="myH2" class="hide" title="我是h2">我是h2</h2>
    </div>
    <script>
        var btn1 = document.getElementById('btn1');
        var btn2 = document.getElementById('btn2');
        var obj = document.querySelector('#box1');
        btn1.onclick = () =>{
            console.log(obj.innerHTML);
            obj.innerHTML = '<h1>我是h1</h1>';//输出  我是h1
        }
        btn2.onclick = () =>{
            console.log(obj.innerText);
            obj.innerText = '<h1>我是h1</h1>';//输出<h1>我是h1</h1>
        }
    </script>
</body>
三，属性获取与设置 （getAttribute、setAttribute）
1，获取属性 div.getAttribute('class')
2，更改属性 div.setAttribute('class','tinctred')
    <style>
        .tinct{
            width: 200px;
            height: 100px;
            border: solid  2px  blueviolet;
        }
        .tinctred{
            width: 200px;
            height: 100px;
            border: solid  2px red;
        }
    </style>
</head>
<body>
        <button id="hue">边框换色</button>&nbsp;

    <div class="tinct" id="box">  
    </div>
    <script>
        var div=document.querySelector('div');
        var hue=document.getElementById('hue');
        hue.onclick=()=>{
            var chang=div.getAttribute('class');//获取class属性
            if (chang=='tinct') {
                div.setAttribute('class','tinctred');//更改属性
            }else{
                div.setAttribute('class','tinct');
            }
        }
    </script>
</body>
四，获取input的值（输入框，单选框多选框，下拉框）
1，输入框
 <body>
    <input type="text" id="username">
    <button id="btn">点击</button>
    <div id="box"></div>
    <script>
        var obj = document.querySelector('#username');
        var box = document.querySelector('#box');
        var btn = document.querySelector('#btn');
        btn.onclick = ()=>{
            box.innerHTML = '<h2>'+obj.value+'</h2>';
            obj.value = '';
        }
    </script>
</body>
2，单选框
<body>
    <input type="checkbox" class="username">
    <button id="btn">选择</button>
    <div id="box"></div>
    <script>
        var obj = document.querySelectorAll('.username');
        console.log(obj);
        var btn = document.querySelector('#btn');
        btn.onclick = ()=>{
            obj.forEach((e)=>{
                e.checked = true;
            })
        }
    </script>
</body>
3，多选框
<body>
    <input type="checkbox" class="username">
    <input type="checkbox" class="username">
    <input type="checkbox" class="username">
    <input type="checkbox" class="username">
    <button id="btn">全选</button>
    <button id="btn1">反选</button>
    <div id="box"></div>
    <script>
        var obj = document.querySelectorAll('.username');
        console.log(obj);
        var btn = document.querySelector('#btn');
        var btn1 = document.querySelector('#btn1');
        btn.onclick = ()=>{
            obj.forEach((e)=>{
                e.checked = true;
            })
        }
        btn1.onclick = ()=>{
            obj.forEach((e)=>{
                e.checked = e.checked?false:true;
            })
        }
    </script>
</body> 
4，下拉框
<body>
    <input type="text" id="txtTitle" />
    <input type="button" value="修改标题" />
    文字颜色:
    <select id="selFgColor" >
        <option value="white">白色</option>
        <option value="red">红色</option>
        <option value="green">绿色</option>
        <option value="blue">蓝色</option>
        <option value="yellow">黄色</option>
        <option value="black">黑色</option>
    </select>
​    背景颜色:
​    <input type="color" id="clBgColor" />
​    <br /><br />
    <p>
        JavaScript一种直译式脚本语言，是一种动态类型、弱类型、基于原型的语言，内置支持类型。
        它的解释器被称为JavaScript引擎，为浏览器的一部分，广泛用于客户端的脚本语言，
        最早是在HTML（标准通用标记语言下的一个应用）网页上使用，用来给HTML网页增加动态功能。
    </p>
    <script>
        var selectObj = document.querySelector('#selFgColor');
        var pSignObj = document.querySelector('p');
        selectObj.onchange =()=>{
            var colorValue = selectObj.value
            //2.设置字体颜色
            pSignObj.style.color = colorValue;
        }
    </script> 
</body>