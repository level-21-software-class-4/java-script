2022.11.16正则2

分组和捕获

分组：1.改变优先级 2.分组引用

分组用（）进行，为子表达式

捕获和反捕获都是针对子表达式进行的

```
/(ab)(12)/.exec('ab12')//小括号中的为单独的字符串
```

```
/d{4}-/d{3}-d{2}
//数字代表几位数
regExp.$1(2.3.....)//输出下标为1的
```

使用replace捕获分组

```
var pattern=/([a-z]+)(\d+)
str.replace.(pattern."$2$1")
//与regExp中的$2$1作用相同
```

反捕获
反捕获组只匹配结果，不能在表达式和程序中做进一步处理
举例说明	
(hello)	匹配 pattern 并获取这一匹配。所获取的匹配可以从产生的 Matches 集合得到。
(?:hello)	匹配 pattern 但不获取匹配结果
(?=hello)	正向肯定预查（look ahead positive assert。这是一个非获取匹配。
(?!hello)	正向否定预查(negative assert)。这是一个非获取匹配。
(?<=hello)	反向(look behind)肯定预查，与正向肯定预查(?=pattern)类似，只是方向相反。这是一个非获取匹配。
(?<!hello)	反向否定预查，与正向否定预查(?!pattern)类似，只是方向相反。这是一个非获取匹配

3， |或者
从左往右匹配，一但匹配上就停止

|(or)
文件名由字母、数字、下划线构成，不可以以数字开头，后缀为.zip/rar/gz  

 console.log(/[^\d]*\.zip|rar|gz/.exec('zip.zip'));

日期

        单词边界:\b
        console.log(/e\b/.exec('eapples'));
    
        中文名字：三个汉字、中间必须是小或晓
        console.log(/([\u4e00-\u9fa5])[小晓]([\u4e00-\u9fa5])/.exec('黄小薇'));

环视
?=:紧跟着  ?!:不紧跟着     ?:-->反捕获  ??--> 非贪婪{0,1}

```
 查b (由c紧跟着)
 console.log(/b(?=c)/.exec('abcbd'));

 查a (后面跟的不是b)
 console.log(/a(?!b)/.exec('abca'));
```

异常

null：不能再添加其他属性

包装类 ：null.undefined没有包装类

异常处理

try(//尝试捕获异常，里面放的是可能出错的代码）{

}catch(error){

log(error)//指出报错的地方

}finally//不管有没有捕获错误都会运行{

}

每次只能捕获一个异常