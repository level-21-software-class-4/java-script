### 正则

###### 创建正则

```
var exp = /a/
console.log(exp.test('a'))
或者
console.log(/a/.test('a'))
或
console.log(/a/.exec('a'))
匹配到true  没匹配到false
```

###### 匹配

1.数字：[0-9]      简写：\d

```
console.log(/[0-9]/.exec('3783630'))
console.log(/\d/.exec('3783630'))
```

2.字母：[a-zA-Z]     简写：\w

```
console.log(/[a-z]/.exec('ugdvj'))
```

3.（1）{m,n}匹配最少m次，最多n次

​              {m}匹配m次

​              {m,}最少匹配m次，到无限次

```
//匹配数字4到6个
console.log(/\d{4,6}/.exec('37830'))
```

4.

​	贪婪模式：？                           

​	{1，} ：匹配1次或多次     简写：+

​	{0，}：匹配0次或多次       简写：*

​	{0，1}：匹配0次或1次       简写：？

​	