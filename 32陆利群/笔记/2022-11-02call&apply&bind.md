### call & apply & bind

作用：都是用来改变函数/方法的this指向，也就是说，可以让一个对象，使用另外一个对象的方法。

**传参时**：
call：

```javascript
fn2.call(fn1,a,b,c);
```

apply：必须以数组的形式传入

```javascript
fn2.apply(fn1,[a,b,c]);
```

bind：和call一样

**使用时：**
call和apply会自动执行函数，而bind不会执行函数，而且将函数返回