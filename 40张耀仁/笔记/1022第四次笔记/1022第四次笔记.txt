JavaScript 函数语法
函数就是包裹在花括号中的代码块，前面使用了关键词 function：

局部 JavaScript 变量
在 JavaScript 函数内部声明的变量（使用 var）是局部变量，所以只能在函数内部访问它。（该变量的作用域是局部的）。
您可以在不同的函数中使用名称相同的局部变量，因为只有声明过该变量的函数才能识别出该变量。
只要函数运行完毕，本地变量就会被删除。

全局 JavaScript 变量
在函数外声明的变量是全局变量，网页上的所有脚本和函数都能访问它。



局部变量会在函数运行以后被删除。

全局变量会在页面关闭后被删除


concat:连接,数组合并
        const arr = [1,2,3]
        const arr1 = [4,5,6]

        const new_arr = [...arr,...arr1]
         const new_arr = arr.concat(arr1);
         console.log(arr);
         console.log(arr1);
        console.log(new_arr);


   join:将数组转成字符串
        const arr = [1, 2, 3, 4, 5, 6]
         console.log(arr.join);
         var str = arr.join(',')
         console.log(str);

	string.split('')将字符串转成数组 
         var text =
         `I love teaching and empowering people. I teach HTML, CSS, JS, React, Python.`
        将上述字符串去除标点空格后统计有多少个单词
         const arr1 = text.split('.').join('').split(',').join('').split(' ')

  forEach(遍历数组)
         arr.forEach(
           匿名函数  
         function (element){
            console.log(element);
         }
         )

        映射方法map(),
         const arr = [1, 2, 3, 4, 5, 6]
         const arr1 = arr.map(
            function (element) {
                return element + 1
             }
         )
         console.log(arr1);

        将数组的单词全转为大写
         const countries = ['Finland', 'Sweden', 'Norway', 'Denmark', 'Iceland']
         const new_countries = countries.map(
             function (e){
                return e.toUpperCase()
            }
         )
         console.log(new_countries);

        filter过滤:返回数组中符合条件的数据
         const new_arr = arr.filter(
            function (e){
                return e%2==0  //true：返回 false：不符合条件=>过滤
             }
         )
         console.log(new_arr);
        

        reduce(),reduceRight():归纳汇总： 返回一个总数据
         const arr = [1, 2, 3, 4, 5, 6]
         var sum = arr.reduceRight(
             function (v1,v2){
                 return v1*v2
            }
         )
         console.log(sum);


some()

例:      var isMatch = arr.some(
            function (e){
                return e==6
            }
        )
        console.log(isMatch);
箭头函数： var aa = (形参列表) => {函数体} arrow function

函数只有一个return语句时，省略{}和return关键字,直接写返回值

无返回值,一般不用箭头函数


        every():只要有一个false，返回

二维数组
         const arrayOfArray =  [[1, 2, 3], [4, 5, 6]]
      增删改查
        console.log(arrayOfArray[1][1]);

        删
         arrayOfArray[1].splice(1,1)
        console.log(arrayOfArray);