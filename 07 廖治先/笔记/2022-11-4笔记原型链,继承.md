## 原型链继承

通过实例化一个新的函数，子类的原型指向了父类的实例，子类就可以调用其父类原型对象上的私有属性和公有方法。（本质就是重写了原型对象）

```
function Parent() {
  this.parentName = '父类';
}
Parent.prototype.getParentName = function() {
  return this.parentName;
};

function Child() {
  this.childName = '子类';
}
Child.prototype = new Parent();

var c = new Child();
console.log(c.getParentName()); // '父类'

//子类要在继承后实例化
```

## 构造函数继承

即在子类型构造函数的内部调用超类型构造函数。

```
function Parent(name) {
  this.name = name;
  this.hobbies = ["sing", "dance", "rap"];
}

function Child(name) {
  Parent.call(this, name);
  this.age = 24
}

var c1 = new Child('c1');
var c2 = new Child('c2');
c1.hobbies.push('coding');
console.log(c1.hobbies)
console.log(c2.hobbies)
```

## 圣杯(永恒)模式

```
function Parent() {
        }
        Parent.prototype.name = '爹';
        Parent.prototype.age = 40;

        function Child() {
        }

        function inherit(Target, Origin) {
            function F(){} //临时构造函数
            F.prototype = Origin.prototype;
            // var f = new F();//实例化:复制一份
            // Target.prototype = f; //独立的f，里面有父类所有的属性和方法
            Target.prototype = new F();
            //归位
            //构造器归位
            Target.prototype.constructor = Target;
            //超类
            Target.prototype.uber = Origin;

        }
        inherit(Child, Parent);
        var child = new Child();
        console.log(child.name);
        console.log(Child.prototype.constructor);
        var parent = new Parent();
        console.log();
        console.log(parent.name);
```

