圣杯模式（最终版）

```
        function Parent() { }
        function Child() { }
        Parent.prototype.name = 'cp';

        //
        var inherit = (function() {
            var Temp = function(){};  //Temp
            return function (Target, Origin) {
                Temp.prototype = Origin.prototype;
                Target.prototype = new Temp();
                //归位
                Target.prototype.constructor = Target;
                //超类
                Target.prototype.uber = Origin.prototype;
            }
        }())
```

class 继承

```
  class Circle extends Shape{
        constructor(x,y,radius){
            super(x,y);  //调用父类的constructor
            this.radius = radius;
        }
       }
       const circle = new Circle(0,0, 10);
       console.log(circle.x);
       console.log(circle.y);
```

hasOwnProperty():

查询函数本身拥有的属性(不包括原型中的属性)

链式调用:(同级的对象可用 . 连接 函数可直接用（）连接在后面实现同时调用)

```
       var obj ={
            mon: function (){
                console.log('周一');
                return this;
            },
            tue: function (){
                console.log('周二');
                return this;
            }
        }
        obj.mon().tue();

        function mon(){
            console.log('周一');
            return function (){
                console.log('周二');
                return function(){
                    console.log('周三');
                }
            }
        }
        mon()()();
```

