正则

```
 console.log(/(aab)\1{4}(会从左向右匹配若干个与大括号中相同的数当有一个匹配不同时结束匹配)/.exec('aabaabaabaabaab'));
```

反捕获:?

```
console.log(/(?:aabc)(ccab)/.exec('aabcccabaabcccab'));
？用于隐藏括号中需要捕获的信息（不显示该分组）
```

?=:-->紧跟着  ?!:-->不紧跟着   ?:-->反捕获  ??--> 非贪婪{0,1}

异常处理：(try {}  catch(error){})

```
		try：尝试捕获异常,里面放的是可能出错的代码
        try {
               const fn = function () {
               const data = null;
               return data;
             };
             const data = fn();
            //包装类: null undefined没有包装类
            console.log(data.name);//报错
            console.log('try');
        } catch (error) {
            //对捕获到的错误进行一些处理
            // console.log(error.message);
            console.log('catch');
        } finally {
            //不管有没捕获错误，都会执行finally中的代码
            console.log('this is finally');
        }
       // finally 可不写 catch必须要写
```

