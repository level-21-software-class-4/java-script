合成事件hover

```
$().hover(fn1, fn2)
```

一次事件

```
$(selector).one(type, fn) //只能使用一次
```

阻止事件冒泡：event.stopPropagation()

阻止默认行为：event.preventDefault()

##### event.type

在jQuery中，我们可以使用event对象的type属性来获取事件的类型。

##### event.which

在jQuery中，我们可以使用event对象的which属性来获取单击事件中鼠标的左、中、右键。 

说明：
event.which会返回一个数字，其中1表示左键，2表示中键，3表示右键。