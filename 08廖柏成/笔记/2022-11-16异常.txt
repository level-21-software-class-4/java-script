//调用后台接口 {name:'cp', age:18}

        //异常处理
        //try：尝试捕获异常,里面放的是可能出错的代码
        // try {
        //     // const fn = function () {
        //     //     const data = null;
        //     //     return data;
        //     // };
        //     // const data = fn();
        //     // //包装类: null undefined没有包装类

        //     console.log(data.name);//报错
        //     console.log('try');

        // } catch (error) {
        //     //对捕获到的错误进行一些处理
        //     // console.log(error.message);
        //     console.log('catch');
        // } finally {
        //     //不管有没捕获错误，都会执行finally中的代码
        //     console.log('this is finally');
        // }


        // console.log('----------');
 


        //1.报错之后的代码中止执行
        // try {
        //     console.log('1');
        //     throw new Error('error 1');//错误
        //     console.log('2 ');  
        // } catch (error) {
        //     console.log('3');
        // } finally {
        //     console.log('4');
        // }
        // console.log('5');
        // //1 345


        //2.每次只能捕获一个异常
        // try {
        //     console.log('1');
        //     throw new Error('error 1'); //error
        //     console.log('2 ');
        // } catch (error) {
        //     console.log('3');
        // } finally {
        //     console.log('4');
        //     try {
        //         throw new Error('error 2'); //error
        //     } catch (error) {

        //     }
        //     console.log('5');
        // }
        // console.log('6');

        // function fn1() {
        //     var a = 6;
        //     try {
        //         console.log('1');
        //         return a; //6
        //     } catch (error) {
        //         console.log(error.message);
        //     } finally {
        //         console.log('2');
        //         a = 9;
        //         // return a;
        //     }
           
            
        // }
        // const fn1Result = fn1();  
        // console.log('fn1Result:' + fn1Result);


正则2
//使用replace分组,捕获
        //引用: \1：引用子表达式的内容
        // console.log(/(aab)\1{4}/.exec('aabaabaabaabaab'));

        //反捕获:?::将捕获到的子表达式隐藏
        // console.log(/(?:aabc)(ccab)/.exec('aabcccabaabcccab'));

        //  |(or)
        // 文件名由字母、数字、下划线构成，不可以以数字开头，后缀为.zip/rar/gz  
        // console.log(/[^\d]*\.zip|rar|gz/.exec('zip.zip'));
        

        //0 - 99  9
        // 0-9 10-99
        // \d|[1-9][\d]
        //自左向右匹配，一旦匹配上 就不再回头
        // console.log(/[1-9][\d]|\d/.exec('9'));
	//日期

        //单词边界:\b
        // console.log(/e\b/.exec('eapples'));

        //中文名字：三个汉字、中间必须是小或晓
        // console.log(/([\u4e00-\u9fa5])[小晓]([\u4e00-\u9fa5])/.exec('黄小薇'));

//环视
    //?=:紧跟着  ?!:不紧跟着     ?:-->反捕获  ??--> 非贪婪{0,1}

    //查b (由c紧跟着)
    // console.log(/b(?=c)/.exec('abcbd'));

    //查a (后面跟的不是b)
    // console.log(/a(?!b)/.exec('abca'));

