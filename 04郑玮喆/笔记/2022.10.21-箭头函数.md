# 5、函数

## 5.1 定义函数

    function funcName (var){
        return var;
    }

## 5.2 匿名函数

    function (var) {
        return var;
    }
    
    var a = function (var) {
        return var;
    }

## 5.3 箭头函数

var a = (形参) => {函数体(只有return语句时可以省略大括号)}

无返回值,一般不用箭头函数

# 6、对象

## 6.1 定义对象

    var obj = {
        name:value;
        'String':value;
    }

构造函数

    function GetName() {
        this.name = values;
        this.age = values;
    }

生成对象

var obj = new obj();

## 6.2 增删查

查：obj.name

增：obj.原来obj中没有的属性，相当于将该属性添加到对象obj中

删：delete  一般删之前要对该属性进行判断
