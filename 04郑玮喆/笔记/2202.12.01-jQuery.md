# 16、jQuery

jQuery 是一个 JavaScript 库。

## jQuery 选择器

| $("*")                   | 选取所有元素                                        |
| ------------------------ | --------------------------------------------- |
| $(this)                  | 选取当前 HTML 元素                                  |
| $("p.intro")             | 选取 class 为 intro 的 <p> 元素                     |
| $("p:first")             | 选取第一个 <p> 元素                                  |
| $("ul li:first")         | 选取第一个 <ul> 元素的第一个 <li> 元素                     |
| $("ul li:first-child")   | 选取每个 <ul> 元素的第一个 <li> 元素                      |
| $("[href]")              | 选取带有 href 属性的元素                               |
| $("a[target='_blank']")  | 选取所有 target 属性值等于 "_blank" 的 <a> 元素           |
| $("a[target!='_blank']") | 选取所有 target 属性值不等于 "_blank" 的 <a> 元素          |
| $(":button")             | 选取所有 type="button" 的 <input> 元素 和 <button> 元素 |
| $("tr:even")             | 选取偶数位置的 <tr> 元素                               |
| $("tr:odd")              | 选取奇数位置的 <tr> 元素                               |

## jQuery操作属性

### 查询属性

.attr('属性名')

.prop('属性名')：如果返回值为boolean

.attr('属性名','属性值')：添加属性

.removeAttr('属性名')：删除属性

### 操作属性内容

.html()：连同标签一起获取

.text()：只获取文本节点

.val()：获取值

### 操作属性样式

.css('样式名','样式值'....)：为对象添加属性样式（可多个）

## jQuery事件

$('').事件名()：绑定事件

$('').off('事件名')：解绑事件

$('').one('事件名')：只执行一次的事件

$('').bind('事件名'，函数体)：为多个事件绑定同一个函数

$('').on('事件名'，函数体)：跟bind一样，添加为动态添加的元素进行事件绑定

e.preventDefault()：禁止默认

e.stopPropagation()：禁止冒泡

### 事件对象

target：事件源对象

currentTarget：当前调用的对象

delegateTarget：当前调用的 jQuery 事件处理程序所添加的元素

## 元素操作

添加元素

.prepend(创建的节点对象)：在对象开头添加节点

.append(创建的节点对象)：在对象末尾添加节点

.remove()：删除子节点（连同自身）

.detach():删除，保留事件

.empty()：只删除子节点

.clone(boolean)：克隆元素，true连同事件一起拷贝，false只拷贝元素

.repalceWith()

.replaceAll()

## 过滤方法

.hasClass()：类名过滤，是否包含指定的类名

.eq()：下标过滤

.is()：判断过滤，根据条件进行判断

.not()：排除符合条件的

.filter()：选择器过滤，使用选择器或方法选取符合条件的元素

.has()：与选择器过滤一样，只能用选择器过滤

.parent

.parents()

.parentUnit()

.children()

.find()

.prev()

.prevAll()

.prevUnit()

.next()

.nextAll()

.nextUntil()

.siblings('p')

index()
