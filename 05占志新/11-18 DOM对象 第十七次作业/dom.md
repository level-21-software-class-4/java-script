Javascript 与DOM的关系:
DOM定义了表示和修改文档所需的方法，DOM对象即为宿主对象，由浏览器厂商定义，用来操作html和xml功能的一类的对象的集合。也有人称DOM是对HTML以及XML的标准编程接口。
 DOM常用API
 文档级节点
document:DOM中最大的节点
document.documentElement
document.head
document.title（可修改）：获取标题
document.body
子节点： 文本节点+元素节点
 元素节点 | 属性节点 | 文本节点 |  注释节点  |
nodeType  |    1       |    2         |    3         |     8      |
nodeName|  元素名|  属性名     |  #text      |  #comment  |
nodeValue |   null   |  属性值    |  文本值     | 注释的文本 |
