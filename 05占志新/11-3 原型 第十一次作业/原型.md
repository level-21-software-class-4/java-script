# call apply bind 
他们的功能是改变this的指向


## 构造函数和prototype原型

构造函数是一种特殊的函数，主要用来初始化对象，即为对象成员变量赋初始值，它总和new一起使用。 我们可以把对象中一些公共属性和方法抽取出来，然后封装到这个函数里面。

#### new在执行时会做四件事情:

1. 在内存中创建一个新的空对象
2. 让this指向这个新对象
3. 执行构造函数里面的代码，给这个新对象添加属性和方法
4. 返回这个新对象（所以这个构造函数里面不需要return）

JS的构造函数中可以添加一些成员，可以在构造函数本身上添加，也可以在构造函数内部的this上添加。 通过这两种方式添加的成员，分别称为静态成员和实例成员。

- 静态成员

  在构造函数本身上添加的成员；只能由构造函数本身来访问

- 实例成员

  在构造函数内部创建的对象成员称为实例成员（this添加）；只能由实例化的对象来访问 ;实例成员就是构造函数内部通过this添加的成员

```js
//构造函数
function fun (name,age){
    this.name
    this.age   
}
var a = new fun('zs',18)
```

## 构造函数 原型对象 prototype

**1.所有被该构造函数构造出的对象都可以继承原型上的属性和方法**。

**2.如果this有自己的属性，那么会覆盖掉原型上已有的属性。**（开发惯例：所有方法写到原型，部分属性写到this）一般来说属性是配置项，需要你主动传参去配置。但是一般来说某个插件的方法功能几乎是一致的。

对象对原型的增删改

```js
function Phone(color, brand) {
 this.color = color;
 this.brand = brand;
 this.screen = screen;
}
//原型本身也是一个对象 增删改查
console.log(Phone.prototype);

 //add properties
 Phone.prototype.screen = '18:9';
 console.log(Phone.prototype);

//修改原型上的属性
 Phone.prototype.screen = '20:10';
 console.log(Phone.prototype.screen);

 //删
 delete Phone.prototype.screen;
 console.log(Phone.prototype);
