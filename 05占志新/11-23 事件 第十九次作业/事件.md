```
鼠标单击事件 btn.addEventListener('click',fn);
鼠标双击事件 btn.addEventListener('dblclick',fn);
鼠标双击移动 btn.addEventListener('mousemove',fn);
```

```
事件取消 btn.removeEventListener('click',fn);
```
```
  //addEventListener('click',fn2,false)：默认false，表示冒泡 true：捕获
  //事件从最外层开始捕获，直到当前元素（触发事件的对象)，再从当前元素向外冒泡到document
  //事件会自发冒泡(微软)（从里层向外层冒泡，直到document）
  //事件捕获(网景)（从最外层向里层）
```
