引入jquery
```
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
```

标签选择器

```
$("标签名")
```

层次选择器

```
后代选择器
$('#parent div'):选择id为parent的元素的所有div元素
子代选择器
$('#parent>div'):选择id为parent的直接div子元素
相邻选择器
$('.blue + img'):选择css类为blue的下一个img元素
同辈选择器
$('.blue ~ img'):选择css类为blue的之后的img元素
```

表单伪类选择器

| Forms          | 名称      | 举例                                                         |
| -------------- | --------- | ------------------------------------------------------------ |
| 表单选择器     | :input    | 查找所有input元素: $(':input');<br />注意：会匹配所有的input, textarea,select和button元素 |
| 文本框选择器   | :text     | 查找所有的文本框: $(':text')                                 |
| 密码框选择器   | :password | 查找所有密码框                                               |
| 单选按钮选择器 | :radio    | 查找所有单选按钮                                             |
| 复选框选择器   | :checkbox | 查找所有复选框                                               |
| 提交按钮选择器 | :submit   | 查找所有提交按钮                                             |
| 图像域选择器   | :image    | 查找所有图像域                                               |
| 重置按钮选择器 | :reset    | 查找所有重置按钮                                             |
| 按钮选择器     | :button   | 查找所有按钮                                                 |
| 文本域选择器   | :file     | 查找所有文本域                                               |