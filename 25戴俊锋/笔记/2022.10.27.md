```javascript
 <script type="text/javascript">
          //map对象 是一组键值对的结构
        //创建map对象
        //var map = new Map();
        
        //添加
        //map.set(1,'呼呼');
        //map.set(2,"拉拉");
        //console.log(map);

        //查看 v
        //console.log(map.get(1));

        //删除
        //map.delete(1);
        //console.log(map);

       //返回map存在的键的数组
       //console.log(map.keys());
       
       //返回map存在的值的数组
       //console.log(map.values());

       //返回map中存在的键值对的数组
       //console.log(map.entries());

       //查看键是否存在
       //console.log(map.has(1));

       //查看map的长度
       //console.log(map.size);

       //map转数组
        // var map = new Map();
        // map.set('name','huhu');
        // map.set('age',25);
        // const arr = [];
        // for( let x of map.entries())
        //     arr.push(x);
        
        // console.log(arr);

        //数组转map
        // const arr = [['name','huhu'],['age',18]];
        // console.log(arr);
        // var map = new Map;
        // for([k,v] of arr){
        //     map.set(k,v);
        // }
        // console.log(map);
        
        //Set 对象类似于数组，且成员的值都是唯一的 所以set可以用来去重
            // var set = new Set([1,2,3,47,5]);
            // console.log(set);
        //去重
        // const arr = [1,3,3,5,7,9];
        // const arr2 = [...new Set(arr)];
        // console.log(arr2);

        //Map 和 Set 都不允许键重复
        //Set 不能通过迭代器来改变Set的值，因为Set的值就是键

        //set 转数组
            // var set = new Set([1,2,3,4,5]);
            // const arr = [...set];
            // console.log(arr);

        //数组转Set
        // const arr = [1,3,5,7,9];
        // var set = new Set(arr);
        // console.log(set);

        //Map 对象保存键值对，并且是按照插入的顺序，任何值（对象或者基本类型）都可以作为一个键或者一个值
        //Set 对象存储任何类型的唯一值，无论是原始值或者是对象引用
        //WeakMap 的键是弱引用，其键对象必须是对象而值可以是任意的
        //WeakSet 对象值的集合
    </script>
```

