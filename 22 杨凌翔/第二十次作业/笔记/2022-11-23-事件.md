## 事件

#### 设置事件：

需要设置事件的对象.addEventListener(事件类型,需要执行的函数,Boolean)

#### 取消事件：

需要取消事件的对象.removeEventListener(事件类型,需要执行的函数)