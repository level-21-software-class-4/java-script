## BOM  浏览器对象模型  

浏览器对象模型（BOM）是以 window 对象为基础的，这个对象代表了浏览器窗口和页面可见的区域。

主要包括window、history、location和document等对象，其中window对象是整个BOM的顶层对象。

 通过 location 对象可以以编程方式操纵浏览器的导航系统。通过设置这个对象上的属性，可 以改变浏览器 URL 中的某一部分或全部。 

 使用 replace()方法可以替换浏览器历史记录中当前显示的页面，并导航到新 URL。 

 navigator 对象提供关于浏览器的信息。

screen 对象中保存着客户端显示器的信息。这些信息 通常用于评估浏览网站的设备信息。

history 对象提供了操纵浏览器历史记录的能力，可以确定历史记录中包含多少个条目，并以编程方式实现在历史记录中导航，而且也可以修改历史记录。

### setTimeout()

可以延时执行函数，只执行一次

```js
//无参
setTimeout(要执行的函数代码, 等待的毫秒数); 
//带参
function sayHi(phrase, who) {
  alert( phrase + ', ' + who );
}
setTimeout(sayHi, 1000, "Hello", "John"); // Hello, John
```

**如果第二个参数为0**

因为js是单线程执行，setTimeout的内容会留到其他普通队列执行后再执行

相当于异步执行，可以改变任务的执行顺序

### setInterval()

依然延迟执行，但是反复

```js
setInterval(要重复执行的函数代码, 等待的毫秒数); //一样也能传参

//结束执行
itv = setInterval(fn,80);
clearInterval(itv);
```

### **onclick()**

onclick 事件在元素被点击时发生

```js
//html
<element onclick="SomeJavaScriptCode">  //SomeJavaScriptCode：规定该事件发生时执行的 JavaScript
//JavaScript
object.onclick=function(){SomeJavaScriptCode};
```



**document.getElementById()**：查找文档特定元素

**getElementsByTagName()**：获取任何类型的 HTML 元素的列表，不区分大小写

```js
var tables = document.getElementsByTagName("table");
alert ("This document contains " + tables.length + " tables");
//也可以获取文档中特定元素
var myParagragh = document.getElementsByTagName("p")[3];
```

##### 修改元素样式

```js
//修改td1的字体颜色
'td1'.style.color = '#FF5072'
//修改td1的背景颜色
'td1'.style.backgroundColor = 'white';
```

**修改文档某元素的文本内容**

```js
document.getElementById('pOutput').innerText = 