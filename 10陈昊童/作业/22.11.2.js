    // //1.        
    // function fn(a){
    //     this.a = a
    // }
    //     var obj={};
    //     var getNum = fn.bind(obj);
    //     getNum(2);
    //     console.log(obj.a);//2

    // //2.
    // function fun(n,o) {
    //     console.log(o);
    //     return{
    //         fun:function (m) {
    //             return fun(m,n)
    //         }
    //     }
    // }

    // var a= fun(0);a.fun(1);a.fun(2);a.fun(3);// undefined ；  0 ； 0 ；0；
    // var b= fun(0).fun(1).fun(2).fun(3);//undefined 0 1 2 
    // var c= fun(0).fun(1);c.fun(2);c.fun(3);//undefined  0 1 1

    //3.使用递归求到20的斐波那契数列之和
    function num(n) {    
        if (n==1||n==2) {
            return 1
        }else{
            return num(n-1)+num(n-2)
        }
    }    
    function sum(n) {
        if (n==0) {
            return 0
        }
        return num(n)+sum(n-1)
    }
    console.log(sum(20));

