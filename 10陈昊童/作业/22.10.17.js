const arr = [3,2,1,4,5]
// 将数组中的  元素1  与  元素3 调换位置
let temp = arr[2]
arr[2] = arr[0]
arr[0] = temp
console.log(arr);

// 使用冒泡排序（从大到小）：  [90,65,78,66,48,39,92,73]  使用数组解构的方法交换变量
const arr2 =[90,65,78,66,48,39,92,73]

for (let index = 0; index < arr2.length-1; index++) {
        for (let j = 0; j < arr2.length-1; j++) {
               if (arr2[j]>arr2[j+1]) {
                [arr2[j],arr2[j+1]]=[arr2[j+1],arr2[j]]
               }
                
        }

}
console.log(arr2);

var text =
'I love teaching and empowering people. I teach HTML, CSS, JS, React, Python.'
//将上述字符串去除标点空格后统计有多少个单词
var text1= text.replace(/[,.]/g,"")
const textarr=text1.split(' ')
console.log(textarr);
console.log(textarr.length);

const arr3 = [87,85,74,70,65,59,43]
// 1.移除第一个元素87，并在开头添加 元素86
arr3.shift();
arr3.unshift(86);
console.log(arr3);
// 2.移除最后一个元素43， 并在末尾添加1
arr3.pop();
arr3.push(1);
console.log(arr3);
// // 3.在 70 65 之间插入 元素 68
arr3.splice(4,0,68);
console.log(arr3);
// // 4.删除元素65
arr3.splice(5,1)
console.log(arr3);
// 5.使用map返回一个新的数组new_arr，要求新数组new_arr比原来数组大2
const new_arr= arr3.map((item)=>{
        return item+2
})
console.log(new_arr);
// 6.筛选数组new_arr返回new_arr1,要求new_arr1: 能被2整除
const new_arr1=arr3.map((item)=>{
        if (item%2==0) {
                return item   
        }
        

})
console.log(new_arr1);