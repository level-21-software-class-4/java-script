```javascript
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>

    </style>
    <script>
        // querySelector
        querySelector("#XXX") // id
        querySelector(".XXX") //Class
        querySelector("XXX") //<XXX>
        document.querySelectorAll
        //添加节点
        function add() {
            var div = document.querySelector("#div01");
            var span = document.createElement("span");
            var text = document.createTextNode("呼呼");
            span.appendChild(text);
            div.appendChild(span);
        }
        //删除节点
        function del() {
            var span = document.querySelector("#div01 span")
            var div = document.querySelector("#div01");
            div.remove(span);
        }
        //替换节点
        function charge() {
            var span = document.querySelector("#div01 span")
            var div = document.querySelector("#div01");
            var div1 = document.createElement("div")
            div.replaceChild(div1, span);
        }
        //克隆节点
        function clone() {
            var span = document.querySelector("#div01 span");

            var div = document.querySelector("#div01");
            // var newSpan = span.cloneNode(false); //默认值 false，表示浅拷贝，只拷贝标签，不拷贝内容;
            var newSpan = span.cloneNode(true); //拷贝标签和内容
            div.appendChild(newSpan);
        }


    </script>
</head>

<body>
    <button id="but01" onclick="add()" >添加节点</button>
    <button id="but02" onclick="del()">删除节点</button>
    <button id="but03" onclick="charge()">替换节点</button>
    <button id="but04" onclick="clone()">克隆节点</button>
    <div id="div01">
        <span>123</span>
    </div>
</body>

</html>
```

